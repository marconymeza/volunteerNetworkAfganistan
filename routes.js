var express = require('express');
var router = express.Router();
var path = require('path');


router.use(express.static(path.join(__dirname, 'public')));

// app.use(express.static(path.join(__dirname, 'public')));
router.use(express.static(__dirname + '/public'));

router.get('/home', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
router.get('/register', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
router.get('/Confirmation', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

router.get('/ForgotPassword', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
router.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
router.get('/loginWho', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

router.get('/panel', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

router.get('/mail', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

router.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
  });

router.get('/activities', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
router.get('/activity', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

router.get('/Vacancy', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
  
router.get('/chat', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
  
router.get('/perfil/:id', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
  });

router.get('/perfilVacancy/:id', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
  });

router.get('/perfilEvent/:id', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
  });


router.get('/perfilVolunteer/:id', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
  });

  router.get('/dashboard', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});



module.exports = router;