const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const PROFESSION_DESCRIPTION = sequelize.define('PROFESSION_DESCRIPTION', 
    {
      // definicion del los campos
      PROFESSION_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      PROFESSION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'PROFESSION_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.ProfessionDescription = PROFESSION_DESCRIPTION


