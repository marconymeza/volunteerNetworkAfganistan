import React from 'react'
import cookie from 'react-cookies'

class Login extends React.Component {
    constructor() {
        super()
        this.state = {
            Login:[]
        }
        this.Login = this.Login.bind(this)
    }
    Login(event) {
        event.preventDefault();
        var data = {
            Password: document.getElementById('loginPassword').value,
            Email: document.getElementById('loginEmail').value
        }
        if (data.Email == "") {
            alertify.error('ایمیل یا نام کاربری نیاز است');
            document.getElementById("loginEmail").className = "form-control error"

        } else if (data.Password == "") {
            alertify.error('رمزعبور لازم است');
            document.getElementById('loginPassword').className = "form-control error"
        }
        else {
            fetch(window.location.origin + '/Get/ValidationLogin/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status==200) {
                                    alertify.success('مؤفقانه انجام شد')
                                    cookie.save('dataUser', JSON.stringify(x.data[0]), { path: '/' })
                                    setTimeout(() => {
                                        window.open('/panel', "_self")
                                      }, 2000);
                            }else if(response.status==404){
                                
                                alertify.error('لطفا ایمیل خود را تأیید کنید')
                            }else if(response.status==500){
                                
                                alertify.error('نام کاربردی یا ایمیل درست نیست')
                            }
                        })
                })
        }



    }
    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }
    render() {
        return (
            <section id="Login">
                <div className="container-fluid topSpace">
                    <div className="row text-right" >
                        <form name="login form-signin" className="form-signin"onSubmit={this.Login}>
                            <img className="img-fluid" src="/images/Home/LogoColor.png" />
                            <h1 className="h3 mb-3 font-weight-normal">لطفاً وارد شوید</h1>
                            <div className="md-form">
                                <input type="text" id="loginEmail" onChange={this.ifEmpty.bind(this, "loginEmail", "form-control text-right")} className="form-control text-right"/>
                                <label className="lblRight" htmlFor="loginEmail">کاربر یا پوست الکترونیک</label>
                            </div>
                            <div className="md-form">
                                <input type="password" id="loginPassword" onChange={this.ifEmpty.bind(this, "loginPassword", "form-control text-right")} className="form-control text-right" />
                                <label className="lblRight" htmlFor="loginPassword">رمز عبور</label>
                            </div>
                            <button type="submit"  className="lblRight btn btn-lg btn-info btn-block" >ورود</button>
                            <a className="text-left" href={`${window.location.origin}/ForgotPassword`}>کاربر یا رمز عبور را فراموش کردید؟</a>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

export default Login