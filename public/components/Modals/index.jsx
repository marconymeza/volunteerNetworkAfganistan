import React from 'react'
import { Input, Option, TextArea } from '../Controls'
export class Modal extends React.Component{
    render(){
        return(
            <div className="modal fade bd-example-modal-lg" id="mapOrg" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Select your ubication</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div id="map"></div>
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-info btn-lg btn-block" data-dismiss="modal" type="button">Close</button>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export class Email extends React.Component{
    render(){
        $(document).ready(function() {$('#mailMessage').summernote({height: 300,});}); 
        return(
            <div className="modal fade bd-example-modal-lg" id="modalMail" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">New email</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                    
                    <Input className="col" TextLabel="Subject" type="text" id="Title" inputClass="form-control " />                                        
                    <TextArea className="col" id="mailMessage" TextLabel="Message" textareaClass="form-control  r" />
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-info btn-lg btn-block" data-dismiss="modal" type="button">Send</button>
                    </div>
                </div>
            </div>
        </div>

        )
    }
}