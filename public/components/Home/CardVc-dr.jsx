import React from 'react'
import ReactTooltip from 'react-tooltip'
import uuidv1 from 'uuid/v1'
import md5 from 'md5'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class CardsVacancy extends React.Component{
    constructor(props){
        super(props)
    }

    selectVacancy(x, y) {
        if (y)
          document.getElementById(x).className = "card cardRed"
        else
          document.getElementById(x).className = "card"
      }
    render(){
        return(
            <div className={this.props.cols}>

              <div className="row text-center">
              <h3 id="noResult" className="redText text-center"></h3>
                {this.props.Vacancy.map(x => {
                  return (
                    <div className="col-lg-3 bottomSpace" key={uuidv1()}>
                      <ReactTooltip />
                      <div className="card cardRed " >
                      {x.IMAGE==null?
                        <div className="col-12 text-center imagePerfil"><img className="img-fluid" src={`/images/vacancyImages/defaultVacancy.jpg`} /></div>
                            :
                            <div className="col-12 text-center imagePerfil"><img className="img-fluid " onError={(e)=>{e.target.src="/images/vacancyImages/defaultVacancy.jpg"}} src={`/images/vacancyImages/${x.IMAGE}`} /></div>
                          }
                        <div className="card-body withoutPadding-bottom">
                          <h5 className="card-title"><strong>{x.TITLE.substring(0,26)}</strong></h5>
                          {/* {x.VACANCY_TYPE_DESCRIPTION == null || x.VACANCY_TYPE_DESCRIPTION == "" ? <p className="card-text tt colorGray">{x.VACANCY_TYPE_DESCRIPTION}</p> : <p className="card-text tt">{`${x.VACANCY_TYPE_DESCRIPTION.substring(0, 90)}...`}</p>} */}
                          {/* {x.DESCRIPTION == null || x.DESCRIPTION == "" ? <p className="card-text tt">{x.DESCRIPTION}</p> : <p className="card-text tt colorGray">{`${x.DESCRIPTION.substring(0, 90)}...`}</p>} */}
                          <p className="card-text withoutPadding withoutMargin"><strong>تاریخ نشر</strong> {x.START_DATE.substring(0,10)}</p>
                          <p className="card-text withoutPadding"><strong>تاریخ ختم</strong> {x.END_DATE.substring(0,10)}</p>
                          <span className="badge  redW marginRigth"><strong>{x.NO_OF_JOBS} </strong> بست ها</span>
                          <span className="badge redW marginRigth">{x.DURATION_DESCRIPTION}</span><br/>
                          <span className="badge redW marginRigth">{x.VACANCY_TYPE_DESCRIPTION}</span>
                          
                        </div><br />
                        <hr className="gradient"/>
                        <div className="viewProfile">
                          <Link to={'/perfilVacancy/'+x.VACANCY_ID}>مشاهده وظیفه</Link>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
              {this.props.getVacancy != null?
              <div className="row bottomSpace">
                <div className="col">
                  <hr />
                </div>
                <div className="col-1 text-center">
                <ReactTooltip />
                {this.props.clearSelection == 0?
                  <a className="button" onClick={this.props.getVacancy.bind(null,'/Get/VacancyHome/','Vacancy')}><i className="fas fa-plus fa-2x redText"></i></a>                 
                :
                <a className="button" onClick={this.props.clearSelectionFunc.bind(null)}  data-tip="Clean filter"><i className="fas fa-eraser fa-2x redText"></i></a>                

              }
                </div>
                <div className="col">
                  <hr />
                </div>
              </div>:
                  null
                }
            </div>
        )
    }
}

export default CardsVacancy