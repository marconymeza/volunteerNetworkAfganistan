const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ENTITY_OCCUPATION = sequelize.define('ENTITY_OCCUPATION', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },    
      OCCUPATION_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      OCCUPATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'ENTITY_OCCUPATION',
      timestamps: false,
      freezeTableName: true,
    });
    
 
module.exports.EntityOcupation = ENTITY_OCCUPATION


