const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ENTITY_ABILITIES = sequelize.define('ENTITY_ABILITIES', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      ABILITIES: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      ABILITIES_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'ENTITY_ABILITIES',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.EntityAbilities = ENTITY_ABILITIES


