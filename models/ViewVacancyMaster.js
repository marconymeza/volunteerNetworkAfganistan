const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;

    const VIEW_VACANCY_MASTER = sequelize.define('VIEW_VACANCY_MASTER', 
    {

      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      VACANCY_ID: 
      {
        type: Sequelize.STRING
      },
      VACANCY_TYPE: 
      {
        type: Sequelize.STRING
      },
      VACANCY_TYPE_DESCRIPTION: 
      {
        type: Sequelize.STRING,
        allowNull: false
      },
      TITLE: 
      {
        type: Sequelize.STRING
      },
      DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      NO_OF_JOBS: 
      {
        type: Sequelize.STRING
      },
      IMAGE: 
      {
        type: Sequelize.STRING
      },
      START_DATE: 
      {
        type: Sequelize.STRING
      },
      END_DATE: 
      {
        type: Sequelize.STRING
      },
      STATUS: 
      {
        type: Sequelize.STRING
      },
      DURATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      EDUCATION_REQUIRED: 
      {
        type: Sequelize.STRING
      },
      WORK_EXPERIENCE_REQUIRED: 
      {
        type: Sequelize.STRING
      },
      AREA_ID: 
      {
        type: Sequelize.STRING
      },
      AREA_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      JOB_ID: 
      {
        type: Sequelize.STRING
      },
      JOB_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      SCOPE: 
      {
        type: Sequelize.STRING
      },
      PROVINCE_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      NAME: 
      {
        type: Sequelize.STRING
      }
       
    },
    // Mas configuracion del modelo
    {
      tableName: 'VIEW_VACANCY_MASTER',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.ViewVacancy = VIEW_VACANCY_MASTER


