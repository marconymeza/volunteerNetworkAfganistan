const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const  PERIOD_DESCRIPTION = sequelize.define('PERIOD_DESCRIPTION', 
    {
      // definicion del los campos
      PERIOD_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      PERIOD_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'PERIOD_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.PeriodDescription = PERIOD_DESCRIPTION


