var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var sequelize = require('../../bin/ormConfig').sequelize;
var tablas = require('../../models/allModels').models
var app = express();
var Models = {
 ModAbilitiesDescription:   require('../../models/AbilitiesDescription2').AbilitiesDescription,
 ModEducationDescription:   require('../../models/EducationDescription').EducationDescription,
 ModGenderDescription:      require('../../models/GenderDescription').GenderDescription,
//  ModEntityDescription:      require('../../models/TypeEntityDesc').TypeDescription,
 ModNacionalityDescription: require('../../models/NacionalityDescription').NacionalityDescription,
 ModProfessionDescription:  require('../../models/ProfessionDescription').ProfessionDescription,
 ModProvinceDescription:    require('../../models/ProvinceDescription').ProvinceDescription,
 ModMaritalDescription:    require('../../models/MaritalDescription').MaritalDescription,
 ModSdgDescription:    require('../../models/SdgDescription').SdgDescription,
 ModOccupation:  require('../../models/OccupationDescription').OccupationDescription,
 ModTypeOrganizationDescription:require('../../models/TypeOrganizationDescription').TypeOrganizationDescription,
 ModBloodDescription: require('../../models/BloodGroupDescription').BloodGroupDescription,
 ModEducation: require('../../models/EducationDescription').EducationDescription,
 ModVacancyType: require('../../models/VacancyTypeDescription').VacancyTypeDescription,
 ModDurationDescription: require('../../models/DurationDescription').DurationDescription,
 ModWorkDescription: require('../../models/WorkExperienceDescription').WorkDescription,
 ModPeriod: require('../../models/PeriodDescription').PeriodDescription,
 NotificationCenter: require('../../models/NotificationCenter').NotificationCenter
}
 var ModVacancyExpertise= require('../../models/VacancyExpertise').VacancyExpertise;
 var ModEventExpertise= require('../../models/EventsVolunteer').EventsVolunteer;
const Op = Sequelize.Op;

router.post('/new/:tabla', function (req, res, next) {
  var includeAtributes = []
  if (req.body.tablas)
    req.body.tablas.map((x) => {
        includeAtributes.push({
          model: tablas[x.tabla],
          attributes: (x.campos)?x.campos:null,
          include: (!x.incluir)?[]:
          [{model: tablas[x.incluir[0].tabla],
          required: true,
          attributes: (x.incluir[0].campos)?x.incluir[0].campos:null
          }]
        }) 
    })
  tablas[req.params.tabla].findAll({
    include: includeAtributes,
    where:req.body.where,
    attributes:req.body.campos
  }).then(x => {
    res.status(200).send({ data: x })
  });
});


router.post('/api/obtener/:metodo/:campo/', function (req, res) {
  Models[String(req.params.metodo)].findAll({
    where: { [String(req.params.campo)]:{
      [Op.notIn]: ['99']
    }},
  }).then(x => {
    res.status(200).send({ data: x })
  });
})

router.post('/api/obtener/dp/:metodo/:campo/:Lenguaje', function (req, res) {
  Models[String(req.params.metodo)].findAll({
    where: { [String(req.params.campo)]:{
      [Op.notIn]: ['99']
    } ,
    [Op.and]: [
      { LANGUAGE: req.params.Lenguaje }
    ]
  },
  }).then(x => {
    res.status(200).send({ data: x })
  });
})

router.post('/NotificationCenter', function (req, res) {
  Models.NotificationCenter.findAll({
  }).then(x => {
    res.status(200).send({ data: x })
  });
})


router.get('/CountExpertiseVacancy/', function (req, res) {
  ModVacancyExpertise.findAll({
    group: ['AREA_ID','AREA_DESCRIPTION'],
    attributes: ['AREA_ID', [Sequelize.fn('COUNT', '0'), 'Conteo'],'AREA_DESCRIPTION'],
  }).then(x => {
    res.status(200).send({ data: x })
  });
});



  module.exports = router;