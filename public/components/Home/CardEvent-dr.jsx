import React from 'react'
import ReactTooltip from 'react-tooltip'
import uuidv1 from 'uuid/v1'
import md5 from 'md5'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class CardsEvents extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div className={this.props.cols}>

              <div className="row text-center">
              <h3 id="noResult" className="redText text-center"></h3>
                {this.props.Events.map(x => {
                  return (
                    <div className="col-lg-3 bottomSpace" key={uuidv1()}>
                      <ReactTooltip />
                      <div className="card cardRed" >
                      
                      {x.IMAGE==null?
                        <div className="col-12 text-center imagePerfil"><img className="img-fluid" src={`/images/activityImages/eventDefault.jpg`} /></div>
                            :
                        <div className="col-12 text-center imagePerfil"><img className="img-fluid " onError={(e)=>{e.target.src="/images/activityImages/eventDefault.jpg"}} src={`/images/activityImages/${x.IMAGE}`} /></div>
                            
                          }
                        <div className="card-body withoutPadding-bottom">
                          <h5 className="card-title"><strong>{x.TITULE.substring(0,26)}</strong></h5>
                          <p className="card-text withoutPadding withoutMargin"><strong>نام شخص مسول</strong></p>                          
                          <p className="card-text withoutPadding withoutMargin">{x.RESPONSIBLE_PERSON_NAME}<strong>:یوزر</strong></p>
                          <p className="card-text withoutPadding withoutMargin">{x.RESPONSIBLE_EMAIL}<strong>:ایمیل</strong></p>
                          <p className="card-text withoutPadding withoutMargin">{x.RESPONSIBLE_TELEPHONE}<strong>:شماره تیلفون</strong></p>                          
                        </div><br />
                        <hr className="gradient"/>
                        <div className="viewProfile">
                          <Link to={'/perfilEvent/'+x.EVENTS_ID}>رویداد را بیبینید</Link>
                        </div>
                      </div>
                    </div>
                  )

                })}
              </div>
              
              
                {this.props.getEvent != null?
                <div className="row bottomSpace">
                <div className="col">
                  <hr />
                </div>
                <div className="col-1 text-center">
                <ReactTooltip />
                {this.props.clearSelection == 0?
                  <a className="button" onClick={this.props.getEvent.bind(null,'/Get/EventHome/','Events')} ><i className="fas fa-plus fa-2x redText"></i></a>                 
                :
                <a className="button" onClick={this.props.clearSelectionFunc.bind(null)}  data-tip="Clean filter"><i className="fas fa-eraser fa-2x redText"></i></a>                

              }
                  </div>
                <div className="col">
                  <hr />
                </div>
              </div>:
                  null
                }
                
            </div>
        )
    }
}

export default CardsEvents