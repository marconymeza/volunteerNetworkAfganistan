
import React from 'react'
import md5 from 'md5'
import uuidv1 from 'uuid/v1'
import random from 'uuid/v1'
import cookie from 'react-cookies'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Modal from '../Modals'
import { Input, Option, TextArea } from '../Controls'
class Activity extends React.Component {

    constructor() {
        super()
        this.state = {
            EventsAll: [],
            IdEvent: "",
            EventObjetives: [],
            Areasofexpertise: [],
            Volunteers: [],
            Abilities: [],
            scope: [],
            volunteers: [],
            Province: []
        }
        this.img = null
        this.userInfo = cookie.load('dataUser')
        this.InsertEvent = this.InsertEvent.bind(this)
    }
    fillCombobox(url, array) {
        fetch(window.location.origin + '/Get/api/obtener/' + url, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    [array]: x.data
                })
            })
    }
    componentWillMount() {
        this.getDataEnvent()
        this.fillCombobox('ModProvinceDescription/PROVINCE_CODE', 'Province')
        this.fillCombobox('ModAbilitiesDescription/ABILITIES_CODE', 'Abilities')
    }
    getDataEnvent() {
        fetch(window.location.origin + '/Get/GetEvent/all/' + this.userInfo.ENTITY_ID, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    EventsAll: x.data

                })
            })
    }
    reload() {
        window.location.replace("/")
    }
    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }
    addItem(x) {
        this.state[x].push({ id: md5(document.getElementById(`txt${x}`).value), description: document.getElementById(`txt${x}`).value })
        this.setState({
            [x]: this.state[x]
        })
        document.getElementById("txt" + x).value = ""
    }
    deleteItem(array, item) {
        this.state[array].splice(item, 1)
        this.setState({
            [array]: this.state[array]
        })
    }
    renderScopes() {
        var arr = []
        $("input:checkbox[name=Scope]:checked").each(function () {
            arr.push({ description: $(this).val().split('#')[1], id: $(this).val().split('#')[0] })
        });
        this.setState({
            scope: arr
        })
    }
    InsertEvent(event) {
        event.preventDefault();
        if (document.getElementById('file-upload').files[0] != undefined)
            this.img = document.getElementById('file-upload').files[0].name
        var data = {
            IdEvent: this.state.IdEvent,
            Titule: document.getElementById('Titule').value,
            Entity: this.userInfo.ENTITY_ID,
            Events: md5(document.getElementById('Titule').value),
            StartDate: document.getElementById('StartDate').value,
            EndDate: document.getElementById('EndDate').value,
            Status: 1,
            img: this.img,
            Latitud: null,
            Longitude: null,
            EventObjetives: document.getElementById('EventObjetives').value,
            detailsEvent: document.getElementById('detailsEvent').value,
            Volunteers: this.state.Areasofexpertise,
            ReName: document.getElementById('ReName').value,
            TeNumber: document.getElementById('TeNumber').value,
            Email: document.getElementById('Email').value,
            scope: []

        }

        $("input:checkbox[name=Scope]:checked").each(function () {
            data.scope.push($(this).val().split('#')[0]);
        });
        if (data.Titule == "") {
            alertify.error('Title is required');

        } else if (data.Description == "") {
            alertify.error('Description is required');
        } else if (data.StartDate == "") {
            alertify.error('Date is required');
        } else if (data.EndDate == "") {
            alertify.error('Date is required');
        }
        else {
            fetch(window.location.origin + '/Insert/InsertEvent/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {

                            if (response.status == 200 || response.status == 202) {

                                alertify.success('Successful')
                                this.getDataEnvent()
                                setTimeout(
                                    function () {
                                        document.getElementById("formImgVacancy").submit()
                                    }, 2000);
                            }
                            else {
                                alertify.error('Failed')
                            }
                        })
                })
        }
        
    }
    update(x) {
        document.getElementById("viewEvent").click()
        fetch(window.location.origin + '/Get/GetEvent/1/' + x, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                if (x.data[0].IMAGE != null)
                    $("#blah").attr('src', '/images/activityImages/' + x.data[0].IMAGE)
                else
                    $("#blah").attr('src', '/images/Home/picture.png')


                this.state.IdEvent = x.data[0].EVENTS_ID
                document.getElementById('Titule').value = x.data[0].TITULE
                document.getElementById('StartDate').value = x.data[0].START_DATE.substring(0, 10)
                document.getElementById('EndDate').value = x.data[0].END_DATE.substring(0, 10)

                $('#EventObjetives').summernote('code', x.data[0].OBJECTIVES)
                $('#detailsEvent').summernote('code', x.data[0].DETAILS)
                document.getElementById('ReName').value = x.data[0].RESPONSIBLE_PERSON_NAME
                document.getElementById('TeNumber').value = x.data[0].RESPONSIBLE_TELEPHONE
                document.getElementById('Email').value = x.data[0].RESPONSIBLE_EMAIL
                x.data.map(x => {
                    x.EVENTS_VOLUNTEER['idDescription'] = `${x.EVENTS_VOLUNTEER.VOLUNTEER_ID}#${x.EVENTS_VOLUNTEER.VOLUNTEER_DESCRIPTION}`
                    x.EVENTS_SCOPE['idDescription'] = `${x.EVENTS_SCOPE.SCOPE}#${x.EVENTS_SCOPE.PROVINCE_DESCRIPTION.PROVINCE_DESCRIPTION}`
                })

                var t = []
                var tt = []

                var grouped = _.mapValues(_.groupBy(x.data, 'EVENTS_VOLUNTEER.idDescription'),
                    clist => clist.map(car => _.omit(car, 'EVENTS_VOLUNTEER.idDescription')));
                for (var propertyName in grouped) {
                    t.push({ description: propertyName.split('#')[1], id: propertyName.split('#')[0] })
                }
                var grouped = _.mapValues(_.groupBy(x.data, 'EVENTS_SCOPE.idDescription'),
                    clist => clist.map(car => _.omit(car, 'EVENTS_SCOPE.idDescription')));
                for (var propertyName in grouped) {
                    tt.push({ description: propertyName.split('#')[1], id: propertyName.split('#')[0] })
                }

                tt.map(x => {
                    document.getElementById(x.id).checked = true
                })
                this.setState({
                    scope: tt,
                    Areasofexpertise: t,
                })
            })




    }
    delete(x) {

        fetch(window.location.origin + '/Update/DeleteEvent/' + x, { method: 'post' })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202) {
                            this.getDataEnvent()
                            alertify.success('Successful')
                        }
                        else {
                            alertify.error('Failed')
                        }
                    })
            })
    }
    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }
    addOptionToList(x) {
        this.state[x].push({ id: document.getElementById(`list${x}`).value.split('#')[0], description: document.getElementById(`list${x}`).value.split('#')[1] })
        this.setState({
            [x]: this.state[x]
        })
    }
    deleteItem(array, item) {
        this.state[array].splice(item, 1)
        this.setState({
            [array]: this.state[array]
        })
    }
    RequestInformationVacancy(x) {
        fetch(window.location.origin + '/get/RequestInformation/ModRequestEvent/EVENTS_ID/' + x, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    volunteers: x.data
                })
            })
    }
    render() {
        $(document).ready(function () { $('#EventObjetives').summernote(); });
        $(document).ready(function () { $('#detailsEvent').summernote(); });
        $("#file-upload").change(function () { readURL(this); });
        return (
            <section id="Modulevacancy">
                <div className="container-fluid topSpace-2x">
                    <div className="row">
                        <div className="col-12">
                            <div className="pos-f-t">
                                <div className="collapse" id="navbarToggleExternalContent">
                                    <div className="bg-darks p-4">
                                        <table className="table table-darks">
                                            <thead >
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Title of Event</th>
                                                    <th scope="col">Start Date Event</th>
                                                    <th scope="col">End Date Event</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody >


                                                {this.state.EventsAll.map((x, y) => {
                                                    return (
                                                        <tr key={uuidv1()}>
                                                            <th scope="row">{y + 1}</th>
                                                            <th scope="row">{x.TITULE}</th>
                                                            <th scope="row">{x.START_DATE.substring(0, 10)}</th>
                                                            <th scope="row">{x.END_DATE.substring(0, 10)}</th>
                                                            <th >
                                                                <div className="row text-center">
                                                                    <div className="col-2 text-center">
                                                                        <a className="whiteText" href="#" onClick={this.update.bind(this, x.EVENTS_ID)} ><i className="fas fa-edit fa-1x whiteText"></i><br />Edit</a>
                                                                    </div>
                                                                    <div className="col-2 text-center">
                                                                        <a className="whiteText" onClick={this.delete.bind(this, x.EVENTS_ID)} href="#"><i className="fas fa-times fa-1x whiteText"></i> <br />Delete</a>
                                                                    </div>
                                                                    <div className="col-2 text-center">
                                                                        <a className="whiteText" data-toggle="modal" data-target="#seeVolunteer" onClick={this.RequestInformationVacancy.bind(this, x.EVENTS_ID)} href="#"><i className="fas fa-users fa-1x"></i> <br />Volunteers</a>
                                                                    </div>
                                                                </div>

                                                            </th>
                                                        </tr>
                                                    )
                                                })}

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <nav className="navbar navbar-dark bg-dark">
                                    <button id="viewEvent" className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i className="fas fa-eye"></i> View Table of Events
                                    </button>
                                </nav>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div className="card border-info mb-4" >
                                    <div className="card-header blueIcon"><h5>Event</h5> </div>
                                    <div className="card-body text-info">
                                        <div className="row">

                                            <div className="col text-center">
                                                <form id="formImgVacancy" name="formImgVacancy" encType="multipart/form-data" action="api/upload/activity" method="post">
                                                    <img src="/images/Home/picture.png" className="img-fluid" id="blah" /><br />
                                                    <label htmlFor="file-upload" className="custom-file-upload">
                                                        <i className="fas fa-image"></i> Select a Photo
                                                    </label>
                                                    <input type="file" id="file-upload" className="btn btn-primary" name="imgUploader" />
                                                    <br />
                                                </form>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <br />
                                            <form className="EventForm" onSubmit={this.InsertEvent}>
                                                <div className="row">

                                                    <Input className="col-md-4 col-lg-4" TextLabel="Title of Event" type="text" id="Titule" inputClass="form-control" />
                                                    <Input className="col-md-4 col-lg-4" TextLabel="Start Date of Event" type="date" id="StartDate" inputClass="form-control" />
                                                    <Input className="col-md-4 col-lg-4" TextLabel="End Date of Event" type="date" id="EndDate" inputClass="form-control" />
                                                    <Input className="col-md-4 col-lg-4" TextLabel="Responsible Person Name" type="text" id="ReName" inputClass="form-control" />
                                                    <Input className="col-md-4 col-lg-4" TextLabel="Telephone Number" type="text" id="TeNumber" inputClass="form-control" />
                                                    <Input className="col-md-4 col-lg-4" TextLabel="Email" type="email" id="Email" inputClass="form-control" />
                                                </div>

                                                <div className="row topSpace-2x">
                                                    <div className="col">
                                                        <div className="form-group purple-border">
                                                            <label id="DescriptionLab" htmlFor="exampleFormControlTextarea4">Objectives of Event</label>
                                                            <textarea id="EventObjetives" name="editordata"></textarea>
                                                        </div>
                                                    </div>
                                                    <div className="col">
                                                        <div className="form-group purple-border">
                                                            <label id="DescriptionLab" htmlFor="exampleFormControlTextarea4">Details of Event</label>
                                                            <textarea id="detailsEvent" name="editordata"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row ">
                                                    <div className="col-3">
                                                        <button type="button" className="btn btn-info btn-sm" data-toggle="modal" data-target="#Volunteers">Volunteers Required</button>
                                                        <ul >
                                                            {this.state.Areasofexpertise.map((x, y) => {
                                                                return (
                                                                    <li key={random()}><p name="Ars" id={"Area" + y}>{x.description}</p></li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </div>
                                                    <div className="col-3">
                                                        <button type="button" className="btn btn-info btn-sm" data-toggle="modal" data-target="#scopes">Location(s) <i className="fas fa-map-marker fa-1x inLine"></i></button>
                                                        <ul >
                                                            {this.state.scope.map((x, y) => {
                                                                return (
                                                                    <li key={random()}><p name="Ars" id={"Scope" + y}>{x.description}</p></li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </div>
                                                </div>
                                                {/* <div className="row topSpace-2x">
                                                    <div className="col">
                                                        <label id="LongitudeLab" htmlFor="Longitude">Longitude <a data-toggle="modal" data-target="#mapOrg"> (Use map, click here)</a></label>

                                                        <input type="text" id="Longitud" className="form-control Md-Select " />
                                                    </div>
                                                    <div className="col">
                                                        <label id="LatitudLab" htmlFor="Latitud">Latitude <a data-toggle="modal" data-target="#mapOrg"> (Use map, click here)</a></label>

                                                        <input type="text" id="Latitud" className="form-control Md-Select " />
                                                    </div>
                                                    <Modal />
                                                </div> */}
                                                <button className="btn btn-info btn-lg btn-block" type="submit">Save Event
                                    <i className="fas fa-save ml-2"></i>
                                                </button>
                                            </form>

                                            
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>

                    </div>

                </div>

                {/*****************************************************************MODAL SCOPE**************************************************************/}
                <div className="modal fade bd-example-modal-lg" id="scopes" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Select Duty Station</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {this.state.Province.map(x => {
                                    return (
                                        <div key={md5(x.PROVINCE_CODE)} onChange={this.renderScopes.bind(this)} className="form-check form-check-inline">
                                            <input className="form-check-input" id={x.PROVINCE_CODE} name="Scope" type="checkbox" value={`${x.PROVINCE_CODE}#${x.PROVINCE_DESCRIPTION}`} />
                                            <label className="form-check-label" htmlFor="defaultCheck1">
                                                {x.PROVINCE_DESCRIPTION}
                                            </label>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info btn-lg btn-block" data-dismiss="modal" type="button">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/***************************************************************MODAL AREA******************************************************/}
                <div className="modal fade" id="Volunteers" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Volunteers Required</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="listAreasofexpertise">You can also select an ability in the list</label>
                                            <select className="form-control" id="listAreasofexpertise" onChange={this.addOptionToList.bind(this, "Areasofexpertise")}>
                                                <option value="99" >Select one</option>
                                                {this.state.Abilities.map(x => {
                                                    return (
                                                        <option key={random()} value={`${x.ABILITIES_CODE}#${x.ABILITIES_DESCRIPTION}`}>{x.ABILITIES_DESCRIPTION}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <a onClick={this.addItem.bind(this, "Areasofexpertise")} ><i className="fas fa-plus blueText "></i></a>
                                                </div>
                                            </div>
                                            <input type="text" id="txtAreasofexpertise" className="form-control" placeholder="New Abilities" />

                                        </div>
                                    </div>
                                </div>
                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.state.Areasofexpertise.map((x, y) => {
                                                return (
                                                    <li id="ListArea" key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Ars" id={"Area" + y}>{x.description}</span>
                                                        <span>
                                                            <a href="#" onClick={this.deleteItem.bind(this, "Areasofexpertise", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                        </span>

                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/*****************************************************************SEE PEOPPLE**************************************************************/}
                <div className="modal fade" id="seeVolunteer" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5>Volunteers</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-darks">
                                    <tbody >
                                        {this.state.volunteers.map(x => {
                                            return (
                                                <tr key={random()}>
                                                    {x.ENTITY_MASTER.NAME == "" ?
                                                        <th scope="row" className="whiteText"><Link to={'/perfilVolunteer/' + x.ENTITY_MASTER.ENTITY_ID}>Nameless</Link>  </th>
                                                        :
                                                        <th scope="row" className="whiteText"><Link to={'/perfilVolunteer/' + x.ENTITY_MASTER.ENTITY_ID}>{x.ENTITY_MASTER.NAME}</Link>  </th>

                                                    }
                                                </tr>
                                            )
                                        })}
                                    </tbody>

                                </table>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Activity