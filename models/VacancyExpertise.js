const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VACANCY_EXPERTISE = sequelize.define('VACANCY_EXPERTISE', 
    {
      // definicion del los campos
      VACANCY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },    
      AREA_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      AREA_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VACANCY_EXPERTISE',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.VacancyExpertise = VACANCY_EXPERTISE


