var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var ModEntity = require('../../models/EntityMaster').EntityMaster;
var ModLogin = require('../../models/Login').Login;
var ModSdg = require('../../models/EntitySdg').EntitySDG
var ModEntityScope = require('../../models/EntityScope').EntityScope;
var ModAbilities = require('../../models/EntityAbilities').EntityAbilities;
var ModVacancy = require('../../models/VacancyAnnouncement').VacancyAnnouncement;
var ModEvents = require('../../models/Events').Events;
var ModProfession = require('../../models/EntityProfession').EntityProfession;
var ModOccupation = require('../../models/EntityOccupation').EntityOcupation;
var ModMessenger = require('../../models/Messenger').Messenger
var ModRequestEntity = require('../../models/RequestEntity').RequestEntity;
var ModRequestEvent = require('../../models/RequestEvents').RequestEvents;
var ModRequestVacancy = require('../../models/VacancyRequest').VacancyRequest;
var ModNotificationCenter = require('../../models/NotificationCenter').NotificationCenter;
var multer = require('multer');
const Op = Sequelize.Op;
var app = express();

var Models = {
  ModRequestVacancy: require('../../models/VacancyRequest').VacancyRequest,
  ModRequestEvent: require('../../models/RequestEvents').RequestEvents,
  ModRequestEntity: require('../../models/RequestEntity').RequestEntity,
  ModNotificationCenter:require('../../models/NotificationCenter').NotificationCenter,
}
router.post('/UpdateDocument/:Entity/:url', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModEntity.update({ DOCUMENT: req.params.url },
      {
        where: { ENTITY_ID: req.params.Entity }
      }).then(x => {
          return 200
      }).catch(error => {
        return 400
      })
    return Result
  })
  queue.then(function (lastResponse) {

    res.status(lastResponse).send({ data: true })
  })
})
router.post('/Confirmation/:Email/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModEntity.update({ STATUS: 1 },
      {
        where: { EMAIL: req.params.Email }
      }).then(x => {
        ModLogin.update({ STATUS: 1 },
          {
            where: {
              EMAIL: req.params.Email
            }
          }).then(y => {
            return y

          })
          return x
      }).catch(error => {
        return 400
      })
    return Result
  })
  queue.then(function (lastResponse) {

    res.status(200).send({ data: lastResponse })
  })
})
router.post('/DeleteEvent/:ID/', function (req, res) {
  ModEvents.update({ STATUS: 0 },
    {
      where: { EVENTS_ID: req.params.ID }
    }).then(x => {
      res.status(200).send({ data: x })
    });


})
router.post('/DeleteVacancy/:ID/', function (req, res) {
  ModVacancy.update({ STATUS: 0 },
    {
      where: { VACANCY_ID: req.params.ID }
    }).then(x => {
      res.status(200).send({ data: x })
    });
})
router.post('/updateChat/:ID/', function (req, res) {
  ModMessenger.update({ REMMIT_STATUS: false },
    {
      where: { CURR_NO: req.params.ID }
    }).then(x => {

      res.status(200).send({ data: x })
    });
})
router.post('/UpdateInformation/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModEntity.update({
      NAME: req.body.name,
      GENDER: req.body.Gender,
      LEGAL_ID: req.body.LegalId,
      TELEPHONE: req.body.Telephone,
      NAME_RESPONSIBLE: req.body.Nresponsible,
      TELEPHONE_RESPONSIBLE: req.body.NTelephone,
      NACIONALITY: req.body.Nacionality,
      MARITAL_STATUS: req.body.Marital,
      FACEBOOK: req.body.Facebook,
      TWITTER: req.body.Twitter,
      GOOGLE: req.body.Google,
      WEBSITE: req.body.Website,
      INSTAGRAM: req.body.Instagram,
      BIRTH_DATE: req.body.Birth,
      PROVINCE: req.body.Province,
      EDUCATION: req.body.Education,
      FREE_ADDRESS: req.body.Address,
      LONGITUDE: req.body.Longitud,
      LATITUD: req.body.Latitud,
      TYPE_ORGANIZATION: req.body.TypeOrganization,
      WORK_AREA: req.body.WorkArea,
      BLOOD_GROUP: req.body.Blood,
      COMMENTARY: req.body.Commentary,
      EMAIL: req.body.Email,
      IMAGE: req.body.img,
      DISTRICT: req.body.District,
      PERIOD: req.body.Period,
      VILLAGE: req.body.Village
    },
      {
        where:
        {
          ENTITY_ID: req.body.Entity
        }
      }).then(anotherTask => {
        if (req.body.TypeEntity == 1) {
          req.body.Scope.map(x => {
            ModEntityScope.upsert({
              ENTITY_ID: req.body.Entity,
              SCOPE: x
            },
              {
                where:
                {
                  ENTITY_ID: req.body.Entity
                }
              }).then(anotherTask => {
                return 202
              })

          })
          if (req.body.Sdg != "") {
            req.body.Sdg.map(x => {
              ModSdg.upsert({
                ENTITY_ID: req.body.Entity,
                SDG: x
              },
                {
                  where:
                  {
                    ENTITY_ID: req.body.Entity
                  }
                }).then(anotherTask => {
                  return 202
                })

            })
          }
        } if (req.body.TypeEntity == 2) {
          if (req.body.Abilities != "") {
            req.body.Abilities.map(x => {
              ModAbilities.upsert({
                ENTITY_ID: req.body.Entity,
                ABILITIES: x.id,
                ABILITIES_DESCRIPTION: x.description
              },
                {
                  where:
                  {
                    ENTITY_ID: req.body.Entity
                  }
                }).then(anotherTask => {
                  return 202
                })

            })
          }
          if (req.body.Sdg != "") {
            req.body.Sdg.map(x => {
              ModSdg.upsert({
                ENTITY_ID: req.body.Entity,
                SDG: x.id,
                SDG_DESCRIPTION: x.description
              },
                {
                  where:
                  {
                    ENTITY_ID: req.body.Entity
                  }
                }).then(anotherTask => {
                  return 202
                })

            })
          }
          if (req.body.Profession != "") {
            req.body.Profession.map(x => {
              ModProfession.upsert({
                ENTITY_ID: req.body.Entity,
                PROFESSION_ID: x.id,
                PROFESSION_DESCRIPTION: x.description
              },
                {
                  where:
                  {
                    ENTITY_ID: req.body.Entity
                  }
                }).then(anotherTask => {
                  return 202
                })

            })
          }
          if (req.body.Occupation != "") {
            req.body.Occupation.map(x => {
              ModOccupation.upsert({
                ENTITY_ID: req.body.Entity,
                OCCUPATION_ID: x.id,
                OCCUPATION_DESCRIPTION: x.description
              },
                {
                  where:
                  {
                    ENTITY_ID: req.body.Entity
                  }
                }).then(anotherTask => {
                  return 202
                })

            })
          }
        }
        var Storage = multer.diskStorage({
          destination: function (req, file, callback) {
            callback(null, __dirname + '../../../public/images/perfilimages/');
          },
          filename: function (req, file, callback) {
            callback(null, req.body.Entity);
          }
        });

        var upload = multer({ storage: Storage }).array("imgUploader", 3);
        ModLogin.update({
          USER_NAME: req.body.User,
          PASSWORD: req.body.PassWord,
          EMAIL: req.body.Email
        },
          {
            where:
            {
              ENTITY_ID: req.body.Entity
            }
          }).then(anotherTask => {
            queue = queue.then(function () {
              return 200
            })
          });
        return 202
      }).catch(error => {
        console.log(error)
        return 500
      })
    return Result
  });

  queue.then(function (lastResponse) {
    // console.log(lastResponse)
    res.status(lastResponse).send({ data: true })

  })

})
router.post('/UpdateNotification/:model/:colum/:ID/', function (req, res) {
  Models[String(req.params.model)].update({ STATUS : 0 },
    {
      where: { [String(req.params.colum)]: req.params.ID }
    }).then(x => {
      res.status(200).send({ data: x })
    });


})
router.post('/UnFollowEntity/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModRequestEntity.update({
      REQUEST_STATUS: 0
    },{
      where:{ENTITY_ID: req.body.Entity, RELATIONAL_ENTITY: req.body.Rentity}
    }).then(x => {return 200}).catch(error => {return 400})
    return Result
  })
  queue.then(function (lastResponse) {

    res.status(lastResponse).send({ data: true })
  })
})
router.post('/UnFollowEvent/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModRequestEvent.update({
      REQUEST_STATUS: 0
    },{
      where:{EVENTS_ID: req.body.Event, RELATIONAL_ENTITY: req.body.Rentity}
    }).then(x => {
      return 200

    }).catch(error => {
      return 400
    })
    return Result
  })
  queue.then(function (lastResponse) {

    res.status(lastResponse).send({ data: true })
  })
})
router.post('/UnFollowVacancy/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModRequestVacancy.update({
      REQUEST_STATUS: 0
    },{
      where:{VACANCY_ID: req.body.Vacancy, RELATIONAL_ENTITY: req.body.Rentity}
    }).then(x => {
      return 200

    }).catch(error => {
      return 404
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: true })
  })
})
module.exports = router;


