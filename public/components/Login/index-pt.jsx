import React from 'react'
import cookie from 'react-cookies'

class LoginPt extends React.Component {
    constructor() {
        super()
        this.state = {
            Login:[]
        }
        this.Login = this.Login.bind(this)
    }
    Login(event) {
        event.preventDefault();
        var data = {
            Password: document.getElementById('loginPassword').value,
            Email: document.getElementById('loginEmail').value
        }
        if (data.Email == "") {
            alertify.error('ایمیل یا د کاروړونکي نوم اړین دی');
            document.getElementById("loginEmail").className = "form-control error"

        } else if (data.Password == "") {
            alertify.error('پاسورډ اړین دی');
            document.getElementById('loginPassword').className = "form-control error"
        }
        else {
            fetch(window.location.origin + '/Get/ValidationLogin/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status==200) {
                                    alertify.success('کړنه بریالۍ وه')
                                    cookie.save('dataUser', JSON.stringify(x.data[0]), { path: '/' })
                                    setTimeout(() => {
                                        window.open('/panel', "_self")
                                      }, 2000);
                            }else if(response.status==404){
                                
                                alertify.error('مهرباني وکړئ خپل برېښنالیک تایید کړئ')
                            }else if(response.status==500){
                                
                                alertify.error('د کاروړونکي نوم یا ایمیل ناسم دی')
                            }
                        })
                })
        }



    }
    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }
    render() {
        return (
            <section id="Login">
                <div className="container-fluid topSpace">
                    <div className="row text-right" >
                        <form name="login form-signin" className="form-signin"onSubmit={this.Login}>
                            <img className="img-fluid" src="/images/Home/LogoColor.png" />
                            <h1 className="h3 mb-3 font-weight-normal">مهرباني وکړئ دننه شئ</h1>
                            <div className="md-form">
                                <input type="text" id="loginEmail" onChange={this.ifEmpty.bind(this, "loginEmail", "form-control text-right")} className="form-control text-right"/>
                                <label className="lblRight" htmlFor="loginEmail">استعمالونکی/یوزر یا بریښنلیک</label>
                            </div>
                            <div className="md-form">
                                <input type="password" id="loginPassword" onChange={this.ifEmpty.bind(this, "loginPassword", "form-control text-right")} className="form-control text-right" />
                                <label className="lblRight" htmlFor="loginPassword">پاسورډ </label>
                            </div>
                            <button type="submit"  className="lblRight btn btn-lg btn-info btn-block" >دننه کیدل</button>
                            <a className="text-left" href={`${window.location.origin}/ForgotPassword`}> د کاروړونکي نوم یا پاسورډ مو له یاده وتلی؟</a>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

export default LoginPt