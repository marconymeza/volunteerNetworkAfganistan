const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VACANCY_JOB = sequelize.define('VACANCY_JOB', 
    {
      // definicion del los campos
      VACANCY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },    
      JOB_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      JOB_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VACANCY_JOB',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.VacancyJob = VACANCY_JOB


