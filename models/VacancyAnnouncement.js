const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VACANCY_ANNOUNCEMENT = sequelize.define('VACANCY_ANNOUNCEMENT', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING

      },
      VACANCY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      VACANCY_TYPE: 
      {
        type: Sequelize.NUMERIC
      },
      TITLE: 
      {
        type: Sequelize.STRING
      },
      DESCRIPTION: 
      {
        type: Sequelize.STRING,
        allowNull: false
      },
      IMAGE: 
      {
        type: Sequelize.STRING
      },
      START_DATE: 
      {
        type: Sequelize.DATE
      },
      END_DATE: 
      {
        type: Sequelize.DATE
      },
      STATUS: 
      {
        type: Sequelize.NUMERIC
      },
      DURATION: 
      {
        type: Sequelize.STRING
      },
      EDUCATION_REQUIRED: 
      {
        type: Sequelize.STRING
      },
      NO_OF_JOBS: 
      {
        type: Sequelize.NUMERIC
      }, 
      INCENTIVES: 
      {
        type: Sequelize.STRING
      },       
      WORK_EXPERIENCE_REQUIRED: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VACANCY_ANNOUNCEMENT',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.VacancyAnnouncement = VACANCY_ANNOUNCEMENT


