import React from 'react'
import md5 from 'md5'
import random from 'uuid/v1'

class Register extends React.Component {

    constructor() {
        super()
        this.state = {
            Gender: [],
            Country: [],
            Type: []
        }
    }

    componentWillMount() {

        fetch(window.location.origin + '/Get/api/obtener/ModGenderDescription/GENDER_CODE', { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Gender: x.data.splice(0, 2)
                })
            })

        fetch(window.location.origin + '/Get/api/obtener/ModNacionalityDescription/NACIONALITY_CODE', { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Country: x.data
                })
            })

        fetch(window.location.origin + '/Get/api/obtener/ModTypeOrganizationDescription/TYPE_ORGANIZATION_CODE', { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Type: x.data
                })
            })

        fetch(window.location.origin + '/Get/api/obtener/ModBloodDescription/BLOOD_GROUP_CODE', { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Blood: x.data
                })
            })
    }

    reload() {
        window.location.replace("/");
    }

    SaveOrganization(A, G) {
        var data = {
            name: document.getElementById('Name').value,
            Email: document.getElementById('Email').value,
            LegalId:"",
            Telephone: "",
            Nresponsible: "",
            NTelephone: "",
            PassWord: document.getElementById('PassWord').value,
            User: document.getElementById('User').value,
            TypeOrganization: 99,
            Commentary:"",
            Gender: G,
            Status: 0,
            TypeEntity: A,
            Key: random()


        }
        if (data.Email == "" || this.validateEmail(document.getElementById('Email').value) == false) {
            alertify.error('Email is required');
            document.getElementById('Email').className = "form-control error"
        } else if (data.User == "") {
            alertify.error('Username is required');
            document.getElementById('User').className = "form-control error"
        } else if (data.PassWord == "") {
            alertify.error('Password is required');
            document.getElementById('PassWord').className = "form-control error"
        }
        else {
            
            fetch(window.location.origin + '/Insert/InsertOrganization/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status == 200 || response.status == 202) {
                                $('#organizationForm').modal('hide')
                                $('#confirm').modal('show')
                                alertify.success('Successful')
                              
                            } else if (response.status == 406) {
                                alertify.error('The mail or user already exists')
                            }
                            else {
                                alertify.error('Failed')
                            }
                        })
                })
        }



    }


    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    SaveVolunteer(A) {
        var data = {
            name: document.getElementById('Name2').value,
            Email: document.getElementById('Email2').value,
            LegalId: "",
            Telephone: "",
            Gender: 99,
            Country: 99,
            // MaritalStatus: document.getElementById('MaritalStatus2').value,
            PassWord: document.getElementById('PassWord2').value,
            User: document.getElementById('User2').value,
            Commentary: "",
            Status: 0,
            TypeEntity: A,
            Key: random()


        }
        if (data.Email == "" || this.validateEmail(document.getElementById('Email2').value) == false) {
            alertify.error('Email is required');
            document.getElementById('Email2').className = "form-control error"
        }else if (data.PassWord == "") {
            alertify.error('Password is required');
            document.getElementById('PassWord2').className = "form-control error"
        } else if (data.User == "") {
            alertify.error('User is required');
            document.getElementById('User2').className = "form-control error"
        }
        else {
            fetch(window.location.origin + '/Insert/InsertOrganization/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status == 200 || response.status == 202) {
                                $('#volunteerForm').modal('hide')
                                $('#confirm').modal('show')
                                // fetch(window.location.origin + '/Email/api/correo/'+data.Email+'/Confirmation Email/Please Open link', { method: 'post' })
                                // .then((response) => {
                                alertify.success('Successful')
                                // })
                            }
                            else if (response.status == 406) {
                                alertify.error('The mail or user already exists')
                            }
                            else {
                                alertify.error('Failed')
                            }
                        })
                })
        }

    }

    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }

    render() {

        return (
            <section id="particles-js" className="Register">

                <div className="container text-center">
                    <div className="row  flotante">
                        <div className="col-12 topSpace bottomSpace">
                            <h1>Who are you?</h1>
                        </div>
                        <div className="col-12 col-md-4 offset-md-2">
                            <a href="#" data-toggle="modal" data-target="#organizationForm" >
                                <i className="fas fa-building fa-10x"></i>
                                <h3>Organization</h3>
                            </a>
                        </div>
                        <div className="col-12 topSpace d-md-none d-lg-none d-xl-none"></div>
                        <div className="col-12 col-md-4">
                            <a href="#" data-toggle="modal" data-target="#volunteerForm">
                                <i className="fas fa-user fa-10x"></i>
                                <h3>Volunteer</h3>
                            </a>
                        </div>
                    </div>
                </div>

                {/* Modal Organization */}
                <div className="modal fade" id="organizationForm" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title" id="exampleModalLongTitle"><i className="fas fa-building fa-lg"></i> Register Organization</h6>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">

                                <div className="md-form">
                                    <input type="text" id="Name" onChange={this.ifEmpty.bind(this, "User", "form-control")} className="form-control" />
                                    <label htmlFor="Name">Full Name</label>
                                </div>
                                <div className="md-form">
                                    <input type="text" id="User" onChange={this.ifEmpty.bind(this, "User", "form-control")} className="form-control" />
                                    <label htmlFor="User">User Name</label>
                                </div>
                                <div className="md-form">
                                    <input type="password" onChange={this.ifEmpty.bind(this, "PassWord", "form-control")} id="PassWord" className="form-control" />
                                    <label htmlFor="password">Password</label>
                                </div>

                                <div className="md-form">
                                    <input type="email" onChange={this.ifEmpty.bind(this, "Email", "form-control")} id="Email" className="form-control" />
                                    <label htmlFor="email">Email</label>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <div className="text-center mt-4 mb-2">
                                    <button className="btn btn-danger" data-dismiss="modal">Cancel
                        <i className="fas fa-times ml-2"></i>
                                    </button>
                                </div>

                                <div className="text-center mt-4 mb-2">
                                    <button onClick={this.SaveOrganization.bind(this, 1, 3)} className="btn btn-info">Send
                        <i className="fa fa-send ml-2"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                {/* Modal Volunteer */}
                <div className="modal fade" id="volunteerForm" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title" id="exampleModalLongTitle"><i className="fas fa-user fa-lg"></i> Register Volunteer</h6>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                            
                            <div className="md-form">
                                    <input type="text" id="Name2" className="form-control" onChange={this.ifEmpty.bind(this, "Name2", "form-control")} />
                                    <label htmlFor="Name2">Full Name</label>
                                </div>
                                <div className="md-form">
                                    <input type="text" id="User2" className="form-control" onChange={this.ifEmpty.bind(this, "User2", "form-control")} />
                                    <label htmlFor="User2">User Name</label>
                                </div>
                                <div className="md-form">
                                    <input type="password" id="PassWord2" onChange={this.ifEmpty.bind(this, "PassWord2", "form-control")} className="form-control" />
                                    <label htmlFor="password">Password</label>
                                </div>
                                <div className="md-form">
                                    <input type="email" id="Email2" onChange={this.ifEmpty.bind(this, "Email2", "form-control")} className="form-control" />
                                    <label htmlFor="email">Email</label>
                                </div>
                            </div>

                            <div className="modal-footer">
                                <div className="text-center mt-4 mb-2">
                                    <button className="btn btn-danger" data-dismiss="modal">Cancel
                                        <i className="fas fa-times ml-2"></i>
                                    </button>
                                </div>

                                <div className="text-center mt-4 mb-2">
                                    <button onClick={this.SaveVolunteer.bind(this, 2)} className="btn btn-info">Send
                                        <i className="fa fa-send ml-2"></i>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                {/* Modal confirm */}
                <div className="modal fade" id="confirm" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h6 className="modal-title" id="exampleModalLongTitle">Your registration have been successful</h6>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body text-center">
                                <i className="animated bounceIn far fa-check-circle fa-10x"></i><br /><br />
                                <p>Please, go to your mail to confirm your account</p>
                            </div>
                            <div className="modal-footer">
                                <div className="text-center mt-4 mb-2">
                                    <button onClick={this.reload.bind(this)} className="btn btn-info">Close
                
                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        )
    }
}

export default Register