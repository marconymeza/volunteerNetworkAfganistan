if(document.location.pathname == '/home' || document.location.pathname == '/vacancy' || document.location.pathname == '/activity')
document.getElementById(document.location.pathname.substring(1)).className = "selectedMenu nav-link whiteColor"

function initMap() {
  var uluru = {lat: 34.52500551623279, lng: 62.158688900000016};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    draggable: true,
    map: map
  });
google.maps.event.trigger(map, 'resize');
google.maps.event.addListener(marker, 'dragend', function (event) {
    document.getElementById("Latitud").value = event.latLng.lat();
    document.getElementById("Longitud").value = event.latLng.lng();
});
  return marker
}

// $('canvas').ready( function(){
//   jdenticon.update('.settings-avatar');
//   jdenticon.update('.userpic');
// });




// $(document).ready(function () {
//   var options = {
//       beforeSubmit: showRequest,  // pre-submit callback
//       success: showResponse  // post-submit callback
//   };

//   // bind to the form's submit event
//   $('#frmUploader').submit(function () {
//     console.log(options)
//       $(this).ajaxSubmit(options);
//       // always return false to prevent standard browser submit and page navigation
//       return false;
//   });
// });

// // pre-submit callback
// function showRequest(formData, jqForm, options) {
//   alert('Uploading is starting.');
//   return true;
// }

// // post-submit callback
// function showResponse(responseText, statusText, xhr, $form) {
//   console.log('status: ' + statusText + '\n\nresponseText: \n' + responseText)
// }

// window.onscroll = function() {myFunction()};

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#blah').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
  }
}

$("#box").hover(function(){
  $("#panels").slideToggle();
}, function(){
  $("#panels").slideToggle();
});