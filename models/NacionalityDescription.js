const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const NACIONALITY_DESCRIPTION = sequelize.define('NACIONALITY_DESCRIPTION', 
    {
      // definicion del los campos
      NACIONALITY_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      NACIONALITY_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'NACIONALITY_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.NacionalityDescription = NACIONALITY_DESCRIPTION


