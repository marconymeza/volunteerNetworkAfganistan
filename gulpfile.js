var gulp = require('gulp'),
    watch = require('gulp-watch');
    var run = require('gulp-run-command').default;

    gulp.task('clean', run('npm run build'))
    gulp.task('server', run('npm start'))
    gulp.task('callback', function () {
        // Callback mode, useful if any plugin in the pipeline depends on the `end`/`flush` event
        return watch('public/components/**/*.jsx', function () {
            gulp.start('clean');
        });
    });

    gulp.task('dev', ['server', 'callback']);