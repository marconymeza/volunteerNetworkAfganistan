const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VACANCY_REQUEST = sequelize.define('VACANCY_REQUEST', 
    {
      // definicion del los campos
      VACANCY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },    
      RELATIONAL_ENTITY: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      CURRICULUM_URL: 
      {
        type: Sequelize.STRING
      },
      REQUEST_STATUS: 
      {
        type: Sequelize.NUMERIC
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VACANCY_REQUEST',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.VacancyRequest = VACANCY_REQUEST


