const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const DISTRICT_DESCRIPTION = sequelize.define('DISTRICT_DESCRIPTION', 
    {
      // definicion del los campos
      PROVINCE_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      DISTRICT_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      DISTRICT_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'DISTRICT_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.DistrinctDescription = DISTRICT_DESCRIPTION


