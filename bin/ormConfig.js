const Sequelize = require('sequelize');


//PRODUCCION
// const sequelize = new Sequelize('DB_A12F0A_ae', 'DB_A12F0A_ae_admin', 'N1ct@3lS', {
//     host: 'sql5014.site4now.net',
//     dialect: 'mssql',
//     pool: {
//         max: 5,
//         min: 0,
//         acquire: 30000,
//         idle: 10000
//     },
// });

//DESARROLLO
const sequelize = new Sequelize('DB_A12F0A_marconymeza', 'DB_A12F0A_marconymeza_admin', 'N1ct@3lS', {
    host: 'sql5032.site4now.net',
    dialect: 'mssql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

    sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err);
    });
module.exports.sequelize = sequelize



