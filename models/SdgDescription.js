const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const SDG_DESCRIPTION = sequelize.define('SDG_DESCRIPTION', 
    {
      // definicion del los campos
      SDG_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      SDG_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'SDG_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.SdgDescription = SDG_DESCRIPTION


