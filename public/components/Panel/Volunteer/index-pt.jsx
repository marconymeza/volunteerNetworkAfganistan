import React from 'react'
import md5 from 'md5'
import uuidv1 from 'uuid/v1'
import random from 'uuid/v1'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class Volunteer extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            Events:[],
            Vacancies:[],
            Entities:[]
        }
    }
    componentWillMount(){
        this.RequestInformationEvents()
        this.RequestInformationVacancies()
        this.RequestInformationEntity()
    }
    RequestInformationEvents() {
        console.log(this.props.entityId)
        fetch(window.location.origin + '/get/RequestInformationEvent/' + this.props.entityId, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Events: x.data
                })
            })
    } 
    RequestInformationEntity() {
        fetch(window.location.origin + '/get/RequestInformationEntity/' + this.props.entityId, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                console.log(x.data,"Entities")
                this.setState({
                    Entities: x.data
                })
            })
    }
    RequestInformationVacancies() {
        fetch(window.location.origin + '/get/RequestInformationVacancy/' + this.props.entityId, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Vacancies: x.data
                })
            })
    }
    render() {

        $("#file-upload").change(function () {
            readURL(this);
        });
        return (
            <section id="UpdateOrganization" >
                <div className="container-fluid ">
                    <div className="row">
                        <div className="col-12 col-md-12  withoutPadding">
                            <div className="card">
                                <div className="card-header text-center">
                                    پروفایل مو تازه کړئ <br />
                                    <img id="blah" src="/images/Home/picture.png" className="img-fluid" /><br />
                                    <form encType="multipart/form-data" id="formImgVolunter" action="api/Upload/" method="post">
                                        <label htmlFor="file-upload" className="custom-file-upload">
                                            <i className="fas fa-image"></i> یو انځور غوره کړئ
                                        </label>
                                        <input type="file" id="file-upload" className="btn btn-primary" name="imgUploader" />
                                    </form>
                                    <a data-toggle="modal" className="blueText labelStyle" data-target="#seeEvents" >درخواست شوی برنامی ({this.state.Events.length})</a> <span className="blueText">|</span> 
                                    <a data-toggle="modal" className="blueText labelStyle" data-target="#seeVacancies" >درخواست شوی بستونه({this.state.Vacancies.length})</a> <span className="blueText">|</span> 
                                    <a data-toggle="modal" className="blueText labelStyle" data-target="#seeEntities" > Followed Organizations({this.state.Entities.length})</a>
                                    
                                </div>
                                <div className="card-body withoutPadding bottomSpace">
                                    <div className="row">
                                        <div className="col-lg-1 col-lg-1  d-none d-lg-block greenW"></div>
                                        <div className="col-12 col-lg-2 greenW text-right">
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">بشپړ نوم</label>
                                                <input type="text" id="name" className="form-control Md-Select text-right whiteText " />
                                            </div>

                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">تذکره</label>
                                                <input type="text" id="LegalIdentification" className="form-control Md-Select text-right whiteText" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">بریښنالیک</label>
                                                <input type="email" id="Email" className="form-control Md-Select text-right whiteText" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">ارتباطی منبع</label>
                                                <input type="text" id="Reference" className="form-control Md-Select text-right whiteText" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">د ارتباطی منبعی د تیلیفون شمیره</label>
                                                <input type="number" id="PhoneReference" className="form-control Md-Select text-right whiteText" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">د زیږیدو نیټه</label>
                                                <input type="date" id="Birth" className="form-control Md-Select text-right whiteText " />
                                            </div>
                                            <div className="form-group topSpace-1x">
                                                <label  >ولایت</label>
                                                <select id="Province" onChange={this.props.changeDistrict.bind(null)} className="form-control Md-Select whiteText withoutPadding text-right">
                                                    <option value="99">یو غوره کړۍ</option>
                                                    {this.props.Province.map(x => {
                                                        return (
                                                            <option key={md5(x.PROVINCE_DESCRIPTION)} name={x.PROVINCE_DESCRIPTION} id={x.PROVINCE_DESCRIPTION} value={x.PROVINCE_CODE}>{x.PROVINCE_DESCRIPTION}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>
                                            <div className="form-group topSpace-1x">
                                                <label >ولسوالي/ ناحیه</label>
                                                <select id="District" className="form-control Md-Select whiteText withoutPadding text-right">
                                                    <option value="99">یو غوره کړۍ</option>
                                                    {this.props.District.map(x => {
                                                        return (
                                                            <option key={md5(x.DISTRICT_DESCRIPTION)} name={x.DISTRICT_DESCRIPTION} id={x.DISTRICT_DESCRIPTION} value={x.DISTRICT_CODE}>{x.DISTRICT_DESCRIPTION}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>


                                            <div className="topSpace-1x">

                                                <label className="withoutMargin" id="lblVillague">کلی</label>
                                                <input type="text" id="Village" className="form-control Md-Select whiteText whiteText text-right" />
                                            </div>
                                           
                                            <div className="form-group topSpace-1x">
                                                <label className="withoutMargin">جنډر</label>
                                                <select id="Gender" className="form-control Md-Select whiteText withoutPadding text-right">
                                                    <option value="99">یو غوره کړۍ</option>
                                                    {this.props.Gender.map(x => {
                                                        return (
                                                            <option key={md5(x.GENDER_DESCRIPTION)} name={x.GENDER_DESCRIPTION} id={x.GENDER_DESCRIPTION} value={x.GENDER_CODE}>{x.GENDER_DESCRIPTION}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>
                                            <div className="form-group topSpace-1x">
                                                <label className="withoutMargin">د ویني ګروپ</label>

                                                <select id="Blood" className="form-control Md-Select whiteText withoutPadding Michelle Martínez">
                                                    <option value="99">یو غوره کړۍ</option>
                                                    {this.props.Blood.map(x => {
                                                        return (
                                                            <option key={md5(x.BLOOD_GROUP_DESCRIPTION)} name={x.BLOOD_GROUP_DESCRIPTION} id={x.BLOOD_GROUP_DESCRIPTION} value={x.BLOOD_GROUP_CODE}>{x.BLOOD_GROUP_DESCRIPTION}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>


                                        </div>
                                        <div className="col-12 col-lg-2 greenW text-right">
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">یوزر</label>
                                                <input type="text" id="User" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">پاسورډ </label>
                                                <input type="password" id="PassWord" className="form-control Md-Select whiteText text-right" />
                                            </div>


                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">تیلیفون</label>
                                                <input type="text" id="Telephone" className="form-control  Md-Select whiteText text-right" />
                                            </div>

                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">فیسبوک</label>
                                                <input type="text" id="Facebook" className="form-control  Md-Select whiteText text-right"/>
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">انستاګرام</label>
                                                <input type="text" id="Instagram" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">ټویټر</label>
                                                <input type="text" id="Twitter" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">+ګوګل</label>
                                                <input type="text" id="Google" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">اختیاری پته</label>
                                                <input type="text" id="Address" className="form-control Md-Select whiteText text-right" />
                                            </div>

                                            <div className="form-group topSpace-1x">
                                                <label className="withoutMargin">زده کړي</label>
                                                <select id="Education" className="form-control Md-Select whiteText withoutPadding Michelle Martínez">
                                                    <option value="99">یو غوره کړۍ</option>
                                                    {this.props.Education.map(x => {
                                                        return (
                                                            <option key={md5(x.EDUCATION_DESCRIPTION)} name={x.EDUCATION_DESCRIPTION} id={x.EDUCATION_DESCRIPTION} value={x.EDUCATION_CODE}>{x.EDUCATION_DESCRIPTION}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>
                                            <div className="form-group topSpace-1x">
                                                <label className="withoutMargin">د رضاکار شتون</label>
                                                <select id="Period" className="form-control Md-Select whiteText withoutPadding Michelle Martínez">
                                                    <option value="99">یو غوره کړۍ</option>
                                                    {this.props.Period.map(x => {
                                                        return (
                                                            <option key={md5(x.PERIOD_DESCRIPTION)} name={x.PERIOD_DESCRIPTION} id={x.PERIOD_DESCRIPTION} value={x.PERIOD_CODE}>{x.PERIOD_DESCRIPTION}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin">زما په اړه</label><br/>
                                                <textarea type="text" id="Commentary" row="5" className="Md-Select-textArea whiteText text-right"/>
                                            </div>

                                        </div>
                                        <div className="col-md-1 greenW  d-none d-lg-block"></div>
                                        <div className="col-12 col-md-12 col-lg-6 orangeW topSpace-2x">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h4 className="text-center" >(SDGs)د علاقي وړ پایداره پرمختیایی موخي</h4> <br />
                                                </div>

                                                {this.props.Sdg.map(x => {
                                                    return (
                                                        <div key={md5(x.SDG_CODE)} className="col-12 col-md-6 col-lg-4">
                                                            <div className="form-check">
                                                                <input
                                                                    className="form-check-input"
                                                                    type="checkbox" id={x.SDG_CODE}
                                                                    name="Sdg"
                                                                    value={`${x.SDG_CODE}#${x.SDG_DESCRIPTION}`} />
                                                                <img src={`/images/SDG/${x.SDG_CODE}.png`} className="smallImg" data-toggle="tooltip" data-placement="top" title={x.SDG_DESCRIPTION} />

                                                            </div>
                                                        </div>
                                                    )
                                                })}





                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid topSpace bottomSpace">

                                        <div className="row text-center">
                                            <div className="col-12 text-center">
                                                <h3 className="blueText">د تخصص برخي</h3>
                                            </div>
                                            {this.props.Abilities.map(x => {
                                                return (

                                                    <div key={uuidv1()}className="col-2">
                                                        <div  className="form-check">
                                                            <input
                                                                className="form-check-input"
                                                                type="checkbox" id={"h" + x.ABILITIES_CODE}
                                                                name="Abilities"
                                                                value={`${x.ABILITIES_CODE}#${x.ABILITIES_DESCRIPTION}`} />
                                                            {x.ABILITIES_DESCRIPTION} <br />
                                                            <img src={`/images/Habilities/${x.ABILITIES_CODE}.png`} className="smallImgx1 img-fluid" data-toggle="tooltip" data-placement="top" title={x.ABILITIES_DESCRIPTION} />
                                                        </div>
                                                    </div>

                                                )

                                            })
                                            }


                                        </div>
                                        <div className="row">
                                            <div className="col-12 topSpace"></div>
                                            <div className="col-3">
                                                <button type="button" className="btn  btn-info  btn-sm" data-toggle="modal" data-target="#modalProfession">مسلکونه</button>
                                                <ul >
                                                    {this.props.Profession.map((x, y) => {
                                                        return (
                                                            <li key={random()}><p>{x.description}</p></li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>

                                            <div className="col-3">
                                                <button type="button" className="btn  btn-info  btn-sm" data-toggle="modal" data-target="#modalOccupation">شغل</button>
                                                <ul >
                                                    {this.props.Occupation.map((x, y) => {
                                                        return (
                                                            <li key={random()}><p>{x.description}</p></li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                            <div className="col-3">
                                                <button type="button" className="btn  btn-info  btn-sm" data-toggle="modal" data-target="#modalInterest">نوري په زړه پوري برخي</button>
                                                <ul >
                                                    {this.props.Interest.map((x, y) => {
                                                        return (
                                                            <li key={random()}><p>{x.description}</p></li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                            <div className="col-3">
                                                <button type="button" className="btn  btn-info  btn-sm" data-toggle="modal" data-target="#modalExpertise">د تخصص نوري برخي</button>
                                                <ul >
                                                    {this.props.Expertise.map((x, y) => {
                                                        return (
                                                            <li key={random()}><p>{x.description}</p></li>
                                                        )
                                                    })}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer text-muted">
                                    <button onClick={this.props.UpdateVolunteer} className="btn btn-info btn-lg btn-block" type="button">بدلونونه ثبت کړئ<i className="fas fa-save ml-2"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/***************************************************************MODAL Profession******************************************************/}
                <div className="modal fade" id="modalProfession" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">مسلکونه</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="listProfession">تاسو له لیست څخه یو مسلک غوره کولای شئ</label>
                                            <select className="form-control" id="listProfession" onChange={this.props.addOptionToList.bind(null, "Profession")}>
                                                <option value="99" >یو غوره کړۍ</option>
                                                {this.props.ProfessionList.map(x => {
                                                    return (
                                                        <option key={random()} value={`${x.PROFESSION_CODE}#${x.PROFESSION_DESCRIPTION}`}>{x.PROFESSION_DESCRIPTION}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <a onClick={this.props.addItem.bind(null, "Profession")} ><i className="fas fa-plus blueText "></i></a>
                                                </div>
                                            </div>
                                            <input type="text" id="txtProfession" className="form-control" placeholder="مسلک اضافه کړئ" />
                                        </div>
                                    </div>
                                </div>

                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.props.Profession.map((x, y) => {
                                                return (
                                                    <li key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Ars" id={"Profession" + y}>{x.description}</span>
                                                        <a onClick={this.props.deleteItem.bind(null, "Profession", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info" data-dismiss="modal">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/***************************************************************MODAL Occupation******************************************************/}
                <div className="modal fade" id="modalOccupation" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">شغل</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="listOccupation">You can also select an Occupation in the list</label>
                                            <select className="form-control" id="listOccupation" onChange={this.props.addOptionToList.bind(null, "Occupation")}>
                                                <option value="99" >یو غوره کړۍ</option>
                                                {this.props.OccupationList.map(x => {
                                                    return (
                                                        <option key={random()} value={`${x.OCCUPATION_CODE}#${x.OCCUPATION_DESCRIPTION}`}>{x.OCCUPATION_DESCRIPTION}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <a onClick={this.props.addItem.bind(null, "Occupation")} ><i className="fas fa-plus blueText "></i></a>
                                                </div>
                                            </div>
                                            <input type="text" id="txtOccupation" className="form-control" placeholder="شغل اضافه کول" />
                                        </div>
                                    </div>
                                </div>




                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.props.Occupation.map((x, y) => {
                                                return (
                                                    <li key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Ars" id={"Occupation" + y}>{x.description}</span>
                                                        <a onClick={this.props.deleteItem.bind(null, "Occupation", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info" data-dismiss="modal">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/***************************************************************MODAL Interest******************************************************/}
                <div className="modal fade" id="modalInterest" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">د علاقی وړ برخه مو اضافه کړئ</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">


                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text">
                                            <a onClick={this.props.addItem.bind(null, "Interest")} ><i className="fas fa-plus blueText "></i></a>
                                        </div>
                                    </div>
                                    <input type="text" id="txtInterest" className="form-control" placeholder="د علاقی وړ برخه مو اضافه کړئ" />
                                </div>
                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.props.Interest.map((x, y) => {
                                                return (
                                                    <li key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Ars" id={"Interest" + y}>{x.description}</span>
                                                        <a onClick={this.props.deleteItem.bind(null, "Interest", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info" data-dismiss="modal">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>


                {/***************************************************************MODAL Expertise******************************************************/}
                <div className="modal fade" id="modalExpertise" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">تخصص اضافه کړئ</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">


                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text">
                                            <a onClick={this.props.addItem.bind(null, "Expertise")} ><i className="fas fa-plus blueText "></i></a>
                                        </div>
                                    </div>
                                    <input type="text" id="txtExpertise" className="form-control" placeholder="تخصص اضافه کړئ" />
                                </div>


                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.props.Expertise.map((x, y) => {
                                                return (
                                                    <li key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Ars" id={"Expertise" + y}>{x.description}</span>
                                                        <a onClick={this.props.deleteItem.bind(null, "Expertise", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info" data-dismiss="modal">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/*****************************************************************SEE EVENT**************************************************************/}
                <div className="modal fade" id="seeEvents" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5>برنامی</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-darks">
                                    <tbody >
                                        {this.state.Events.map(x => {
                                            return (
                                                <tr key={uuidv1()}>
                                                        <th onClick={$('#seeEvents').modal('hide')} scope="row" className="whiteText"><Link to={'/perfilEvent/' + x.EVENTS_ID}>{x.EVENT.TITULE}</Link>  </th>
                                                </tr>
                                            )
                                        })}
                                    </tbody>

                                </table>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>

                       {/*****************************************************************SEE EVENT**************************************************************/}
                       <div className="modal fade" id="seeVacancies" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5>بستونه</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-darks">
                                    <tbody >
                                        {this.state.Vacancies.map(x => {
                                            return (
                                                <tr key={uuidv1()}>
                                                        <th onClick={$('#seeVacancies').modal('hide')} scope="row" className="whiteText"><Link to={'/perfilVacancy/' + x.VACANCY_ID}>{x.VACANCY_ANNOUNCEMENT.TITLE}</Link>  </th>
                                                </tr>
                                            )
                                        })}
                                    </tbody>

                                </table>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>

                       {/*****************************************************************SEE ENTITIES**************************************************************/}
                       <div className="modal fade" id="seeEntities" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5>سازمانونه/ ادارې</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-darks">
                                    <tbody >
                                        {this.state.Entities.map(x => {
                                            return (
                                                <tr key={uuidv1()}>
                                                        <th onClick={$('#seeEntities').modal('hide')} scope="row" className="whiteText"><Link to={'/perfil/' + x.ENTITY_ID}>{x.ENTITY_MASTER.NAME}</Link>  </th>
                                                </tr>
                                            )
                                        })}
                                    </tbody>

                                </table>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">بندول</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </section>
        )
    }
}

export default Volunteer