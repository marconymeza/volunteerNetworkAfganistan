import React from 'react'
import ReactDom from 'react-dom'
import cookie from 'react-cookies'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'



////////////////////MAIN//////////////////
/*ENGLISH*/ import Main from './Main/index'   //
/*DARY*/    import MainDr from './Main/index-dr'   //
/*PASHTO*/  import MainPt from './Main/index-pt'   //
//////////////////////////////////////////

////////////////////MENU//////////////////////////
/*ENGLISH*/ import Menu from './Menu/index'     //
/*DARY*/    import MenuDr from './Menu/index-dr'//
/*PASHTO*/  import MenuPt from './Menu/index-pt'//
//////////////////////////////////////////////////

////////////////////LOGIN//////////////////////////
/*ENGLISH*/ import Login from './Login/index'     //
/*DARY*/    import LoginDr from './Login/index-dr'//
/*PASHTO*/  import LoginPt from './Login/index-pt'//
//////////////////////////////////////////////////

////////////////////LOGIN/////////////////////////////////
/*ENGLISH*/ import LoginWho from './LoginWho/index'     //
/*DARY*/    import LoginWhoDr from './LoginWho/index-dr'//
/*PASHTO*/  import LoginWhoPt from './LoginWho/index-pt'//
//////////////////////////////////////////////////////////

////////////////////REGISTER//////////////////////////////
/*ENGLISH*/ import Register from './Register/index'     //
/*DARY*/    import RegisterDr from './Register/index-dr'//
/*PASHTO*/  import RegisterPt from './Register/index-pt'//
/////////////////////////////////////////////////////////

////////////////////PANEL///////////////////////////
/*ENGLISH*/ import Panel from './Panel/index'     //
/*DARY*/    import PanelDr from './Panel/index-dr'//
/*PASHTO*/  import PanelPt from './Panel/index-pt'//
////////////////////////////////////////////////////

////////////////////ACTIVITY///////////////////////////////
/*ENGLISH*/ import Activity from './Activity/index'     //
/*DARY*/    import ActivityDr from './Activity/index-dr'//
/*PASHTO*/  import ActivityPt from './Activity/index-pt'//
//////////////////////////////////////////////////////////

////////////////////PANEL/////////////////////////////////
/*ENGLISH*/ import Vacancy from './Vacancy/index'       //
/*DARY*/    import VacancyDr from './Vacancy/index-dr'  //
/*PASHTO*/  import VacancyPt from './Vacancy/index-pt'  //
/////////////////////////////////////////////////////////

////////////////////PERFILORG/////////////////////////////////
/*ENGLISH*/ import Perfil from './Perfil/index'       //
/*DARY*/    import PerfilDr from './Perfil/index-dr'  //
/*PASHTO*/  import PerfilPt from './Perfil/index-pt'  //
/////////////////////////////////////////////////////////

////////////////////PANEL/////////////////////////////////////////////////
/*ENGLISH*/ import PefilVolunteer from './Perfil/Volunteer/index'       //
/*DARY*/    import PefilVolunteerDr from './Perfil/Volunteer/index-dr'  //
/*PASHTO*/  import PefilVolunteerPt from './Perfil/Volunteer/index-pt'  //
/////////////////////////////////////////////////////////////////////////

////////////////////PANEL///////////////////////////////////////////////
/*ENGLISH*/ import PerfilVacancy from './Perfil/Vacancy/index'        //
/*DARY*/    import PerfilVacancyDr from './Perfil/Vacancy/index-dr'     //
/*PASHTO*/  import PerfilVacancyPt from './Perfil/Vacancy/index-pt'     //
///////////////////////////////////////////////////////////////////////

////////////////////PANEL//////////////////////////////////////////////
/*ENGLISH*/ import PerfilEvent from './Perfil/Activity/index'        //
/*DARY*/    import PerfilEventDr from './Perfil/Activity/index-dr'   //
/*PASHTO*/  import PerfilEventPt from './Perfil/Activity/index-pt'   //
///////////////////////////////////////////////////////////////////////

////////////////////PANEL//////////////////////////////
/*ENGLISH*/ import Mail from './Mail'                //
/*DARY*/    import MailDr from './Mail/index-dr'     //
/*PASHTO*/  import MailPt from './Mail/index-pt'     //
///////////////////////////////////////////////////////

////////////////////PANEL//////////////////////////////
/*ENGLISH*/ import Home from './Home'                //
/*DARY*/    import HomeDr from './Home/index-dr'     //
/*PASHTO*/  import HomePt from './Home/index-pt'     //
///////////////////////////////////////////////////////

////////////////////CHAT///////////////////////////////
/*ENGLISH*/ import Chat from './Chat'                //
/*DARY*/    import ChatDr from './Chat/index-Dr'       //
/*PASHTO*/  import ChatPt from './Chat/index-Pt'       //
///////////////////////////////////////////////////////




import Confirmation from './Confirmation'
import Dashboard from './Dashboard'



import ForgotPass from './ForgotPass'
import Activities from './Activities'
import AccessDenied from './accessDenied'


class App extends React.Component {
    componentWillMount(){
        if(!cookie.load('language')){
            cookie.save('language', JSON.stringify({ language: "en" }), { path: '/' })
        }
    }
    render() {
        /////////////////////////////////LOGED//////////////
        if (cookie.load('dataUser')) {
            switch (cookie.load('language').language) {
                case "pt":
                    return (
                        ////////////////////////////////PASHTO//////////////////////////////////////////////////////////
                        <Router>
                            <div id="main">
                                <MenuPt />
                                <Route exact path="/" component={() => (<Main />)} />
                                <Route exact path="/Home" component={() => (<HomePt />)} />
                                <Route path="/Activities" component={() => <Activities />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/Vacancy" component={() => <VacancyPt />} />:<Route path="/Vacancy" component={() => <Home />} />}
                                <Route path="/register" component={() => <HomePt />} />
                                <Route path="/Confirmation" component={() => <Confirmation />} />
                                <Route path="/ForgotPassWord" component={() => <ForgotPass />} />
                                <Route path="/loginWho" component={() => <LoginWhoPt />} />   
                                <Route path="/login" component={() => <HomePt />} />
                                <Route path="/panel" component={() => <PanelPt />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/Activity" component={() => <ActivityPt />} />:<Route path="/Activity" component={() => <Home />} />}
                                <Route path="/chat" component={() => <ChatPt />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/mail" component={() => <MailDr />} />:<Route path="/mail" component={() => <Home />} />}                                
                                <Route path="/api/Upload/" component={() => <PanelPt />} />
                                <Route path="/perfilVacancy/:id" component={({ match }) => (<PerfilVacancyPt id={match.params.id} />)} />{/*TODO*/}
                                <Route path="/perfil/:id" component={({ match }) => (<PerfilPt id={match.params.id} />)} />{/*TODO*/}
                                <Route path="/perfilEvent/:id" component={({ match }) => (<PerfilEventPt id={match.params.id} />)} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/perfilVolunteer/:id" component={({ match }) => (<PefilVolunteerPt id={match.params.id} />)} />:<Route path="/perfilVolunteer/:id" component={({ match }) => (<AccessDenied/>)} />}                                
                                <Route path="/dashboard" component={() => <Dashboard />} />
                            </div>
                        </Router>
                    )
                    break;
                    case "dr":
                    return (
                        ////////////////////////////////DARY//////////////////////////////////////////////////////////
                        <Router>
                            <div id="main">
                                <MenuDr />
                                <Route exact path="/" component={() => (<Main />)} />
                                <Route exact path="/Home" component={() => (<HomeDr />)} />
                                <Route path="/Activities" component={() => <Activities />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/Vacancy" component={() => <VacancyDr />} />:<Route path="/Vacancy" component={() => <Home />} />}
                                <Route path="/register" component={() => <HomeDr />} />
                                <Route path="/Confirmation" component={() => <Confirmation />} />
                                <Route path="/ForgotPassWord" component={() => <ForgotPass />} />
                                <Route path="/loginWho" component={() => <LoginWhoDr />} />   
                                <Route path="/login" component={() => <HomeDr />} />
                                <Route path="/panel" component={() => <PanelDr />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/Activity" component={() => <ActivityDr />} />:<Route path="/Activity" component={() => <Home />} />}
                                <Route path="/chat" component={() => <ChatDr />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/mail" component={() => <MailPt />} />:<Route path="/mail" component={() => <Home />} />}                                
                                <Route path="/api/Upload/" component={() => <PanelDr />} />
                                <Route path="/perfilVacancy/:id" component={({ match }) => (<PerfilVacancyDr id={match.params.id} />)} />{/*TODO*/}
                                <Route path="/perfil/:id" component={({ match }) => (<PerfilDr id={match.params.id} />)} />{/*TODO*/}
                                <Route path="/perfilEvent/:id" component={({ match }) => (<PerfilEventDr id={match.params.id} />)} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/perfilVolunteer/:id" component={({ match }) => (<PefilVolunteerDr id={match.params.id} />)} />:<Route path="/perfilVolunteer/:id" component={({ match }) => (<AccessDenied/>)} />}                                
                                <Route path="/dashboard" component={() => <Dashboard />} />

                            </div>
                        </Router>
                    )
                    break;
                default:
                    return (
                        ////////////////////////////////ENGLISH//////////////////////////////////////////////////////////
                        <Router>
                            <div id="main">
                                <Menu />
                                <Route exact path="/" component={() => (<Main />)} />
                                <Route exact path="/Home" component={() => (<Home />)} />
                                <Route path="/Activities" component={() => <Activities />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/Vacancy" component={() => <Vacancy />} />:<Route path="/Vacancy" component={() => <Home />} />}
                                <Route path="/register" component={() => <Home />} />
                                <Route path="/Confirmation" component={() => <Confirmation />} />
                                <Route path="/ForgotPassWord" component={() => <ForgotPass />} />
                                <Route path="/loginWho" component={() => <LoginWho />} />   
                                <Route path="/login" component={() => <Home />} />
                                <Route path="/panel" component={() => <Panel />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/Activity" component={() => <Activity />} />:<Route path="/Activity" component={() => <Home />} />}
                                <Route path="/chat" component={() => <Chat />} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/mail" component={() => <Mail />} />:<Route path="/mail" component={() => <Home />} />}                                
                                <Route path="/api/Upload/" component={() => <Panel />} />
                                <Route path="/perfilVacancy/:id" component={({ match }) => (<PerfilVacancy id={match.params.id} />)} />{/*TODO*/}
                                <Route path="/perfil/:id" component={({ match }) => (<Perfil id={match.params.id} />)} />{/*TODO*/}
                                <Route path="/perfilEvent/:id" component={({ match }) => (<PerfilEvent id={match.params.id} />)} />
                                {cookie.load('dataUser').ENTITY_MASTER.TYPE_ENTITY == 1?<Route path="/perfilVolunteer/:id" component={({ match }) => (<PefilVolunteer id={match.params.id} />)} />:<Route path="/perfilVolunteer/:id" component={({ match }) => (<AccessDenied/>)} />}                                
                                <Route path="/dashboard" component={() => <Dashboard />} />
                            </div>
                        </Router>
                )
                    break;
            }
        } 
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        else {
            /////////////////////////////////UNLOGED//////////////
            switch (cookie.load('language').language) {
                case "pt":
                return (
                    ////////////////////////////////PASHTO//////////////////////////////////////////////////////////
                    <Router>
                <div id="main">
                    <MenuPt />
                    <Route exact path="/" component={() => (<Main />)} />
                    <Route exact path="/Home" component={() => (<HomePt />)} />
                    <Route path="/Activities" component={() => (<LoginPt />)} />
                    <Route path="/Vacancy" component={() => <LoginPt />} />                                        
                    <Route path="/register" component={() => (<RegisterPt />)} />
                    <Route path="/Confirmation" component={() => (<Confirmation />)} />
                    <Route path="/ForgotPassWord" component={() => <ForgotPass />} /> 
                    <Route path="/loginWho" component={() => <LoginWhoPt />} />   
                    <Route path="/login" component={() => (<LoginPt />)} />                                     
                    <Route path="/panel" component={() => (<LoginPt />)} />
                    <Route path="/Activity" component={() => <LoginPt />} />
                    <Route path="/chat" component={() => <LoginPt />} />
                    <Route path="/mail" component={() => <LoginPt />} />
                    <Route path="/api/Upload/" component={() => <PanelPt />} />                    
                    <Route path="/perfil/:id" component={({ match }) => <LoginPt />} />{/*TODO*/}
                    <Route path="/perfilVacancy/:id" component={({ match }) => <LoginPt />} />{/*TODO*/}
                    <Route path="/perfilEvent/:id" component={({ match }) => <LoginPt />} />
                    <Route path="/perfilVolunteer/:id" component={({ match }) => <LoginPt />} />
                    <Route path="/dashboard" component={() => <Dashboard />} />
                </div>
            </Router>
                )
                    break;
                case "dr":
                return (
                        ////////////////////////////////DARY//////////////////////////////////////////////////////////                        
                        <Router>
                        <div id="main">
                            <MenuDr />
                            <Route exact path="/" component={() => (<Main />)} />
                            <Route exact path="/Home" component={() => (<HomeDr />)} />
                            <Route path="/Activities" component={() => (<LoginDr />)} />
                            <Route path="/Vacancy" component={() => <LoginDr />} />                                        
                            <Route path="/register" component={() => (<RegisterDr />)} />
                            <Route path="/Confirmation" component={() => (<Confirmation />)} />
                            <Route path="/ForgotPassWord" component={() => <ForgotPass />} />   
                            <Route path="/loginWho" component={() => <LoginWhoDr />} />   
                            <Route path="/login" component={() => (<LoginDr />)} />                                     
                            <Route path="/panel" component={() => (<LoginDr />)} />
                            <Route path="/Activity" component={() => <LoginDr />} />
                            <Route path="/chat" component={() => <LoginDr />} />
                            <Route path="/mail" component={() => <LoginDr />} />
                            <Route path="/api/Upload/" component={() => <PanelDr />} />                    
                            <Route path="/perfil/:id" component={({ match }) => <LoginDr />} />{/*TODO*/}
                            <Route path="/perfilVacancy/:id" component={({ match }) => <LoginDr />} />{/*TODO*/}
                            <Route path="/perfilEvent/:id" component={({ match }) => <LoginDr />} />
                            <Route path="/perfilVolunteer/:id" component={({ match }) => <LoginDr />} />
                            <Route path="/dashboard" component={() => <Dashboard />} />

                        </div>
                    </Router>
                )
                    break;
                default:
                return(
                    ////////////////////////////////ENGLISH//////////////////////////////////////////////////////////
                    <Router>
                <div id="main">
                    <Menu />
                    <Route exact path="/" component={() => (<Main />)} />
                    <Route exact path="/Home" component={() => (<Home />)} />
                    <Route path="/Activities" component={() => (<Login />)} />
                    <Route path="/Vacancy" component={() => <Login />} />                                        
                    <Route path="/register" component={() => (<Register />)} />
                    <Route path="/Confirmation" component={() => (<Confirmation />)} />
                    <Route path="/ForgotPassWord" component={() => <ForgotPass />} />   
                    <Route path="/loginWho" component={() => <LoginWho />} />   
                    <Route path="/login" component={() => (<Login />)} />                                     
                    <Route path="/panel" component={() => (<Login />)} />
                    <Route path="/Activity" component={() => <Login />} />
                    <Route path="/chat" component={() => <Login />} />
                    <Route path="/mail" component={() => <Login />} />
                    <Route path="/api/Upload/" component={() => <Panel />} />                    
                    <Route path="/perfil/:id" component={({ match }) => <Login />} />{/*TODO*/}
                    <Route path="/perfilVacancy/:id" component={({ match }) => <Login />} />{/*TODO*/}
                    <Route path="/perfilEvent/:id" component={({ match }) => <Login />} />
                    <Route path="/perfilVolunteer/:id" component={({ match }) => <Login />} />
                    <Route path="/dashboard" component={() => <Dashboard />} />
                </div>
            </Router>
                )
                    break;
            }
        }
    }
}

ReactDom.render(<App />, document.getElementById('root'))