const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const WORK_EXPERIENCE_DESCRIPTION = sequelize.define('WORK_EXPERIENCE_DESCRIPTION', 
    {
      // definicion del los campos
      WORK_EXPERIENCE_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      WORK_EXPERIENCE_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'WORK_EXPERIENCE_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.WorkDescription =  WORK_EXPERIENCE_DESCRIPTION


