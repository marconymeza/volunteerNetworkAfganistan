const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ABILITIES_DESCRIPTION = sequelize.define('ABILITIES_DESCRIPTION', 
    {
      // definicion del los campos
      ABILITIES_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      ABILITIES_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'ABILITIES_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.AbilitiesDescription = ABILITIES_DESCRIPTION


