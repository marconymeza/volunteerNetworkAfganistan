const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;

    const PROVINCE_DESCRIPTION = sequelize.define('PROVINCE_DESCRIPTION', 
    {
      PROVINCE_CODE: { type: Sequelize.STRING, primaryKey: true, allowNull: false },
      PROVINCE_DESCRIPTION: { type: Sequelize.STRING },
      LANGUAGE: { type: Sequelize.INTEGER } 

    },
      {
        tableName: 'PROVINCE_DESCRIPTION', timestamps: false, freezeTableName: true,
    });
    
    const EVENTS = sequelize.define('EVENTS', 
    {
      ENTITY_ID: { type: Sequelize.STRING, primaryKey: true, allowNull: false },
      EVENTS_ID: { type: Sequelize.STRING, primaryKey: true, allowNull: false },
      START_DATE: { type: Sequelize.DATE },
      END_DATE: { type: Sequelize.DATE },
      STATUS: { type: Sequelize.NUMERIC },
      NOTIFICATION_STATUS: { type: Sequelize.BOOLEAN },
      LATITUD: { type: Sequelize.STRING },
      LONGITUDE: { type: Sequelize.STRING },
      TITULE: { type: Sequelize.STRING },
      SUB_TITULE: { type: Sequelize.STRING },
      DESCRIPTION: { type: Sequelize.STRING },
      IMAGE: { type: Sequelize.STRING },
      RESPONSIBLE_PERSON_NAME: { type: Sequelize.STRING },
      RESPONSIBLE_TELEPHONE: { type: Sequelize.STRING },
      RESPONSIBLE_EMAIL: { type: Sequelize.STRING },
      DETAILS: { type: Sequelize.STRING },
      OBJECTIVES: { type: Sequelize.STRING }
    },
      {
        tableName: 'EVENTS', timestamps: false, freezeTableName: true,
    });

const VACANCY_ANNOUNCEMENT = sequelize.define('VACANCY_ANNOUNCEMENT',
  {
    ENTITY_ID: { type: Sequelize.STRING },
    VACANCY_ID: { type: Sequelize.STRING, primaryKey: true, allowNull: false },
    VACANCY_TYPE: { type: Sequelize.NUMERIC },
    TITLE: { type: Sequelize.STRING },
    DESCRIPTION: { type: Sequelize.STRING, allowNull: false },
    IMAGE: { type: Sequelize.STRING },
    START_DATE: { type: Sequelize.DATE },
    END_DATE: { type: Sequelize.DATE },
    STATUS: { type: Sequelize.NUMERIC },
    DURATION: { type: Sequelize.STRING },
    EDUCATION_REQUIRED: { type: Sequelize.STRING },
    NO_OF_JOBS: { type: Sequelize.NUMERIC },
    INCENTIVES: { type: Sequelize.STRING },
    WORK_EXPERIENCE_REQUIRED: { type: Sequelize.STRING }
  },
  {
    tableName: 'VACANCY_ANNOUNCEMENT', timestamps: false, freezeTableName: true,
  });

const VACANCY_EXPERTISE = sequelize.define('VACANCY_EXPERTISE',
  {
    VACANCY_ID: { type: Sequelize.STRING, primaryKey: true, allowNull: false },
    AREA_ID: { type: Sequelize.STRING, primaryKey: true, allowNull: false },
    AREA_DESCRIPTION: { type: Sequelize.STRING }
  },
  {
    tableName: 'VACANCY_EXPERTISE', timestamps: false, freezeTableName: true,
  });

module.exports.models = {
    provinceDescription:PROVINCE_DESCRIPTION,
    events: EVENTS,
    vacancy: VACANCY_ANNOUNCEMENT,
    vacancyExpertice: VACANCY_EXPERTISE
}
