import React from 'react'
import ReactTooltip from 'react-tooltip'
import uuidv1 from 'uuid/v1'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import md5 from 'md5'
class CardsOrg extends React.Component{
    constructor(props){
        super(props)
    }

    selectOrg(x, y) {
        if (y)
          document.getElementById(x).className = "card cardRed"
        else
          document.getElementById(x).className = "card"
      }
    render(){
        return(
            <div className="col-12">

              <div className="row text-center">
                <h3 id="noResult" className="redText text-center"></h3>
                {this.props.organizations.map(x => {
                  return (
                    <div className="col-lg-3 bottomSpace" key={uuidv1()}>
                      <ReactTooltip />
                      <div className="card cardRed" id={`pointer${x.ENTITY_ID}`}>
                            {x.IMAGE==null?
                        <div className="col-12 text-center imagePerfil"><img className="img-fluid" src={`/images/orgImages/defaultOrg.jpg`} /></div>
                            :
                            <div className="col-12 text-center imagePerfil"><img className="img-fluid" src={`/images/orgImages/${x.IMAGE}`} onError={(e)=>{e.target.src="/images/orgImages/defaultOrg.jpg"}} /></div>

                          }
                            
                        <div className="card-body withoutPadding-bottom">
                          <h5 className="card-title"><strong>{x.NAME}</strong></h5>
                          {x.COMMENTARY == null || x.COMMENTARY == "" ? <p className="card-text tt colorGray">{x.COMMENTARY}</p> : <p className="card-text tt">{`${x.COMMENTARY.substring(0, 90)}...`}</p>}
                          {/* <p className="card-text"><i className="fas fa-envelope fa-1x inLine redText"></i>{` ${x.EMAIL}`}</p> */}
                          {x.FACEBOOK == null || x.FACEBOOK == "" ? <p className="fab redText">شبکه اجتماعی را دارا نیست</p> :
                            <a href={`${x.FACEBOOK}`} target="_blank"><i className="fab fa-facebook-square fa-2x inLine paddingRigth-1x redText" data-tip={x.FACEBOOK}></i></a>
                          }
                          {x.TWITTER == null || x.TWITTER == "" ? null :
                            <a href={`${x.FACEBOOK}`} target="_blank"><i className="fab fa-twitter-square fa-2x inLine  paddingRigth-1x redText" data-tip={x.TWITTER}></i></a>

                          }
                          {x.INSTAGRAM == null || x.INSTAGRAM == "" ? null :
                            <a href={`${x.FACEBOOK}`} target="_blank"><i className="fab fa-instagram fa-2x paddingRigth-1x inLine redText" data-tip={x.INSTAGRAM}></i></a>
                          }
                          {x.GOOGLE == null || x.GOOGLE == "" ? null :
                            <a href={`${x.GOOGLE}`} target="_blank"><i className="fab fa-google-plus-square fa-2x inLine redText" data-tip={x.GOOGLE}></i></a>
                          }
                        </div><br/>
                        <hr className="gradient"/>
                        <div className="viewProfile">
                            <Link to={'/perfil/'+x.ENTITY_ID}>مشاهده پروفایل</Link>                            
                        </div>

                        {/* <hr className="redText"/>
                        
                      <p className="text-center redText">More</p> */}
                      </div>
                    </div>
                  )

                })}
              </div>
              <div className="row bottomSpace">
                <div className="col">
                  <hr />
                </div>
                <div className="col-1 text-center">
                <ReactTooltip />
                {this.props.clearSelection == 0?
                  <a className="button" onClick={this.props.getOrg.bind(null,'/Get/EntityHome/1/','organizations')}  ><i className="fas fa-plus fa-2x redText"></i></a>
                :
                <a className="button" onClick={this.props.clearSelectionFunc.bind(null)}  data-tip="Clean filter"><i className="fas fa-eraser fa-2x redText"></i></a>                

              }
                </div>
                <div className="col">
                  <hr />
                </div>
              </div>
            </div>
        )
    }
}

export default CardsOrg