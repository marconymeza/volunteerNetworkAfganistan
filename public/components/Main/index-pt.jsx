import React from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class Home extends React.Component {

    constructor() {
        super()
        this.state = {
            Events: [],
            CountExpertiseVacancy: []
        }
    }
    getDataCard(url, array) {
        fetch(`${window.location.origin}${url}0`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                var data = []
                x.data.map((r, y) => {
                    this.state[array].push(r)
                })
                this.setState({ [array]: this.state[array] })
            })
    }
    goHome() {
        location.href = '/home'
    }
    componentDidMount() {
        fetch(`${window.location.origin}/get/CountExpertiseVacancy/`, { method: 'get' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                x.data.map((r, y) => {
                    if (r.AREA_ID.length < 3)
                        this.state.CountExpertiseVacancy.push(r)
                })
                this.setState({ CountExpertiseVacancy: this.state.CountExpertiseVacancy })
            })




        this.getDataCard('/Get/EventHome/', 'Events')
    }
    render() {
        return (
            <section  >
                <section id="particles-js" className="cover">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                            </div>
                        </div>
                    </div>
                </section>
                <div className="row">
                    <div className="col-12 text-center">
                        <p><i className="fas fa-angle-down blueText fa-5x"></i></p>
                    </div>
                </div>
                <div className="container topSpace bottomSpace">
                    <div className="row">
                        <div className="col-12 text-center bottomSpace">
                            <h4 className="blueText">د مهارتونو په کار وړنه</h4>
                            <h1 className="colorGray" >د رضاکارۍ بیلابیلي لاري</h1>
                        </div>
                        <div className="col-8 offset-2 text-center">
                            <div className="row">
                                {this.state.CountExpertiseVacancy.map(x => {
                                    return (
                                        <div className="col-3 cardRed text-center">
                                            <img src={`/images/Habilities/${x.AREA_ID}.png`} className="img-fluid mediumImage" />
                                            <p className="font-weight-light">{x.AREA_DESCRIPTION} <br /><strong>({x.Conteo})</strong></p>
                                        </div>
                                    )
                                })}
                            </div>
                            <button type="button" onClick={this.goHome.bind(this)} className="btn  btn-info  btn-sm">دندي وګورۍ</button>
                        </div>
                    </div>
                </div>
                <section className="blueBack">
                    <div className="container topSpace bottomSpace">
                        <div className="row">
                            <div className="col-12 text-center bottomSpace">
                                <h4 className="">پیل یی کړئ</h4>
                                <h1 className="" >د بدلون لپاره پیښه / پروګرام ومومئ</h1>
                            </div>
                            {this.state.Events.map(x => {
                                return (
                                    <div className="col-3 text-center">
                                        <div className="card border-info mb-3 cardRed" >
                                            <div className="card-header blueText">{x.TITULE.substring(0, 23)}</div>
                                            <div className="card-body text-info">
                                                <Link to={'/perfilEvent/' + x.EVENTS_ID}>پیښه/ پروګرام وګورئ</Link>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <div className="row grayColor bottomSpace topSpace">
                        <div className="col-12 text-center">
                            <p>ټول حقونه د اطلاعاتو او فرهنګ وزارت سره خوندي دي</p>
                            <p>د ډیټابس جوړښت د ملګرو ملتونو پرمختیایی پروګرام لخوا شوي</p>
                        </div>
                    </div>
                </section>
            </section>
        )
    }
}

export default Home