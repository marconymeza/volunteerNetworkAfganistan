import React from 'react'
import md5 from 'md5'
import random from 'uuid/v1'

class Register extends React.Component {

    constructor() {
        super()
        this.state = {
            Gender: [],
            Country: [],
            Type: []
        }
    }
    render() {

        return (
            <section className="Register blueText">
                <div className="container text-center">
                    <div className="row  flotante">
                        <div className="col-12 topSpace  bottomSpace">
                            <h1>Who are you?</h1>
                        </div>
                        <div className="col-12 col-md-4 offset-md-2 ">
                            <a href="/login">
                                <i className="fas  fa-building fa-10x blueText"></i>
                                <h3 className="blueText">Organization</h3>
                            </a>
                        </div>
                        <div className="col-12 topSpace d-md-none d-lg-none d-xl-none"></div>
                        <div className="col-12 col-md-4">
                            <a href="/login">
                                <i className="fas fa-user fa-10x blueText"></i>
                                <h3 className="blueText">Volunteer</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Register