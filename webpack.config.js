// const cssModules = 'modules&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:5]'
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin')
const extractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin')


module.exports = {
    resolve:{
        extensions:['.js','.jsx']
    },

    entry:['./public/components/index.jsx'],

    output:{
        path:__dirname + '/public/js',
        filename: 'index.js',
        publicPath: '/'
    },
    devServer:{
        host: '0.0.0.0',
        port: 3001,
        inline: true
    },
    module:{
        loaders:[
            {test:/(\.js|jsx)$/, exclude: /node_modules/, loader:'babel-loader'},
        ]
    }
}