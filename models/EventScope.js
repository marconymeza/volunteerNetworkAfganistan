const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const EVENTS_SCOPE = sequelize.define('EVENTS_SCOPE', 
    {
      // definicion del los campos
      EVENTS_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      SCOPE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'EVENTS_SCOPE',
      timestamps: false,
      freezeTableName: true,
    });
    

module.exports.EventScope = EVENTS_SCOPE


