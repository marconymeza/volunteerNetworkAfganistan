import React from 'react'
import { Input, Option, TextArea } from '../Controls'
import { UV_UDP_REUSEADDR } from 'constants';
import uuidv1 from 'uuid/v1'
import cookie from 'react-cookies'


class Mail extends React.Component{

    constructor(){
        super()
        this.state = {
            list:[],
            sdg:[],
            listSelected:[]
        }
        this.userInfo = cookie.load('dataUser')
    }
    fillCombobox(url, array) {
        fetch(window.location.origin + '/Get/api/obtener/' + url, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    [array]: x.data
                })
            })
    }
    componentWillMount(){
        this.fillCombobox('dp/ModSdgDescription/SDG_CODE/1', 'sdg')
    }
    search(){
        if(document.getElementById('to').value != '')
        fetch(window.location.origin + '/Get/GetEmail/' + document.getElementById('to').value, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    list: x.data
                })
            })
            else this.setState({
                list: []
            })
    }
    select(name,email){
        this.state.listSelected.push({name:name,email:email})
        this.setState({listSelected:this.state.listSelected})
        this.setState({list: []})
        document.getElementById('to').value = ""
    }
    clean(){
        this.setState({list: []})
    }
    deleteItem(y){
        this.state.listSelected.splice(y, 1)
        this.setState({
            listSelected: this.state.listSelected
        })
    }
    sendEmail(){
        console.log(this.userInfo)
        if(document.getElementById("Title").value == "")
         alertify.error('Subject is empty');
        else if(document.getElementById("mailMessage").value == "")
         alertify.error('Message is empty');
        else if(this.state.listSelected.length <1)
         alertify.error('Select destinatary');
         else{
             
            var data = {
                Subjetc: document.getElementById("Title").value,
                Message: document.getElementById("mailMessage").value,
                To:this.state.listSelected,
                Contact:this.userInfo.EMAIL,
                USER_NAME:this.userInfo.USER_NAME
            }
    
    
            fetch(window.location.origin + '/Insert/SendEmail/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status == 200 || response.status == 202) {
                                alertify.success('Message sent successfully')
                                document.getElementById("mailMessage").value = ""
                                setTimeout(
                                    function(){
                                        location.reload();
                                    }, 2000);
                            }
                            else {
                                alertify.error('Failed, try again')
                            }
                        })
                })
         }
        

        
        
    }
    render(){
        $(document).ready(function() {$('#mailMessage').summernote({height: 300,});});                
        return (
            <section id="mailSection">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-8 offset-2">
                            <div className="card text-white bg-darks mb-3" >
                                <div className="card-header">Send email</div>
                                <div className="card-body bg-Gray">
                                    <div className="col-8 offset-2">
                                        <input type="text" className="form-control" onChange={this.search.bind(this)} id="to" placeholder="To" />
                                        <div className="list-group ss">
                                            {this.state.list.map(x => {
                                                return (
                                                    <a key={uuidv1()} onDoubleClick={this.select.bind(this, x.NAME, x.EMAIL)} className="list-group-item list-group-item-action">{x.NAME} ({x.EMAIL})</a>
                                                )
                                            })}
                                        </div>
                                        {this.state.listSelected.map((x, y) => {
                                            return (
                                                <span key={uuidv1()} className="badge badge-info marginRigth">{x.name} <a onClick={this.deleteItem.bind(this, y)}>(x)</a></span>
                                            )
                                        })}
                                        <br />
                                        <Input className="col" TextLabel="Subject" type="text" id="Title" inputClass="form-control " />
                                        <TextArea className="col" id="mailMessage" TextLabel="Message" textareaClass="form-control  r" />
                                        <button className="btn btn-info" onClick={this.sendEmail.bind(this)} type="button">Send <i className="fas fa-paper-plane"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Mail