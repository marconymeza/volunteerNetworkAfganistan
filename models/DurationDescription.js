const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const DURATION_DESCRIPTION = sequelize.define('DURATION_DESCRIPTION', 
    {
      // definicion del los campos
      DURATION_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      DURATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'DURATION_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.DurationDescription =  DURATION_DESCRIPTION


