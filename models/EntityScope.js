const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ENTITY_SCOPE = sequelize.define('ENTITY_SCOPE', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      SCOPE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'ENTITY_SCOPE',
      timestamps: false,
      freezeTableName: true,
    });
    

module.exports.EntityScope = ENTITY_SCOPE


