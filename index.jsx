import React from 'react'
import FiltroPSP from '../FiltrosPSP'
import Buscador from '../Buscador'
import uuidv1 from 'uuid/v1'
import PSP from '../PSP'
import fecth from "isomorphic-fetch"
class Directorio extends React.Component {

    constructor() {
        super()
        this.state = {
            organizaciones: [],
            filtros: [{
                key: 321312432432,
                nombre: 'Organizaciones',
                opciones: [
                    { key: 23223, value: 'NINGUNA' },
                    { key: 2342343, value: 'PRIVADA' }
                ]
            }],
            FullOng: {},
            tipoOrg: [],
            ftroBeneficiario: []
        }


        fetch(window.location.origin+'/Organizacion10reg/' + 0, { method: 'POST' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({ organizaciones: x.data })
                this.arryInicial = x.data
                this.tt = ""
          
            })
    }

    componentDidMount() {






        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                var miInit = { method: 'POST' };
                var n = 10
                fetch(window.location.origin+'/Organizacion10reg/' + n, miInit)
                    .then((response) => {
                        return response.json()
                    })
                    .then((x) => {
                        x.data.map(y => {
                            this.state.organizaciones.push(y)
                        })
                        this.setState({ organizaciones: this.state.organizaciones })
                        this.arryInicial = x.data
                        this.tt = ""
                      
                    })
                n = n + 10
            }
        }.bind(this));
    }



    Buscador() {
        var drillDownOrganizaciones = document.getElementById('btnBeneficiario').value
        if ('NINGUNA' == drillDownOrganizaciones) {
            var arrOrg = this.buscarPorTexto(this.arryInicial)
            this.setState({ organizaciones: arrOrg })
        }
        else {
            var arrOrg = this.buscarPorDrillDown(drillDownOrganizaciones, this.arryInicial)
            arrOrg = this.buscarPorTexto(arrOrg)
            this.setState({ organizaciones: arrOrg })
        }
    }
    buscarPorTexto(arr) {
        var PSP = document.getElementById('txtBuscador').value
        var arrayProv = []
        arr.find(x => {
            if (x.NOMBRE.toUpperCase().includes(PSP.toUpperCase()))
                arrayProv.push(x)
        })
        return arrayProv
    }

    buscarPorDrillDown(drillDown, arr) {
        var arrayProv = []
        arr.find(x => {
            if (x.DEF_TIPO_ORGANIZACION.DESCRIPCION_TIPO_ORGANIZACION.toUpperCase().includes(drillDown))
                arrayProv.push(x)
        })
        return arrayProv
    }

    BeneficiaioOPT(r) {

        fetch(window.location.origin+'/' + r, { method: 'get' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                if(r == "GetFormularioCatalogoDimension"){
                    delete x.data[0]
                }
                x.data.map(y => {
                    if (r == "GetFormularioIntervencionBeneficiario") {
                        y.id = y['CODIGO_BENEFICIARIO'];
                        y.nom = y['NOMBRE_BENEFICIARIO'];
                        y.met = 'PostFiltroBeneficiario/'
                    } else if (r == "GetFormularioIntervencionUbicacion") {
                        y.id = y['CODIGO_UBICACION'];
                        y.nom = y['NOMBRE_UBICACION'];
                        y.met = 'postFiltroUbicacion/'
                    }else if (r == "GetFormularioCatalogoDimension") {
                        y.id = y['CODIGO_DIMENSION'];
                        y.nom = y['NOMBRE_DIMENSION'];
                        y.met = 'GetFormularioCatalogoDimension/'
                    }
                })
            
                this.setState({ ftroBeneficiario: x.data })
                
            })
    }

    nuevaData(s,m) {


        fetch(window.location.origin+'/'+m + s, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                x.data.map(y => {
                    y.NOMBRE = y['MAESTRA_ORGANIZACION.NOMBRE'];
                    y.DIRECCION = y['MAESTRA_ORGANIZACION.DIRECCION'];
                    y.TELEFONO = y['MAESTRA_ORGANIZACION.TELEFONO'];
                    y.CORREO_ELECTRONICO = y['MAESTRA_ORGANIZACION.CORREO_ELECTRONICO'];


                })

                this.setState({ organizaciones: x.data })

            })
    }

    render() {
        return (
            <section id="directorio">
                <div className="container-fluid container">
                    <div className="row text-center">
                        <div className="col-12">
                            <br />
                            <h4>Bienvenido al Directorio de organizaciones que trabajan en prevención de violencia</h4>
                            {/* {
                               this.state.tipoOrg.map(valores=>{
                                   return(
                                       <h5>{valores.DESCRIPCION_TIPO_ORGANIZACION}</h5>
                                   )
                               })
                           } */}
                        </div>
                    </div><br />
                    <h6>Puedes buscar una organización en específico</h6>
                    <Buscador buscarPSP={this.Buscador.bind(this)} />
                    {/* <FiltroPSP filtros={this.state.filtros} ops={this.Buscador.bind(this)} /> */} <br />
                    <button type="button" className="btn btn-outline-primary" onClick={this.BeneficiaioOPT.bind(this, 'GetFormularioIntervencionUbicacion')}>Lugar de Intervención</button> <span className="pequenioPading"></span>
                    <button type="button" className="btn btn-outline-primary" onClick={this.BeneficiaioOPT.bind(this, 'GetFormularioIntervencionBeneficiario')}>Beneficiarios</button><span className="pequenioPading"></span>
                    <button type="button" className="btn btn-outline-primary" onClick={this.BeneficiaioOPT.bind(this, 'GetFormularioCatalogoDimension')}>Areas de Intervención</button><span className="pequenioPading"></span><br />
                    <br />
                    {this.state.ftroBeneficiario.map(x => {
                        return (
                            <div key={uuidv1()} className="enLineaa">
                                <button type="button" id="btnBeneficiario" onClick={this.nuevaData.bind(this, x.id,x.met)} className="btn btn-outline-info">{x.nom.charAt(0).toUpperCase() + x.nom.slice(1).toLowerCase()} </button> <span className="pequenioPadings"></span>
                            </div>
                        )
                    })}

                    <PSP ongs={this.state.organizaciones} />
                </div>
            </section>
        )
    }
}

export default Directorio