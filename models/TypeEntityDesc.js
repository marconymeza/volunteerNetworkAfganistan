const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const TYPE_ENTITY_DESCRIPTION = sequelize.define('TYPE_ENTITY_DESCRIPTION', 
    {
      // definicion del los campos
      TYPE_ENTITY_CODE: 
      {
        type: Sequelize.NUMERIC,
        primaryKey: true,
        allowNull: false
      },
      TYPE_ENTITY_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'TYPE_ENTITY_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.TypeDescription = TYPE_ENTITY_DESCRIPTION


