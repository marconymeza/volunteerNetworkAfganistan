import React from 'react'
import { get } from 'https';
import random from 'uuid/v1'
import ReactTooltip from 'react-tooltip'
import cookie from 'react-cookies'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class PerfilEvent extends React.Component {
    constructor() {
        super()
        this.state = {
            Volunteer: [],
            Province: [],
            data:[],
            isFollowing:0
        }
        this.userInfo = cookie.load('dataUser') 
    }
    componentDidMount() {
        this.getEvent()
        this.isFollowing()
    }
    isFollowing(){

        fetch(`${window.location.origin}/get/ValidateRequestEvent/${this.userInfo.ENTITY_ID}/${this.props.id}`, { method: 'post' })
        .then((response) => {
            return response.json()
        })
        .then((x) => {
            this.setState({
                isFollowing:x.data[0].REQUEST_STATUS
            })
        })
    }
    Follow() {

        var data = {Event: this.props.id,Rentity: this.userInfo.ENTITY_ID,ID:random(),ENTITY_ORG:this.state.data.ENTITY_MASTER.ENTITY_ID}
        fetch(window.location.origin + '/Insert/FollowEvent/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202)
                            {
                                alertify.success('Applying');
                            this.setState({
                                isFollowing:1
                            })
                            }
                        else 
                        alertify.success('Failed');

                    })
            })
            fetch(window.location.origin + '/Insert/NotificationEvent/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                    })
            })
            

    }
    UnFollow() {
        var data = {
            Event: this.props.id,
            Rentity: this.userInfo.ENTITY_ID

        }
        fetch(window.location.origin + '/Update/UnFollowEvent/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202)
                            {
                                alertify.success('Not Applying');
                            this.setState({
                                isFollowing:0
                            })
                            }
                        else 
                        alertify.success('Failed');

                    })
            })
    }
    getEvent() {
        fetch(`${window.location.origin}/get/GetEvent/1/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    data:x.data[0]
                })
                if(x.data[0].IMAGE != undefined && x.data[0].IMAGE != null){
                    $("#blah").attr('src', '/images/activityImages/'+ x.data[0].IMAGE);                        
                    document.getElementById("blah").className = "img-fluid imgblah"
                }


                
                document.getElementById('DETAILS').innerHTML = String(x.data[0].DETAILS)               
                document.getElementById('OBJECTIVES').innerHTML = String(x.data[0].OBJECTIVES)

                var t1 = [...new Set(x.data.map(item => {
                    if (item.EVENTS_VOLUNTEER != null)
                        return item.EVENTS_VOLUNTEER.VOLUNTEER_ID
                }))]
                var tt1 = [...new Set(x.data.map(item => {
                    if (item.EVENTS_VOLUNTEER != null)
                        return item.EVENTS_VOLUNTEER.VOLUNTEER_DESCRIPTION
                }))]
                if (t1[0] != undefined)
                    t1.map((y, o) => {
                        if (y != null)

                            this.state.Volunteer.push({ id: y, description: tt1[o] })
                    })
                    console.log(x.data)
                var t2 = [...new Set(x.data.map(item => {
                    if (item.EVENTS_SCOPE != null)
                        return item.EVENTS_SCOPE.SCOPE
                }))]
                console.log("te",t2)
                var tt2 = [...new Set(x.data.map(item => {
                    if (item.EVENTS_SCOPE != null)
                        return item.EVENTS_SCOPE.PROVINCE_DESCRIPTION.PROVINCE_DESCRIPTION
                }))]
                if (t2[0] != undefined)
                    t2.map((y, o) => {
                        if (y != null)

                            this.state.Province.push({ id: y, description: tt2[o] })
                    })
                    console.log("this.state.Province",this.state.Province)
                this.setState({
                    Volunteer: this.state.Volunteer,
                    Province: this.state.Province
                })

            })
    }
    render() {
        return (
            <section id="profileVacancy">
                <div className="container-fluid">
                <ReactTooltip />
                    <div className="row topSpace-1x">
                        <div className="col-12 text-left" data-tip="Go back"><a href="javascript:history.back()"><i className="fas fa-arrow-circle-left fa-2x redText"></i></a></div>
                    </div>
                    <div className="row">
                        <div className="col-12 text-center withoutPadding">
                        <img src="/images/Home/picture.png" onError={(e)=>{e.target.src="/images/activityImages/eventDefault.jpg"}} className="img-fluid topSpace" id="blah" />                         
                        <h3>{this.state.data.TITULE}</h3>

                         {this.state.data.ENTITY_MASTER != null && this.state.data.ENTITY_MASTER != "" ?
                          <p className="card-text withoutPadding withoutMargin"><strong>Organization:</strong> <Link to={'/perfil/'+this.state.data.ENTITY_MASTER.ENTITY_ID}>{this.state.data.ENTITY_MASTER.NAME}</Link></p>:null}

                        {this.state.data.RESPONSIBLE_PERSON_NAME != null && this.state.data.RESPONSIBLE_PERSON_NAME != "" ?
                        <p className="card-text withoutPadding withoutMargin"><strong>Responsable person name:</strong> {this.state.data.RESPONSIBLE_PERSON_NAME}</p>:null}

                        {this.state.data.RESPONSIBLE_TELEPHONE != null && this.state.data.RESPONSIBLE_TELEPHONE != "" ?
                        <p className="card-text withoutPadding withoutMargin"><strong>Phone:</strong> {this.state.data.RESPONSIBLE_TELEPHONE}</p>:null}

                        {this.state.data.RESPONSIBLE_EMAIL != null && this.state.data.RESPONSIBLE_EMAIL != "" ?
                        <p className="card-text withoutPadding withoutMargin"><strong>Email:</strong> {this.state.data.RESPONSIBLE_EMAIL}</p>:null}

                        {this.state.data.START_DATE != null && this.state.data.START_DATE != "" ?
                        <p className="card-text withoutPadding withoutMargin"><strong>Start date publication:</strong> {this.state.data.START_DATE.substring(0,10)}</p>:null}

                        {this.state.data.END_DATE != null && this.state.data.END_DATE != "" ?
                        <p className="card-text withoutPadding withoutMargin"><strong>End date publication:</strong> {this.state.data.END_DATE.substring(0,10)}</p>:null}

                        <hr/>
                        </div>


                        <div className="col-12 text-center">
                        
                            <ul className="nav nav-pills mb-3 center-pills" id="pills-tab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="pills-Objectives-tab" data-toggle="pill" href="#pills-Objectives" role="tab" aria-controls="pills-Objectives" aria-selected="true">Objectives of event</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-Details-tab" data-toggle="pill" href="#pills-Details" role="tab" aria-controls="pills-Details" aria-selected="false">Details of event</a>
                                </li>

                            </ul>
                            <div className="tab-content" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-Objectives" role="tabpanel" aria-labelledby="pills-Objectives-tab">
                                {this.state.data.OBJECTIVES != null && this.state.data.OBJECTIVES != "" ?
                        <div className="col-10 offset-1 text-center topSpace-2x">
                            <div className="card-text withoutPadding withoutMargin" id="OBJECTIVES"></div>
                        </div>:null}
                                </div>
                                <div className="tab-pane fade" id="pills-Details" role="tabpanel" aria-labelledby="pills-Details-tab">
                                {this.state.data.DETAILS != null && this.state.data.DETAILS != "" ?
                        <div className="col-10 offset-1 text-center topSpace-2x">
                            <div className="card-text withoutPadding withoutMargin" id="DETAILS"></div>
                        </div>:null}
                                </div>
                            </div>

                        </div>

                        <div className="col-12 text-center ">
                        
                        {this.state.isFollowing == 0 ?
                            <a data-tip="Apply now" onClick={this.Follow.bind(this)}  className="marginRigth"><i className="fas fa-check fa-2x redText"></i></a>
                            :
                            <a data-tip="Applied"  onClick={this.UnFollow.bind(this)}  className="marginRigth"><i className="fas fa-check fa-2x blueText"></i></a>
                            
                            }
                            <a href="/chat" data-tip="Message"><i className="fas fa-comments fa-2x redText"></i></a>
                            <hr/>
                        </div> 
                        {this.state.Volunteer.length != 0 ?
                        <div className="col-12 text-center topSpace-2x">
                            <h5>Volunteers required</h5>
                            {this.state.Volunteer.map(x=>{
                                return(
                                    <span key={random()} className="badge badge-info marginRigth">{x.description}</span>
                                )
                            })}
                        </div>:null}

                        {this.state.Province.length != 0 ?
                        <div className="col-12 text-center topSpace-2x bottomSpace">
                            <h5>Location(s)</h5>
                            {this.state.Province.map(x=>{
                                return(
                                    <span key={random()} className="badge badge-info marginRigth">{x.description}</span>
                                )
                            })}
                        </div>:null}
                    </div>
                </div>

            </section>
        )
    }
}

export default PerfilEvent