const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const GENDER_DESCRIPTION = sequelize.define('GENDER_DESCRIPTION', 
    {
      // definicion del los campos
      GENDER_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      GENDER_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'GENDER_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.GenderDescription = GENDER_DESCRIPTION


