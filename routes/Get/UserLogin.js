var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var ModLogin = require('../../models/Login').Login;
var ModEntityMaster = require('../../models/EntityMaster').EntityMaster;
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'volunteernetworkafghanistan@gmail.com',
        pass: '@fghAnistan'
    }
});
 
const Op = Sequelize.Op;
var app = express();

router.post('/ValidationLogin/', function (req, res) {
  var queue = Promise.resolve();
  ModLogin.findAll({
    where: {
      [Op.or]: [
        { EMAIL: req.body.Email },
        { USER_NAME: req.body.Email }
      ],
      PASSWORD: req.body.Password

    }
  }).then(count => {
    // console.log(count)
    if (count.length != 0) {
      ModLogin.belongsTo(ModEntityMaster, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
      ModLogin.findAll({
        include: [{
          model: ModEntityMaster,
          attributes: ['TYPE_ENTITY','IMAGE']

        }],
        where: {
          [Op.or]: [
            { EMAIL: req.body.Email },
            { USER_NAME: req.body.Email }
          ],
          PASSWORD: req.body.Password,
          STATUS: 1

        },
        attributes: ['EMAIL', 'ENTITY_ID', 'USER_NAME', 'PASSWORD']
      }).then(count2 => {
        if (count2.length != 0) {

          queue = queue.then(function (res) {
            return 200

          });

        } else {
          queue = queue.then(function (res) {
            return 404

          });
        }

        queue.then(function (lastResponse) {
          res.status(lastResponse).send({ data: count2 })

        })


      })


    }
    else {
      queue = queue.then(function () {
        return 500
      })

      queue.then(function (lastResponse) {

        res.status(lastResponse).send({ data: true })

      })

    }



  });


})



router.post('/ForgotPassWord/:Email/', function (req, res) {
  var queue = Promise.resolve();
  ModLogin.findAll({
    where: {
      EMAIL: req.params.Email
    },
    attributes: ['PASSWORD'],
  }).then(x => {
    console.log(x.length)
    if (x.length == 1) {
      // return 200
      const Z = x[0].dataValues.PASSWORD
      var mailOptions = {
        from: '"Volunteer Network Afghanistan 👥" <volunteernetworkafghanistan@gmail.com>',
        to: req.params.Email,
        subject: 'Confirmation Email',
        html: 'Your Password is  ' + Z
      };
      queue = queue.then(function (res) {
        return 200

      });
      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          queue = queue.then(function (res) {
            return 406
          });

        }
        else {
          queue = queue.then(function (res) {
            return 202

          });
        }
      });
    } else {
      queue = queue.then(function (res) {
        return 500

      });
    }
    queue.then(function (lastResponse) {
      // console.log(lastResponse)
      res.status(lastResponse).send({ data: true })

    })

  });


})

// marconymeza@gmail.com


module.exports = router;