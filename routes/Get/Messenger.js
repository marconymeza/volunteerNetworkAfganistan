var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var app = express();
const Op = Sequelize.Op;
var sequelize = require('../../bin/ormConfig').sequelize;
var ModEntityMaster = require('../../models/EntityMaster').EntityMaster;
var ModMessenger = require('../../models/Messenger').Messenger;
var ModViewEntity = require('../../models/ViewEntityMaster').ViewEntity

var Models = {
  ModEvent: require('../../models/Events').Events,
}

router.post('/GetMessenger/:ID/', function (req, res) {
  var queue = Promise.resolve();
  ModMessenger.findAll({
    where: {
      [Op.or]: [
        { REMMIT_ID: req.params.ID },
        { RECEPTOR_ID: req.params.ID }
      ]
    },
    attributes: ['CURR_NO'],
    group: ['MESSENGER.CURR_NO'],
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/GetMessengerEntity/:ID/', function (req, res) {
  var queue = Promise.resolve();
  ModEntityMaster.findAll({
    where: {
      ENTITY_ID: req.params.ID
    },
    attributes: ['IMAGE','ENTITY_ID','NAME','TYPE_ENTITY']
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/GetMessengerAll/:ID/', function (req, res) {
  var queue = Promise.resolve();
  ModMessenger.findOne({
    where: {
      CURR_NO: req.params.ID
    }, order: [
      ['DATE', 'DESC']
    ],
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/GetMessengerCurrno/:ID/:ID2', function (req, res) {
  var queue = Promise.resolve();
  ModMessenger.findAll({
    where: {
      [Op.or]: [
        { CURR_NO: req.params.ID },
        { CURR_NO: req.params.ID2 }
      ]
    }, order: [
      ['DATE', 'ASC']
    ],
  }).then(x => {
    res.status(200).send({ data: x })
  });
});


module.exports = router;