import React from 'react'
import md5 from 'md5'
import uuidv1 from 'uuid/v1'
import cookie from 'react-cookies'
import Volunteer from './Volunteer/index-pt'
import Organization from './Organization/index-pt'
class CompleteInformation extends React.Component {
    constructor() {
        super()
        this.state = {
            Gender: [],
            Country: [],
            TypeOrganization: [],
            Province: [],
            Education: [],
            Sdg: [],
            Occupation: [],
            Interest:[],
            Expertise:[],
            District:[],
            OccupationList:[],
            Profession:[],
            ProfessionList:[],
            Blood: [],
            Abilities: [],
            Province: [],
            Period:[],
            Docs:[],
            xrt:[]}
            this.img = null
        this.gets = [
            {path:'ModNacionalityDescription/NACIONALITY_CODE',obj:"Country"},
            {path:'ModTypeOrganizationDescription/TYPE_ORGANIZATION_CODE',obj:"TypeOrganization"},
            {path:'ModEducationDescription/EDUCATION_CODE',obj:"Education"},
            {path:'ModProfessionDescription/PROFESSION_CODE',obj:"ProfessionDesc"},
            {path:'dp/ModAbilitiesDescription/ABILITIES_CODE/2',obj:"Abilities"},
            {path:'ModPeriod/PERIOD_CODE',obj:"Period"},            
            {path:'ModBloodDescription/BLOOD_GROUP_CODE',obj:"Blood"},]
        this.userInfo = cookie.load('dataUser')
    }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        return re.test(String(email).toLowerCase());
    }

    fillCombobox(url, array) {
        fetch(window.location.origin + '/Get/api/obtener/' + url, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    [array]: x.data
                })
            })
    }
    componentWillMount() {
        this.fillCombobox('ModProfessionDescription/PROFESSION_CODE', 'ProfessionList')
        this.fillCombobox('ModOccupation/OCCUPATION_CODE', 'OccupationList')
        fetch(window.location.origin +'/Get/new/provinceDescription',
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                "where":{"LANGUAGE":2}
               })
        })
        .then((response) => {
            return response.json()
                .then((x) => {
                    if (response.status === 200 || response.status === 202) {
                        this.setState({
                            Province:x.data
                        })
                    }
                    else {
                        console.log("nada Province")
                    }
                })
        })
        this.gets.map(x=>{
            fetch(window.location.origin + '/Get/api/obtener/'+x.path, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((y) => {
                this.setState({
                    [x.obj]: y.data
                })
            })})
        fetch(window.location.origin + '/Get/api/obtener/ModGenderDescription/GENDER_CODE', { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Gender: x.data.splice(0, 2)
                })
            })

        fetch(window.location.origin + '/Get/api/obtener/dp/ModSdgDescription/SDG_CODE/2', { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                delete x.data[17];
                
                this.setState({
                    Sdg: x.data
                })
            })

        fetch(window.location.origin + '/Get/GetCompleteInformation/' + this.userInfo.ENTITY_ID, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                if (this.userInfo.ENTITY_MASTER.TYPE_ENTITY == 1) {
                    if(x.data[0].IMAGE != null){
                        $("#blah").attr('src', '/images/orgImages/'+ x.data[0].IMAGE);                        
                        document.getElementById("blah").className = "img-fluid imgblah"
                            
                        }
                        var docss = []
                        console.log(x.data[0].DOCUMENT,"x.data[0].DOCUMENT")
                        if(x.data[0].DOCUMENT != "" && x.data[0].DOCUMENT !=null){
                            x.data[0].DOCUMENT.split("@_@").map(xs=>{
                                if(xs != "")
                                docss.push(xs.split("@-@"))
                            })
                            this.setState({
                                Docs:[x.data[0].DOCUMENT,docss]
                            })
                        }
                        
                    document.getElementById('name').value = x.data[0].NAME
                    document.getElementById('LegalIdentification').value = x.data[0].LEGAL_ID
                    document.getElementById('Telephone').value = x.data[0].TELEPHONE
                    document.getElementById('Nresponsible').value = x.data[0].NAME_RESPONSIBLE
                    document.getElementById('NTelephone').value = x.data[0].TELEPHONE_RESPONSIBLE
                    document.getElementById('Address').value = x.data[0].FREE_ADDRESS
                    document.getElementById('Twitter').value = x.data[0].TWITTER
                    this.img = x.data[0].IMG
                    document.getElementById('Google').value = x.data[0].GOOGLE
                    document.getElementById('Email').value = x.data[0].EMAIL
                    document.getElementById('Instagram').value = x.data[0].INSTAGRAM
                    document.getElementById('Google').value = x.data[0].GOOGLE
                    document.getElementById('Website').value = x.data[0].WEBSITE
                    document.getElementById('Email').value = x.data[0].EMAIL
                    document.getElementById('Facebook').value = x.data[0].FACEBOOK
                    document.getElementById('TypeOrganization').value = x.data[0].TYPE_ORGANIZATION
                    // document.getElementById('Latitud').value = ""
                    // document.getElementById('LatitudLab').className = 'active'
                    // document.getElementById('Longitud').value = ""
                    // document.getElementById('LongitudLab').className = 'active'
                    document.getElementById('WorkArea').value = x.data[0].WORK_AREA
                    document.getElementById('User').value = x.data[0].LOGIN.USER_NAME
                    document.getElementById('PassWord').value = x.data[0].LOGIN.PASSWORD
                    document.getElementById('Commentary').value = x.data[0].COMMENTARY

                    x.data.map(y => {
                        if(y.ENTITY_SDG != null)
                        document.getElementById(y.ENTITY_SDG.SDG).checked = true
                    })
                    x.data.map(y => {
                        if(y.ENTITY_SCOPE != null)
                        document.getElementById(y.ENTITY_SCOPE.SCOPE).checked = true
                        // this.state.Scopes.push(document.getElementById("label"+y.ENTITY_SCOPE.SCOPE).innerHTML)   
                   
                    })
                    
                } else if (this.userInfo.ENTITY_MASTER.TYPE_ENTITY == 2) {
                    if(x.data[0].IMAGE != null){
                    $("#blah").attr('src', '/images/volunteerImages/'+ x.data[0].IMAGE);                        
                    document.getElementById("blah").className = "img-fluid imgblah"
                        
                    }
                    document.getElementById('name').value = x.data[0].NAME
                    document.getElementById('LegalIdentification').value = x.data[0].LEGAL_ID
                    document.getElementById('Province').value = x.data[0].PROVINCE  
                    document.getElementById('Village').value = x.data[0].VILLAGE
                    document.getElementById('Telephone').value = x.data[0].TELEPHONE
                    document.getElementById('Address').value = x.data[0].FREE_ADDRESS
                    document.getElementById('Twitter').value = x.data[0].TWITTER
                    document.getElementById('Instagram').value = x.data[0].INSTAGRAM
                    document.getElementById('Facebook').value = x.data[0].FACEBOOK
                    document.getElementById('Google').value = x.data[0].GOOGLE
                    document.getElementById('Email').value = x.data[0].EMAIL
                    // document.getElementById('Nacionality').value = x.data[0].NACIONALITY
                    document.getElementById('Period').value = x.data[0].PERIOD
                    document.getElementById('Birth').value = x.data[0].BIRTH_DATE.substring(0,10)
                    document.getElementById('Gender').value = x.data[0].GENDER
                    document.getElementById('Blood').value = x.data[0].BLOOD_GROUP
                    document.getElementById('Education').value = x.data[0].EDUCATION
                    document.getElementById('User').value = x.data[0].LOGIN.USER_NAME
                    document.getElementById('PassWord').value = x.data[0].LOGIN.PASSWORD
                    document.getElementById('Commentary').value = x.data[0].COMMENTARY
                    this.img = x.data[0].IMG
                    
                    document.getElementById('Reference').value = x.data[0].NAME_RESPONSIBLE
                    document.getElementById('PhoneReference').value = x.data[0].TELEPHONE_RESPONSIBLE
                    fetch(window.location.origin + '/get/GetDistrict/'+x.data[0].PROVINCE, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((y) => {             
                this.setState({
                    District: y.data
                })
                document.getElementById('District').value = x.data[0].DISTRICT 
            })

            // WASACA
            var t =[...new Set(x.data.map(item => {
                if(item.ENTITY_OCCUPATION != null) 
                return item.ENTITY_OCCUPATION.OCCUPATION_ID
            }))]
            var tt =[...new Set(x.data.map(item => {
                if(item.ENTITY_OCCUPATION != null) 
                return item.ENTITY_OCCUPATION.OCCUPATION_DESCRIPTION
            }))]    
            if(t[0] != undefined)
            t.map((y,o)=>{
                this.state.Occupation.push({ id: y, description: tt[o] })
            })
                    x.data.map(k => {
                        
                        if(k.ENTITY_SDG != null )
                        {
                            if(k.ENTITY_SDG.SDG>=1 && k.ENTITY_SDG.SDG <=17)
                        document.getElementById(k.ENTITY_SDG.SDG).checked = true
                            
                        }
                    })
                    // x.data.map(k => {
                    //     if(k.ENTITY_SDG != null )
                    //     {
                    //         if(k.ENTITY_SDG.SDG.length > 3)
                    //             this.state.Interest.push({ id: k.ENTITY_SDG.SDG_ID, description: k.ENTITY_SDG.SDG_DESCRIPTION })
                                
                            
                    //     }
                    // })
                    
                    var t =[...new Set(x.data.map(item => {
                        if(item.ENTITY_ABILITy!=null)
                        return item.ENTITY_ABILITy.ENTITY_ID
                    }))]
                    var tt =[...new Set(x.data.map(item => {
                        if(item.ENTITY_ABILITy!=null)                        
                       return item.ENTITY_ABILITy.ABILITIES_DESCRIPTION
                    }))]
                    if(t[0] != undefined)
                    t.map((y,o)=>{
                        if(y != null)
                        if(y.length > 3)
                        this.state.Expertise.push({ id: y, description: tt[o] })
                    })
                            this.setState({
                                Expertise:this.state.Expertise
                            })
                            

                    var t =[...new Set(x.data.map(item => {
                        if(item.ENTITY_SDG!=null)                                                
                        return item.ENTITY_SDG.SDG
                    }))]
            var tt =[...new Set(x.data.map(item => {
                if(item.ENTITY_SDG!=null)                                                                
                return item.ENTITY_SDG.SDG_DESCRIPTION
            }))]
            if(t[0] != undefined)
            t.map((y,o)=>{
                if(y != null)
                if(y.length > 3)
                this.state.Interest.push({ id: y, description: tt[o] })
            })
                    this.setState({
                        Interest:this.state.Interest
                    })

                        this.setState({
                            Occupation:this.state.Occupation
                        })

                    var t =[...new Set(x.data.map(item => {
                if(item.ENTITY_PROFESSION!=null)                                                                                        
                        return item.ENTITY_PROFESSION.PROFESSION_ID
                    }))]
            var tt =[...new Set(x.data.map(item => {
                if(item.ENTITY_PROFESSION!=null)                                                                                                        
                return item.ENTITY_PROFESSION.PROFESSION_DESCRIPTION
            }))]
            if(t[0] != undefined)
            t.map((y,o)=>{
                this.state.Profession.push({ id: y, description: tt[o] })
            })
                    this.setState({
                        Profession:this.state.Profession
                    })

                    x.data.map(y => {
                        if(y.ENTITY_ABILITy != null)
                        if(y.ENTITY_ABILITy.ABILITIES>=1 && y.ENTITY_ABILITy.ABILITIES <=12)
                        document.getElementById("h"+y.ENTITY_ABILITy.ABILITIES).checked = true
                    })
                }
                
            })
    }
    changeDistrict(){
        fetch(window.location.origin + '/get/GetDistrict/'+document.getElementById('Province').value, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {             
                this.setState({
                    District: x.data
                })
            })
    }
    UpdateOrganization() {
        if(document.getElementById('file-upload').files[0] != undefined)
        this.img = document.getElementById('file-upload').files[0].name
        var data = {
            name: document.getElementById('name').value,
            Entity: this.userInfo.ENTITY_ID,
            LegalId: document.getElementById('LegalIdentification').value,
            Telephone: document.getElementById('Telephone').value,
            Nresponsible: document.getElementById('Nresponsible').value,
            NTelephone: document.getElementById('NTelephone').value,
            PassWord: document.getElementById('PassWord').value,
            User: document.getElementById('User').value,
            Facebook: document.getElementById('Facebook').value,
            Instagram: document.getElementById('Instagram').value,
            Twitter: document.getElementById('Twitter').value,
            Address: document.getElementById('Address').value,
            Google: document.getElementById('Google').value,
            img:this.img,
            Website: document.getElementById('Website').value,
            Latitud: "",
            Email: document.getElementById('Email').value,
            Longitud: "",
            TypeOrganization: document.getElementById('TypeOrganization').value,
            WorkArea: document.getElementById('WorkArea').value,
            Commentary: document.getElementById('Commentary').value,
            TypeEntity:this.userInfo.ENTITY_MASTER.TYPE_ENTITY,
            Sdg: [],
            Scope: []
            // Sdg:document.getElementById('Sdg').value
        }

        $("input:checkbox[name=Sdg]:checked").each(function () {
            data.Sdg.push($(this).val());
        });
        
        $("input:checkbox[name=Scope]:checked").each(function () {
            data.Scope.push($(this).val());
        });
        if (data.name == "") {
            alertify.error('نوم اړین دي');

        } else if (data.LegalId == "") {
            alertify.error('قانونی هویت اړین دي');
        } else if (data.PassWord == "") {
            alertify.error('پاسورډ/ رمز اړین دي');
        } else if (data.Email == "") {
            alertify.error('ایمیل نیاز است');
        }else if (data.Facebook != "" && this.validate_url(data.Facebook) != "facebook") {
            alertify.error('د فیسبوک یو ار ایل ناسم دي');
        }else if (data.Twitter != "" && this.validate_url(data.Twitter) != "twitter") {
            alertify.error('د ټویټر یو ار ایل ناسم دي');
        }else if (data.Instagram != "" && this.validate_url(data.Instagram) != "instagram") {
            alertify.error('د انسټاګرام یو ار ایل ناسم دي');
        }else if (data.Google != "" && this.validate_url(data.Google) != "google") {
            alertify.error('د ګوګل + یو ار ایل ناسم دي');
        } else if (data.User == "") {
            alertify.error('د کاروړونکې نوم اړین دي ');
        }
        else if(!this.validateEmail(data.Email))
        alertify.error('ایمیل نیاز است');
        else {

            (data.TypeOrganization == "")?data.TypeOrganization=99:0
           
            fetch(window.location.origin + '/Update/UpdateInformation/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {

                            if (response.status == 200 || response.status == 202) {
                                alertify.success('کړنه بریالۍ وه')
                                setTimeout(
                                    function(){
                                        document.getElementById("formImgOrg").submit()
                                    }, 2000);
                            }
                            else {
                                alertify.error('ناکامه هڅه')
                            }
                        })
                })
        }
    }

    validate_url(url) {
        if (/^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(url))
            return 'twitter';

        if (/^(https?:\/\/)?((w{3}\.)?)facebook.com\/.*/i.test(url))
            return 'facebook';

        if (/^(https?:\/\/)?((w{3}\.)?)instagram.com\/.*/i.test(url))
            return 'instagram';

        if (/^(https?:\/\/)?((w{3}\.)?)plus.google.com\/.*/i.test(url))
            return 'google';

        return 'unknown';
    }
    UpdateVolunteer() {
        if(document.getElementById('file-upload').files[0] != undefined)
        this.img = document.getElementById('file-upload').files[0].name
        var data = {
            name: document.getElementById('name').value,
            Entity: this.userInfo.ENTITY_ID,
            LegalId: document.getElementById('LegalIdentification').value,
            Telephone: document.getElementById('Telephone').value,
            PassWord: document.getElementById('PassWord').value,
            User: document.getElementById('User').value,
            Facebook: document.getElementById('Facebook').value,
            Instagram: document.getElementById('Instagram').value,
            Twitter: document.getElementById('Twitter').value,
            Address: document.getElementById('Address').value,
            img:this.img,
            Birth: document.getElementById('Birth').value,
            Gender: document.getElementById('Gender').value,
            Email:  document.getElementById('Email').value,
            Village:  document.getElementById('Village').value,
            Education: document.getElementById('Education').value,
            Province: document.getElementById('Province').value,
            District: document.getElementById('District').value,
            Nacionality: 99,
            Blood: document.getElementById('Blood').value,
            Nresponsible: document.getElementById('Reference').value,
            NTelephone:document.getElementById('PhoneReference').value,
            google:document.getElementById('Google').value,
            Commentary: document.getElementById('Commentary').value,
            Profession:this.state.Profession,
            Occupation:this.state.Occupation,
            Sdg: [],
            Period:document.getElementById('Period').value,            
            // nameImg:document.getElementById('file-upload').files[0].name,
            Abilities: [],
            TypeEntity: this.userInfo.ENTITY_MASTER.TYPE_ENTITY
        }


        $("input:checkbox[name=Sdg]:checked").each(function () {
            // data.Sdg.push($(this).val());
            data.Sdg.push({ id: $(this).val().split('#')[0], description: $(this).val().split('#')[1] })
        });

        this.state.Interest.map(x=>{
            data.Sdg.push({ id: x.id, description: x.description })
            
        })
        this.state.Expertise.map(x=>{
            data.Abilities.push({ id: x.id, description: x.description })
        })

        $("input:checkbox[name=Abilities]:checked").each(function () {
            data.Abilities.push({ id: $(this).val().split('#')[0], description: $(this).val().split('#')[1] })
            
            // data.Abilities.push($(this).val());
        });
    

        if (data.name == "") {
            alertify.error('نوم اړین دي');
            document.getElementById("name").className = "form-control error"

        } else if (data.LegalId == "") {
            alertify.error('قانونی هویت اړین دي');
        } else if (data.PassWord == "") {
            alertify.error('پاسورډ/ رمز اړین دي');
        } else if (data.Email == "") {
            alertify.error('ایمیل نیاز است');
        } else if (data.User == "") {
            alertify.error('د کاروړونکې نوم اړین دي ');
        }else if (data.Facebook != "" && this.validate_url(data.Facebook) != "facebook") {
            alertify.error('د فیسبوک یو ار ایل ناسم دي');
        }else if (data.Twitter != "" && this.validate_url(data.Twitter) != "twitter") {
            alertify.error('د ټویټر یو ار ایل ناسم دي');
        }else if (data.Instagram != "" && this.validate_url(data.Instagram) != "instagram") {
            alertify.error('د انسټاګرام یو ار ایل ناسم دي');
        }else if (data.google != "" && this.validate_url(data.google) != "google") {
            alertify.error('د ګوګل + یو ار ایل ناسم دي');
        } else if (data.nameImg == "") {
            data.nameImg = 'n'
        }
        else if(!this.validateEmail(data.Email))
        alertify.error('ایمیل نیاز است');
        else {

            if (data.Gender == "") {
                data.Gender=99
            }if (data.Blood == "") {
                data.Blood=99
            }if (data.Education == "") {
                data.Education=99
            }if (data.Period == "") {
                data.Period=99
            }if (data.Province == "") {
                data.Province=99
            }if (data.District == "") {
                data.District=99
            }if (data.Nacionality == "") {
                data.Nacionality=99
            }

            
            fetch(window.location.origin + '/Update/UpdateInformation/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status == 200 || response.status == 202) {
                                alertify.success('کړنه بریالۍ وه')
                                setTimeout(
                                    function(){
                                        document.getElementById("formImgVolunter").submit()
                                    }, 2000);
                            }
                            else {
                                alertify.error('ناکامه هڅه')
                            }
                        })
                })
        }
    }
    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }
    addItem(x) {
        this.state[x].push({ id: md5(document.getElementById(`txt${x}`).value), description: document.getElementById(`txt${x}`).value })
        this.setState({
            [x]: this.state[x]
        })
        document.getElementById("txt" + x).value = ""
    }
    deleteItem(array, item) {
        this.state[array].splice(item, 1)
        this.setState({
            [array]: this.state[array]
        })
    }
    addOptionToList(x) {
        this.state[x].push({ id: document.getElementById(`list${x}`).value.split('#')[0], description: document.getElementById(`list${x}`).value.split('#')[1] })
        this.setState({
            [x]: this.state[x]
        })
    }
    render() {
        if (this.userInfo.ENTITY_MASTER.TYPE_ENTITY == 1) {
            return (
                <Organization 
                    Sdg={this.state.Sdg}
                    id={this.userInfo.ENTITY_ID}
                    Province={this.state.Province}
                    Docs={this.state.Docs}
                    sxt={this.state.xrt}
                    TypeOrganization={this.state.TypeOrganization}
                    UpdateOrganization={this.UpdateOrganization.bind(this)}
                />
            )
        } else if (this.userInfo.ENTITY_MASTER.TYPE_ENTITY == 2) {
            return (
                <Volunteer 
                    entityId={this.userInfo.ENTITY_ID}
                    Sdg={this.state.Sdg}
                    Gender={this.state.Gender}
                    Province={this.state.Province}
                    addOptionToList={this.addOptionToList.bind(this)}
                    Country={this.state.Country}  
                    deleteItem={this.deleteItem.bind(this)}
                    District={this.state.District}
                    Profession={this.state.Profession}
                    addItem={this.addItem.bind(this)}
                    changeDistrict={this.changeDistrict.bind(this)}
                    Period={this.state.Period}
                    ProfessionList = {this.state.ProfessionList}
                    Education={this.state.Education}
                    Occupation={this.state.Occupation}
                    Interest={this.state.Interest}
                    Expertise={this.state.Expertise}
                    OccupationList = {this.state.OccupationList}
                    Blood={this.state.Blood}
                    Abilities={this.state.Abilities}
                    UpdateVolunteer={this.UpdateVolunteer.bind(this)}
                />
            )
        }
    }
}

export default CompleteInformation