import React from 'react'
import CardsOrg from './CardsOrg-pt'
import CardsVlt from './CardsVlt-pt'
import CardsEvents from './CardEvent-pt'
import CardsVacancy from './CardVc-pt'
import uuidv1 from 'uuid/v1'
import md5 from 'md5'

class Organizations extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      organizations: [],
      Volunteer: [],
      Events: [],
      Vacancy: [],
      selectData: 'org',
      SdgDesc: [],
      AbilitiesDesc:[],
      clearSelection:0
    }
    this.oldNav = "org"
    this.btnGerDataByFilter = this.btnGerDataByFilter.bind(this)
    this.orgCount ={
      organizations:0,
      Volunteer:0,
      Events:0,
      Vacancy:0
    }

  }
  fillCombobox(url, array){
    fetch(window.location.origin +'/Get/api/obtener/'+ url, { method: 'post' })
    .then((response) => {
      return response.json()
    })
    .then((x) => {
      this.setState({
        [array]: x.data
      })

    })
  }
  getDataCard(url,array){
    this.scrollDown(array)
    fetch(`${window.location.origin}${url}${this.orgCount[array]}`, { method: 'post' })
      .then((response) => {
        return response.json()
      })
      .then((x) => {
        var data = []
        x.data.map((r, y) => {
          this.state[array].push(r)
        })
        this.setState({ [array]: this.state[array] })
        this.orgCount[array] = this.orgCount[array] + 8
      })
  }
  scrollDown(x){
    if(this.orgCount[x]>1){
      this.scrollSmoothToBottom()
    }
  }
  scrollSmoothToBottom(id) {
    $(document.scrollingElement).animate({
      scrollTop: document.body.scrollHeight
    }, 500);
  }
  selectData(x) {
    document.getElementById("nav"+this.oldNav).className = "nav-link"
    document.getElementById("nav"+x).className = "nav-link active"
    this.oldNav = x
    $('body,html').animate({scrollTop: 800}, 800); 
    this.setState({
      selectData: x
    })
  }
  clearSelection(){
    this.orgCount ={
      organizations:0,
      Volunteer:0,
      Events:0,
      Vacancy:0
    }
    this.setState({
      organizations: [],
    Volunteer: [],
    Events: [],
    Vacancy: []
    })
    
    this.getDataCard('/Get/EntityHome/1/','organizations')
    this.getDataCard('/Get/EntityHome/2/','Volunteer')
    this.getDataCard('/Get/EventHome/','Events')
    this.getDataCard('/Get/VacancyHome/','Vacancy')
    this.setState({
      clearSelection:0
    })
    document.getElementById("noResult").innerHTML = "" 
    
  }
  componentDidMount() {
    this.fillCombobox('ModSdgDescription/SDG_CODE','SdgDesc')
    this.fillCombobox('dp/ModAbilitiesDescription/ABILITIES_CODE/2','AbilitiesDesc')
    this.getDataCard('/Get/EntityHome/1/','organizations')
    this.getDataCard('/Get/EntityHome/2/','Volunteer')
    this.getDataCard('/Get/EventHome/','Events')
    this.getDataCard('/Get/VacancyHome/','Vacancy')
  }
  btnGerDataByFilter(event) {
    event.preventDefault();
    if(document.querySelector('input[name="inlineRadioOptions"]:checked') == null)
    {
      alertify.error('د متن بکس لاندې یو اختیار غوره کړئ');
    }else if(!document.getElementById('txtFilter').value)
    {
      alertify.error('متن خالې ده');
    }else{
      switch (document.querySelector('input[name="inlineRadioOptions"]:checked').value) {
        case 'org':
          this.state.organizations=[]        
          this.getDataByFilter('org', 'ModEntityMaster', 'NAME', 'organizations', '1')
          break;
        case 'vcy':
          this.state.Vacancy=[]
          this.getDataByFilter('vcy', 'ModVacancy', 'TITLE', 'Vacancy','3')
          break;
        case 'eve':
          this.state.Events=[]
          this.getDataByFilter('eve', 'ModEvent', 'TITULE', 'Events','3')
          break;
        case 'vlt':
          this.state.Volunteer=[]
          this.getDataByFilter('vlt', 'ModEntityMaster', 'NAME', 'Volunteer', '2')
          break;
        default:
          null
          break;
      }
    }
    this.setState({
      clearSelection:1
    })

    // this.orgCount ={
    //   organizations:1,
    //   Volunteer:1,
    //   Events:1,
    //   Vacancy:1
    // }
    // this.setState({
    //   orgCount:this.orgCount
    // })
    this.scrollSmoothToBottom()
    
  }
  getDataByFilter(typeFilter, model, colum, stateName, TypeEntity,event) {
    this.deslectOptionList()
    document.getElementById(`list-${typeFilter}-list`).className = "list-group-item list-group-item-action active"
    this.selectData(typeFilter)
    fetch(`${window.location.origin}/Get/FilterLike/${model}/${colum}/${document.getElementById('txtFilter').value}/${TypeEntity}`, { method: 'post' })
      .then((response) => {
        return response.json()
      })
      .then((x) => {
        if(x.data.length==0)
        document.getElementById("noResult").innerHTML = " کومې پایلې و نه موندل شوي" 
        else
        document.getElementById("noResult").innerHTML = "" 
        this.setState({ [stateName]: x.data})
      })
  }
  deslectOptionList() {
    document.getElementById("list-org-list").className = "list-group-item list-group-item-action"
    document.getElementById("list-vlt-list").className = "list-group-item list-group-item-action"
    document.getElementById("list-vcy-list").className = "list-group-item list-group-item-action"
    document.getElementById("list-eve-list").className = "list-group-item list-group-item-action"
  }
  render() {

    return (
      <section >
        <section id="organizations">
          <div className="container" id="myHeader" >
            <div className="row">
              <div className="offset-2 text-center col-8 d">
                <div className="form-group formSearch">
                  <h1 className="whiteText">وپلټئ</h1>
                  <form onSubmit={this.btnGerDataByFilter}>
                  <input className="form-control form-control-lg text-right transparentImput" type="text" id="txtFilter" placeholder="لټون" /> <br />
                  <div className="form-check form-check-inline">
                    <input className="form-check-input checkGrande" type="radio" name="inlineRadioOptions" value="org" />
                    <label className="form-check-label whiteText" htmlFor="inlineRadio1"><h5>سازمانونه/ ادارې</h5></label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="inlineRadioOptions" value="vlt" />
                    <label className="form-check-label whiteText" htmlFor="inlineRadio1"><h5>رضاکاران</h5></label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="inlineRadioOptions" value="vcy" />
                    <label className="form-check-label whiteText" htmlFor="inlineRadio1"><h5>بستونه</h5></label>
                  </div><div className="form-check form-check-inline">
                    <input className="form-check-input" type="radio" name="inlineRadioOptions" value="eve" />
                    <label className="form-check-label whiteText" htmlFor="inlineRadio1"><h5>پروګرام/ برنامه</h5></label>
                  </div> <br />
                  <button type="submit" className="btn btn-outline-light ">لټون</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="container-fluid topSpace">
          <div className="row">
          <div className="col-12 bottomAndTopSpace">
          
          <ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" id="navorg" onClick={this.selectData.bind(this, 'org')} href="#">سازمانونه/ ادارې</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="navvlt" onClick={this.selectData.bind(this, 'vlt')} href="#">رضاکاران </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="navvcy" onClick={this.selectData.bind(this, 'vcy')}  href="#">بستونه</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " id="naveve"onClick={this.selectData.bind(this, 'eve')} href="#">پروګرام/ برنامه</a>
  </li>
</ul>
          </div>
            {/* <div className="col-2 control-red">
              <div className="list-group" id="list-tab" role="tablist">
                <a className="list-group-item list-group-item-action active" onClick={this.selectData.bind(this, 'org')} id="list-org-list" data-toggle="list" href="#list-home" role="tab" aria-controls="org">/a>
                <a className="list-group-item list-group-item-action" onClick={this.selectData.bind(this, 'vlt')} id="list-vlt-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="vlt"></a>
                <a className="list-group-item list-group-item-action" onClick={this.selectData.bind(this, 'vcy')} id="list-vcy-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="vcy"></a>
                <a className="list-group-item list-group-item-action" onClick={this.selectData.bind(this, 'eve')} id="list-eve-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="eve"></a>
              </div>
              <hr className="redW" />
              <select className="form-control">
                <option >دومداره پرمختیایی موخې</option>
                {this.state.SdgDesc.map(x => {
                  return (
                    <option key={uuidv1()} name='sdgx' id={x.SDG_CODE}   value={x.SDG_DESCRIPTION}>{x.SDG_DESCRIPTION}</option>
                  )
                })}
              </select>
              <br />
              <select className="form-control">
                <option >وړتیاوي</option>
                {this.state.AbilitiesDesc.map(x => {
                  return (

                    <option key={uuidv1()} name={x.ABILITIES_CODE} id={x.ABILITIES_CODE}  value={x.ABILITIES_DESCRIPTION}>{x.ABILITIES_DESCRIPTION}</option>
                  )
                })}

              </select>
            </div> */}
            {
              this.state.selectData == 'org' ? <CardsOrg organizations={this.state.organizations} getOrg={this.getDataCard.bind(this)} clearSelection={this.state.clearSelection} clearSelectionFunc={this.clearSelection.bind(this)}/> :
                this.state.selectData == 'vlt' ? <CardsVlt Volunteer={this.state.Volunteer} getVol={this.getDataCard.bind(this)} clearSelection={this.state.clearSelection} clearSelectionFunc={this.clearSelection.bind(this)} /> :
                  this.state.selectData == 'eve' ? <CardsEvents Events={this.state.Events} getEvent={this.getDataCard.bind(this)} clearSelection={this.state.clearSelection} clearSelectionFunc={this.clearSelection.bind(this)} cols="col-10"/> :
                    this.state.selectData == 'vcy' ? <CardsVacancy Vacancy={this.state.Vacancy} getVacancy={this.getDataCard.bind(this)} clearSelection={this.state.clearSelection} clearSelectionFunc={this.clearSelection.bind(this)} cols="col-10" /> :
                      <CardsOrg organizations={this.state.organizations} getOrg={this.getDataCard.bind(this)} clearSelection={this.state.clearSelection} clearSelectionFunc={this.clearSelection.bind(this)} />
            }

          </div>
        </div>
      </section>
    )
  }
}
export default Organizations