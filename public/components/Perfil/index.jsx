import React from 'react'
import { get } from 'https';
import random from 'uuid/v1'
import ReactTooltip from 'react-tooltip'
import CardsEvents from '../Home/CardEvent'
import CardsVacancies from '../Home/CardVcy'
import cookie from 'react-cookies'

class Perfil extends React.Component {
    constructor() {
        super()
        this.userInfo = cookie.load('dataUser')
        this.state = {
            Sdg: [],
            Province: [],
            data: [],
            Events: [],
            Docs: [],
            Vacancy: [],
            isFollowing: 0
        }
    }
    componentDidMount() {
        this.getPerfil()
        this.getEvents()
        this.getVacancy()
        this.isFollowing()
    }
    isFollowing() {

        fetch(`${window.location.origin}/get/ValidateRequestEntity/${this.userInfo.ENTITY_ID}/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                console.log(x)
                this.setState({
                    isFollowing: x.data[0].REQUEST_STATUS
                })
            })
    }
    Follow() {
        var data = {
            Entity: this.props.id,
            Rentity: this.userInfo.ENTITY_ID,
            ID:random(),
            ENTITY_ORG:this.props.id
        }



   
        fetch(window.location.origin + '/Insert/NotificationFollow/',
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
        .then((response) => {
            return response.json()
                .then((x) => {
                    
                    console.log(x,"mar",response.status)

                })
        })




        fetch(window.location.origin + '/Insert/FollowEntity/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202) {
                            alertify.success('Following');
                            this.setState({
                                isFollowing: 1
                            })
                        }
                        else
                            alertify.success('Failed');

                    })
            })
    }
    UnFollow() {
        var data = {
            Entity: this.props.id,
            Rentity: this.userInfo.ENTITY_ID
        }
        fetch(window.location.origin + '/Update/UnFollowEntity/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        console.log("x",x.data,"response.status",response.status)
                        if (response.status == 200 || response.status == 202) {
                            alertify.success('Not Following');
                            this.setState({
                                isFollowing: 0
                            })
                        }
                        else
                            alertify.success('Failed');

                    })
            })
    }
    getEvents() {
        fetch(`${window.location.origin}/Get/GetEvent/2/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Events: x.data
                })
            })
    }
    getVacancy() {
        fetch(`${window.location.origin}/Get/GetVacancy/2/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    Vacancy: x.data
                })
            })
    }
    getPerfil() {
        fetch(`${window.location.origin}/Get/GetCompleteInformation/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    data: x.data[0]
                })

                var docss = []
                if (x.data[0].DOCUMENT != "") {
                    x.data[0].DOCUMENT.split("@_@").map(xs => {
                        if (xs != "")
                            docss.push(xs.split("@-@"))

                    })
                    this.setState({
                        Docs: [x.data[0].DOCUMENT, docss]
                    })
                }

                if (x.data[0].IMAGE != undefined && x.data[0].IMAGE != null) {
                    $("#blah").attr('src', '/images/orgImages/' + x.data[0].IMAGE);
                    document.getElementById("blah").className = "img-fluid imgblah"
                }

                var t = [...new Set(x.data.map(item => {
                    if (item.ENTITY_SDG != null)
                        return item.ENTITY_SDG.SDG
                }))]
                var tt = [...new Set(x.data.map(item => {
                    if (item.ENTITY_SDG != null)
                        return item.ENTITY_SDG.SDG_DESCRIPTION
                }))]
                if (t[0] != undefined)
                    t.map((y, o) => {
                        if (y != null)

                            this.state.Sdg.push(y)
                    })


                var t2 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_SCOPE != null)
                        return item.ENTITY_SCOPE.SCOPE
                }))]
                var tt2 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_SCOPE != null)
                        return item.ENTITY_SCOPE.PROVINCE_DESCRIPTION.PROVINCE_DESCRIPTION
                }))]
                if (t2[0] != undefined)
                    t2.map((y, o) => {
                        if (y != null)
                            this.state.Province.push({ id: y, description: tt2[o] })
                    })
                this.setState({
                    Sdg: this.state.Sdg,
                    Province: this.state.Province
                })



            })
    }
    render() {
        return (
            <section id="profileOrg">
                <div className="container-fluid">
                    <ReactTooltip />
                    <div className="row topSpace-1x">
                        <div className="col-12 text-left" data-tip="Go back"><a href="/home"><i className="fas fa-arrow-circle-left fa-2x redText"></i></a></div>

                    </div>
                    <div className="row">
                        <div className="col-12 text-center withoutPadding">
                            <img src="/images/Home/picture.png" onError={(e)=>{e.target.src="/images/orgImages/defaultOrg.jpg"}} className="img-fluid topSpace" id="blah" />
                            <h3>{this.state.data.NAME}</h3>

                            {this.state.data.LEGAL_ID != null && this.state.data.LEGAL_ID != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Legal ID:</strong> {this.state.data.LEGAL_ID}</p> : null}

                            {this.state.data.EMAIL != null && this.state.data.EMAIL != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Email:</strong> {this.state.data.EMAIL}</p> : null}

                            {this.state.data.TELEPHONE != null && this.state.data.TELEPHONE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Phone:</strong> {this.state.data.TELEPHONE}</p> : null}

                            {this.state.data.WEBSITE != null && this.state.data.WEBSITE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Website:</strong> {this.state.data.WEBSITE}</p> : null}

                            {this.state.data.GOOGLE != null && this.state.data.GOOGLE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Google+:</strong> {this.state.data.GOOGLE}</p> : null}

                            {this.state.data.FACEBOOK != null && this.state.data.FACEBOOK != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Facebook:</strong> {this.state.data.FACEBOOK}</p> : null}

                            {this.state.data.INSTAGRAM != null && this.state.data.INSTAGRAM != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Instagram:</strong> {this.state.data.INSTAGRAM}</p> : null}

                            {this.state.data.TWITTER != null && this.state.data.TWITTER != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Twitter:</strong> {this.state.data.TWITTER}</p> : null}

                            {this.state.data.NAME_RESPONSIBLE != null && this.state.data.NAME_RESPONSIBLE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Contact Person:</strong> {this.state.data.NAME_RESPONSIBLE}</p> : null}

                            {this.state.data.TELEPHONE_RESPONSIBLE != null && this.state.data.TELEPHONE_RESPONSIBLE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Telephone of Contact Person:</strong> {this.state.data.TELEPHONE_RESPONSIBLE}</p> : null}

                            {this.state.data.WORK_AREA != null && this.state.data.WORK_AREA != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Mandate of Organization:</strong> {this.state.data.WORK_AREA}</p> : null}
                            <hr />
                        </div>

                        <div className="col-12 text-center">


                            <ul className="nav nav-pills mb-3 center-pills" id="pills-tab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="pills-Description-tab" data-toggle="pill" href="#pills-Description" role="tab" aria-controls="pills-Description" aria-selected="true">Description</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-Geographical-tab" data-toggle="pill" href="#pills-Geographical" role="tab" aria-controls="pills-Geographical" aria-selected="false">Geographical coverage</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-SDG-tab" data-toggle="pill" href="#pills-SDG" role="tab" aria-controls="pills-SDG" aria-selected="false">Ong’s SDG</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-LD-tab" data-toggle="pill" href="#pills-LD" role="tab" aria-controls="pills-LD" aria-selected="false">Legal documents</a>
                                </li>
                            </ul>
                            <div className="tab-content bottomSpace" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-Description" role="tabpanel" aria-labelledby="pills-Description-tab">
                                    {this.state.data.COMMENTARY != null && this.state.data.COMMENTARY != "" ?
                                        <div className="col-4 offset-4 text-center topSpace-2x">
                                            <p className="card-text withoutPadding withoutMargin"><strong>{this.state.data.COMMENTARY}</strong></p>
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-Geographical" role="tabpanel" aria-labelledby="pills-Geographical-tab">
                                    {this.state.Province.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            {this.state.Province.map(x => {
                                                return (
                                                    <span key={random()} className="badge badge-info marginRigth">{x.description}</span>
                                                )
                                            })}
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-SDG" role="tabpanel" aria-labelledby="pills-SDG-tab">
                                    {this.state.Sdg.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            <h5>Ong’s SDG's</h5>
                                            {this.state.Sdg.map(x => {
                                                return (
                                                    <img key={random()} src={`/images/SDG/${x}.jpg`} className="miniImg marginRigth" />
                                                )
                                            })}
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-LD" role="tabpanel" aria-labelledby="pills-LD-tab">
                                    <div className="col-4 offset-4">
                                        {this.state.Docs.length > 0 ?
                                            this.state.Docs[1].map(x => {
                                                return (
                                                    <li className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span>{x[0]}</span>
                                                        {/* <i className="fas fa-times redText"></i> */}
                                                        <a href={`/images/legalDocs/${x[1]}`} target="_blank"><span className=" badge-pill"><i className="fas fa-eye redText marginRigth"></i></span></a>
                                                    </li>
                                                )
                                            }) :
                                            null}
                                    </div>
                                </div>
                            </div>
                            {this.state.isFollowing == 0 ?
                                <a data-tip="Follow" onClick={this.Follow.bind(this)} className="marginRigth"><i className="fas fa-check fa-2x redText"></i></a>
                                :
                                <a data-tip="Following" onClick={this.UnFollow.bind(this)} className="marginRigth"><i className="fas fa-check fa-2x blueText"></i></a>

                            }
                            <a href="/chat" data-tip="Message"><i className="fas fa-comments fa-2x redText"></i></a>
                            <br /><br />
                            <hr />

                        </div>

                        <div className="col-10 offset-1 text-center">
                            <ul className="nav nav-pills mb-3 center-pills" id="myTab" role="tablist">

                                <li className="nav-item">
                                    <a className="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Events from this organization</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Vacancies from this organization</a>
                                </li>
                            </ul>
                            <div className="tab-content" id="myTabContent">
                                <div className="tab-pane fade  show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <CardsEvents Events={this.state.Events} getEvent={null} cols="col-12" />

                                </div>
                                <div className="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <CardsVacancies Vacancy={this.state.Vacancy} getVacancy={null} cols="col-12" />

                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </section>
        )
    }
}

export default Perfil