import React from 'react'


class AccessDenied extends React.Component {
    render(){
        return(
            <section>
                <div className="container">
                    <div className="row topSpace">
                        <div className="text-center topSpace blueText col-12">
                        <i class="fas fa-ban fa-7x"></i> <br/>
                        <h4 className="topSpace-1x">Access denied, only Organizations can see this information</h4>
                        <br/>
                        <a className="btn btn-info" href="/home">Go to Home</a>

                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default AccessDenied
