const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const BLOOD_GROUP_DESCRIPTION = sequelize.define('BLOOD_GROUP_DESCRIPTION', 
    {
      // definicion del los campos
      BLOOD_GROUP_CODE: 
      {
        type: Sequelize.NUMERIC,
        primaryKey: true,
        allowNull: false
      },
      BLOOD_GROUP_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'BLOOD_GROUP_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.BloodGroupDescription =  BLOOD_GROUP_DESCRIPTION


