const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ENTITY_PROFESSION = sequelize.define('ENTITY_PROFESSION', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },    
      PROFESSION_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      PROFESSION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'ENTITY_PROFESSION',
      timestamps: false,
      freezeTableName: true,
    });
    
 
module.exports.EntityProfession = ENTITY_PROFESSION


