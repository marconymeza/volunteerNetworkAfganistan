const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const MESSENGER = sequelize.define('MESSENGER', 
    {
      // definicion del los campos
      MESSENGER_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      REMMIT_ID: 
      {
        type: Sequelize.STRING
      },
      RECEPTOR_ID: 
      {
        type: Sequelize.STRING
      },
      REMMIT_STATUS: 
      {
        type: Sequelize.BOOLEAN
      },
      MESSAGE: 
      {
        type: Sequelize.STRING
      },
      STATUS: 
      {
        type: Sequelize.NUMERIC
      },
      CURR_NO: 
      {
        type: Sequelize.STRING
      },
      DATE: 
      {
        type: Sequelize.STRING
      },
      RECEPTOR_STATUS: 
      {
        type: Sequelize.BOOLEAN
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'MESSENGER',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.Messenger = MESSENGER


