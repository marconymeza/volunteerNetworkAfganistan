const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VACANCY_TYPE_DESCRIPTION = sequelize.define('VACANCY_TYPE_DESCRIPTION', 
    {
      // definicion del los campos
      VACANCY_TYPE_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      VACANCY_TYPE_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VACANCY_TYPE_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.VacancyTypeDescription = VACANCY_TYPE_DESCRIPTION


