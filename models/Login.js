const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const LOGIN = sequelize.define('LOGIN', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      EMAIL: 
      {
        type: Sequelize.STRING
      },
      USER_NAME: 
      {
        type: Sequelize.STRING
      },
      PASSWORD: 
      {
        type: Sequelize.STRING
      },
      OLD_PASSWORD: 
      {
        type: Sequelize.STRING
      },
      STATUS: 
      {
        type: Sequelize.NUMERIC,
        primaryKey: true,
        allowNull: false
      },
      DATE: 
      {
        type: Sequelize.DATE
      },
      KEY: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'LOGIN',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.Login = LOGIN


