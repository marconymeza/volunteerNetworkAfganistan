const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const NOTIFICATION_CENTER = sequelize.define('NOTIFICATION_CENTER', 
    {
      // definicion del los campos
      ID_NOTIFICATION: 
      {
        type: Sequelize.STRING
      },
      ENTITY_ORG:{
        type: Sequelize.STRING,
        allowNull: false
      },

      ENTITY_ID_PRIMARY: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      ENTITY_ID_SECONDARY: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      DATE: 
      {
        type: Sequelize.DATE
      },
      STATUS: 
      {
        type: Sequelize.BOOLEAN
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'NOTIFICATION_CENTER',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.NotificationCenter =  NOTIFICATION_CENTER
