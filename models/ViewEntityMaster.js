const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;

    const VIEW_ENTITY_MASTER = sequelize.define('VIEW_ENTITY_MASTER', 
    {

      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      TYPE_ENTITY: 
      {
        type: Sequelize.NUMERIC
      },
      LEGAL_ID: 
      {
        type: Sequelize.STRING
      },
      NAME: 
      {
        type: Sequelize.STRING,
        allowNull: false
      },
      EMAIL: 
      {
        type: Sequelize.STRING
      },
      TELEPHONE: 
      {
        type: Sequelize.STRING
      },
      FACEBOOK: 
      {
        type: Sequelize.STRING
      },
      INSTAGRAM: 
      {
        type: Sequelize.STRING
      },
      TWITTER: 
      {
        type: Sequelize.STRING
      },
      GOOGLE: 
      {
        type: Sequelize.STRING
      },
      WEBSITE: 
      {
        type: Sequelize.STRING
      },
      BIRTH_DATE: 
      {
        type: Sequelize.DATE
      },
      WORK_AREA: 
      {
        type: Sequelize.STRING
      },
      LATITUD: 
      {
        type: Sequelize.STRING
      },
      LONGITUDE: 
      {
        type: Sequelize.STRING
      },
      NAME_RESPONSIBLE: 
      {
        type: Sequelize.STRING
      },
      TELEPHONE_RESPONSIBLE: 
      {
        type: Sequelize.STRING
      },
      FREE_ADDRESS: 
      {
        type: Sequelize.STRING
      },
      COMMENTARY: 
      {
        type: Sequelize.STRING
      },
      ABILITIES_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      BLOOD_GROUP_DESCRIPTION : 
      {
        type: Sequelize.STRING
      },
      GENDER_DESCRIPTION : 
      {
        type: Sequelize.STRING
      },
      NACIONALITY_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      OCCUPATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      PROFESSION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      PROVINCE_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
      ,
      TYPE_ENTITY_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
      ,
      EDUCATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
      ,
      SDG: 
      {
        type: Sequelize.STRING
      }
      ,
      SCOPE: 
      {
        type: Sequelize.STRING
      } ,
      IMAGE: 
      {
        type: Sequelize.STRING
      }
       
       
    },
    // Mas configuracion del modelo
    {
      tableName: 'VIEW_ENTITY_MASTER',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.ViewEntity = VIEW_ENTITY_MASTER


