const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ENTITY_MASTER = sequelize.define('ENTITY_MASTER', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      TYPE_ENTITY: 
      {
        type: Sequelize.NUMERIC
      },
      TYPE_ORGANIZATION: 
      {
        type: Sequelize.NUMERIC
      },
      LEGAL_ID: 
      {
        type: Sequelize.STRING
      },
      NAME: 
      {
        type: Sequelize.STRING,
        allowNull: false
      },
      EMAIL: 
      {
        type: Sequelize.STRING
      },
      STATUS: 
      {
        type: Sequelize.NUMERIC
      },
      TELEPHONE: 
      {
        type: Sequelize.STRING
      },
      FACEBOOK: 
      {
        type: Sequelize.STRING
      },
      INSTAGRAM: 
      {
        type: Sequelize.STRING
      },
      TWITTER: 
      {
        type: Sequelize.STRING
      },
      GOOGLE: 
      {
        type: Sequelize.STRING
      },
      WEBSITE: 
      {
        type: Sequelize.STRING
      },
      BLOOD_GROUP: 
      {
        type: Sequelize.NUMERIC
      },
      GENDER: 
      {
        type: Sequelize.NUMERIC
      },
      BIRTH_DATE: 
      {
        type: Sequelize.DATE
      },
      NACIONALITY: 
      {
        type: Sequelize.STRING
      },
      PROVINCE: 
      {
        type: Sequelize.STRING
      },
      WORK_AREA: 
      {
        type: Sequelize.STRING
      },
      LATITUD: 
      {
        type: Sequelize.STRING
      },
      LONGITUDE: 
      {
        type: Sequelize.STRING
      },
      GRADE: 
      {
        type: Sequelize.STRING
      },
      EDUCATION: 
      {
        type: Sequelize.STRING
      },
      NAME_RESPONSIBLE: 
      {
        type: Sequelize.STRING
      },
      TELEPHONE_RESPONSIBLE: 
      {
        type: Sequelize.STRING
      },
      FREE_ADDRESS: 
      {
        type: Sequelize.STRING
      },
      COMMENTARY: 
      {
        type: Sequelize.STRING
      },
      IMAGE: 
      {
        type: Sequelize.STRING
      },
      DISTRICT: 
      {
        type: Sequelize.STRING
      },
      PERIOD: 
      {
        type: Sequelize.STRING
      },
      VILLAGE:
      {
        type: Sequelize.STRING
      },
      DOCUMENT:
      {
        type: Sequelize.STRING
      }
       
       
    },
    // Mas configuracion del modelo
    {
      tableName: 'ENTITY_MASTER',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.EntityMaster = ENTITY_MASTER


