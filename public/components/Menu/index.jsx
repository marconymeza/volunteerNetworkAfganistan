import React from 'react'
import uuidv1 from 'uuid/v1'
import cookie from 'react-cookies'

class Menu extends React.Component {
    constructor() {
        super()
        this.state = { dataUser: cookie.load('dataUser'),notifications:[] ,notificationsState:0}
        this.onLogout = this.onLogout.bind(this)
        this.pil = []
    }
    componentWillMount(){
       this.seeNotification()
    }
    seeNotification(){
        // window.setInterval(function(){
            if(this.state.dataUser !=undefined){
                fetch(window.location.origin + '/get/RequestInformationEventMe/' + this.state.dataUser.ENTITY_ID, { method: 'post' })
                .then((response) => {
                    return response.json()
                })
                .then((y) => { 
                    this.setState({
                        
                        notificationsState: y.data.length,
                        notifications:y.data
                    })
                    // var grouped = _.mapValues(_.groupBy(y.data, 'ENTITY_ID_SECONDARY'),
                    //         clist => clist.map(car => _.omit(car, 'ENTITY_ID_SECONDARY')));
                    //     y.data = []
                    //     for (var propertyName in grouped) {
                    //         y.data.push({ id: propertyName,url:'/activity',mod:'ModRequestEvent',field:'EVENTS_ID', name:grouped[propertyName][0].EVENT.TITULE ,values: grouped[propertyName] })
                    //     }
                    //     console.log(y.data)
                    //             this.pil=y.data

                    //             if(this.pil.length != this.state.notifications.length && this.pil.length >0)
                    //             {
                    //                 this.setState({
                    //                     notifications:this.pil
                    //                 })
                    //             }  
                    //         fetch(window.location.origin + '/get/RequestInformationVacancyMe/' + this.state.dataUser.ENTITY_ID, { method: 'post' })
                    //         .then((response) => {
                    //             return response.json()
                    //         })
                    //         .then((x) => {
                                
                                
                    //             var grouped = _.mapValues(_.groupBy(x.data, 'VACANCY_ID'),
                    //                     clist => clist.map(car => _.omit(car, 'VACANCY_ID')));
                    //                 x.data = []
                    //                 for (var propertyName in grouped) {
                    //                     x.data.push({ id: propertyName,url:'/vacancy',mod:'ModRequestVacancy',field:'VACANCY_ID', name:grouped[propertyName][0].VACANCY_ANNOUNCEMENT.TITLE ,values: grouped[propertyName] })
                    //                 }
                    //                 // if(this.state.notifications.length != x.data.length)
                    //                 x.data.map(p=>{
                    //                     this.pil.push(p)
                    //                 })
                    //                 if(this.pil.length != this.state.notifications.length)
                    //                 {
                    //                     this.setState({
                    //                         notifications:this.pil
                    //                     })
                    //                 }                    
                                    
                    //         })
                         
                })
        
              
        
            }
        // }.bind(this), 5000);
        
    }
    onLogout() {
        cookie.remove('dataUser', { path: '/' })
        window.open('/', "_self")
    }
    changeLanguage(l) {
        cookie.save('language', JSON.stringify({ language: l }), { path: '/' })
        location.reload();
    }

    changeStatus(){
        this.state.notifications.map(x=>{
            fetch(`${window.location.origin}/update/UpdateNotification/ModNotificationCenter/ENTITY_ORG/${this.state.dataUser.ENTITY_ID}`, { method: 'post' })
                .then((response) => {
                    return response.json()
                })
                .then((x) => {
                })
        })
    }
    render() {
        ///////// LOGIN //////////
        if (this.state.dataUser) {

            ///////////////// ORGANIZATIONS ///////////////////////
            if (this.state.dataUser.ENTITY_MASTER.TYPE_ENTITY == 1)
                return (
                    <div className="container-fluid grayColor">
                        <div className="topbar animated fadeInLeftBig"></div>
                        <div className="row">
                            <div className="col-12">
                                <nav className="navbar navbar-expand-lg navbar-light withoutPadding">
                                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                                        <i className="fas fa-bars whiteColor"></i>
                                    </button>
                                    <a className="navbar-brand d-lg-none d-xl-none" href="/"><img src="/images/Home/miniLogoBlanco.png" className="img-fluid" /></a>
                                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                                        <a className="navbar-brand d-none d-lg-block" href="/"><img src="/images/Home/miniLogoBlanco.png" className="img-fluid" /></a>
                                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                                            <a className="nav-link menuStyle " id="home" href="/home" role="button">Home</a>
                                            <a className="nav-link menuStyle " id="vacancy" href="/vacancy" role="button">Vacancy</a>
                                            <a className="nav-link menuStyle " id="activity" href="/activity" role="button">Event</a>

                                        </ul>
                                        <div className="form-inline my-2 my-lg-0" >

                                            <ul className="navbar-nav mr-auto" >
                                            
                                                <li className="nav-item dropdown" id="notifi" onClick={this.changeStatus.bind(this)} >
                                                    <a className="nav-link dropdown-toggle" href="#"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        {this.state.notificationsState >= 1 ?
                                                            <i className="fas fa-bell fa-1x redText"></i>
                                                            :
                                                            <i className="fas fa-bell fa-1x whiteText"></i>

                                                        }
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown">
                                                    {this.state.notificationsState ==0?
                                                    <a className="dropdown-item" id="dropdown-item" ><span className="labelStyle blueText">Do not have notifications</span></a>
                                                    :
                                                    this.state.notifications.map(x => {
                                                        return (
                                                            
                                                            <span key={uuidv1()} >
                                                            {x.EVENT!=null?
                                                            <a className="dropdown-item" id="dropdown-item" href={x.url}><span className="labelStyle blueText"><strong>{x.ENTITY_MASTER.NAME}</strong> applied to <br /> <strong>{x.EVENT.TITULE}</strong></span>   </a>:                                                               
                                                                null}
                                                                {x.VACANCY_ANNOUNCEMENT!=null?
                                                                <a className="dropdown-item" id="dropdown-item" href={x.url}><span className="labelStyle blueText"><strong>{x.ENTITY_MASTER.NAME}</strong> applied to <br /> <strong>{x.VACANCY_ANNOUNCEMENT.TITLE}</strong></span>   </a>:
                                                                null}
                                                                {x.VACANCY_ANNOUNCEMENT==null&&x.EVENT==null?
                                                                <a className="dropdown-item" id="dropdown-item" href={x.url}><span className="labelStyle blueText"><strong>{x.ENTITY_MASTER.NAME}</strong> is following you</span>   </a>:
                                                               null}
                                                                <hr />
                                                            </span>

                                                        )
                                                    })
                                                    }
                                                       

                                                    </div>
                                                </li>
                                                <li className="nav-item dropdown">
                                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i className="fas fa-envelope fa-1x whiteText"></i>
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown">
                                                        <a className="dropdown-item"  href="/chat">Messages</a>
                                                        <a className="dropdown-item"  href="/mail">Mail</a>
                                                    </div>
                                            </li>
                                            <li className="nav-item dropdown">
                                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <p className="whiteText inLine">English</p>  
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown">
                                                        <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "en")} href="#">English</a>
                                                        <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "dr")} href="#">Dari</a>
                                                        <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "pt")} href="#">Pashto</a>
                                                    </div>
                                                </li>

                                                <li className="nav-item dropdown">
                                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i className="fas fa-user-circle fa-1x whiteText"></i>
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown2">
                                                        <a className="dropdown-item" href="/panel">Profile</a>
                                                        <a className="dropdown-item" onClick={this.onLogout.bind(this)} href="#">Log out</a>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div className="paddingRigth"></div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                )
            ///////////////// VOLUNTEERS//////////////////////////
            if (this.state.dataUser.ENTITY_MASTER.TYPE_ENTITY == 2)
                return (
                    <div className="container-fluid grayColor">
                        <div className="topbar animated fadeInLeftBig"></div>
                        <div className="row">
                            <div className="col-12">
                                <nav className="navbar navbar-expand-lg navbar-light">
                                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                                        <i className="fas fa-bars whiteColor"></i>
                                    </button>
                                    <a className="navbar-brand d-lg-none d-xl-none" href="/"><img src="/images/Home/miniLogoBlanco.png" className="img-fluid" /></a>
                                    <div className="collapse navbar-collapse "  >
                                        <a className="navbar-brand d-none d-lg-block" href="/"><img src="/images/Home/miniLogoBlanco.png" className="img-fluid" /></a>
                                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                                            <a className="nav-link menuStyle " href="/home" id="home" role="button">Home</a>

                                        </ul>
                                        <div className="form-inline my-2 my-lg-0">

                                            <ul className="navbar-nav mr-auto">
                                            {/* <li className="nav-item"><a href="" className="nav-link"><i className="fas fa-envelope fa-2x whiteText"></i></a></li>                                             */}
                                            <li className="nav-item dropdown">
                                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i className="fas fa-envelope fa-1x whiteText"></i>
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown">
                                                        <a className="dropdown-item"  href="/chat">Messages</a>
                                                    </div>
                                            </li>
                                                <li className="nav-item dropdown">
                                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <p className="whiteText inLine">English</p>
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown">
                                                        <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "en")} href="#">English</a>
                                                        <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "dr")} href="#">Dari</a>
                                                        <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "pt")} href="#">Pashto</a>
                                                    </div>
                                                </li>

                                                <li className="nav-item dropdown">
                                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i className="fas fa-user-circle fa-1x whiteText"></i>
                                                    </a>
                                                    <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown2">
                                                        <a className="dropdown-item" href="/panel">Profile</a>
                                                        <a className="dropdown-item" onClick={this.onLogout.bind(this)} href="#">Log out</a>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div className="paddingRigth"></div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                )
            }
        else {
            return (
                <div className="container-fluid grayColor">
                    <div className="topbar animated fadeInLeftBig"></div>
                    <div className="row">
                        <div className="col-12">
                            <nav className="navbar navbar-expand-lg navbar-light">
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                                    <i className="fas fa-bars whiteColor"></i>
                                </button>
                                <a className="navbar-brand d-lg-none d-xl-none" href="/"><img src="/images/Home/miniLogoBlanco.png" className="img-fluid" /></a>
                                <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                                    <a className="navbar-brand d-none d-lg-block" href="/"><img src="/images/Home/miniLogoBlanco.png" className="img-fluid" /></a>
                                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                                        <a className="nav-link menuStyle " href="/home" id="home" role="button">Home</a>
                                    </ul>
                                    <div className="form-inline my-2 my-lg-0">
                                        <ul className="navbar-nav mr-auto">
                                        {/* <li className="nav-item"><a href="" className="nav-link"><i className="fas fa-envelope fa-2x whiteText"></i></a></li>                                         */}
                                            <li className="nav-item dropdown">
                                                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <p className="whiteText inLine">English</p>
                                                </a>
                                                <div className="dropdown-menu whiteText" aria-labelledby="navbarDropdown">
                                                    <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "en")} href="#">English</a>
                                                    <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "dr")} href="#">Dari</a>
                                                    <a className="dropdown-item" onClick={this.changeLanguage.bind(this, "pt")} href="#">Pashto</a>
                                                </div>
                                            </li>

                                        </ul>
                                        <a href="/loginWho" role="button" id="login" className="nav-link menuStyle grayColor">Log in</a>
                                        <a href="/register" role="button" id="register" className="nav-link menuStyle grayColor">Sign up</a>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default Menu