import React from 'react'
import cookie from 'react-cookies'
import uuidv1 from 'uuid/v1'



class Chat extends React.Component {
	constructor() {
		super()
		this.state = {
			list: [],
			messages: [],
			allMessages: [],
			contactSelected: false,
			nameInvited: ""
		}
		this.sendMessage = this.sendMessage.bind(this)
		this.evaluateMessage = []
		this.onlyOne = true
		this.refreshIntervalId
		this.userInfo = cookie.load('dataUser')

		if (this.userInfo.ENTITY_MASTER.TYPE_ENTITY == 1)
			this.urlImage = "orgImages"
		else
			this.urlImage = "volunteerImages"

	}
	componentDidMount() {
		this.getAllChats()
	}
	GetMessengerEntity(arr) {
		let array = []
		arr.map(o => {
			if (o[0] != null && o[1] != null) {
				if (o[0].DATE < o[1].DATE)
					this.state.allMessages.push(o[1])
				else
					this.state.allMessages.push(o[0])
			} else if (o[0] != null && o[1] == null)
				this.state.allMessages.push(o[0])
			else if (o[0] == null && o[1] != null)
				this.state.allMessages.push(o[1])
		})

		let id = null
		this.state.allMessages.map((x, z) => {
			if (x.CURR_NO.split("_")[1] == this.userInfo.ENTITY_ID)
				id = x.CURR_NO.split("_")[0]
			else if (x.CURR_NO.split("_")[0] == this.userInfo.ENTITY_ID)
				id = x.CURR_NO.split("_")[1]

			fetch(window.location.origin + '/Get/GetMessengerEntity/' + id, { method: 'post' })
				.then((response) => {
					return response.json()
				})
				.then((y) => {

					this.state.allMessages[z] = Object.assign({}, this.state.allMessages[z], y.data[0]);     //,y.data[0]]
					this.setState({
						allMessages: this.state.allMessages
					})
				})
		})
	}
	getLastMessage(key1, key2, num, len) {

		fetch(window.location.origin + '/Get/GetMessengerAll/' + key1, { method: 'post' })
			.then((response) => {
				return response.json()
			})
			.then((z) => {


				fetch(window.location.origin + '/Get/GetMessengerAll/' + key2, { method: 'post' })
					.then((response) => {
						return response.json()
					})
					.then((zz) => {
						this.evaluateMessage.push([z.data, zz.data])

						if (len == num + 1)
							this.GetMessengerEntity(this.evaluateMessage)

					})


			})
	}
	getAllChats() {

		fetch(window.location.origin + '/Get/GetMessenger/' + this.userInfo.ENTITY_ID, { method: 'post' })
			.then((response) => {
				return response.json()
			})
			.then((x) => {
				let arr = []
				x.data.map((x) => {
					if (x.CURR_NO.split("_")[1] == this.userInfo.ENTITY_ID)
						arr.push({ id: x.CURR_NO.split("_")[0], info: x.VIEW_ENTITY_MASTER })
					else if (x.CURR_NO.split("_")[0] == this.userInfo.ENTITY_ID)
						arr.push({ id: x.CURR_NO.split("_")[1], info: x.VIEW_ENTITY_MASTER })
				})

				var t = [...new Set(arr.map(item => {
					return item.id
				}))]
				t.map((x, y) => {
					this.getLastMessage(`${this.userInfo.ENTITY_ID}_${x}`, `${x}_${this.userInfo.ENTITY_ID}`, y, t.length)
				})
			})
	}
	search() {
		if (document.getElementById('to').value != '')
			fetch(window.location.origin + '/Get/GetEmail/' + document.getElementById('to').value, { method: 'post' })
				.then((response) => {
					return response.json()
				})
				.then((x) => {
					this.setState({
						list: x.data
					})
				})
		else this.setState({
			list: []
		})
	}
	updateStatusChat(id) {

		fetch(window.location.origin + '/update/updateChat/' + id, { method: 'post' })
			.then((response) => {
				return response.json()
			})
			.then((x) => {
			})
	}
	select(name, id, img, TYPE_ENTITY, cla) {
		this.setState({ contactSelected: true })
		this.updateStatusChat(`${id}_${this.userInfo.ENTITY_ID}`)
		clearInterval(this.refreshIntervalId)
		this.onlyOne = true
		if (TYPE_ENTITY == 1)
			this.urlImageInvited = "orgImages/" + img
		else
			this.urlImageInvited = "volunteerImages/" + img
		this.nameInvited = name
		this.idInvited = id
		this.setState({
			nameInvited: name,
			list: []
		})

		this.refreshIntervalId = window.setInterval(function () {
			fetch(window.location.origin + `/get/GetMessengerCurrno/${this.userInfo.ENTITY_ID}_${id}/${id}_${this.userInfo.ENTITY_ID}`, { method: 'post' })
				.then((response) => {
					return response.json()
				})
				.then((x) => {
					if (this.onlyOne) {
						this.setState({
							messages: []
						})
						this.setState({
							messages: x.data
						})
						$('.Divmessages').animate({ scrollTop: $('.Divmessages').prop("scrollHeight") }, 500)
						this.onlyOne = false

					}
					if (x.data.length != this.state.messages.length) {
						this.setState({
							messages: x.data
						})
						$('.Divmessages').animate({ scrollTop: $('.Divmessages').prop("scrollHeight") }, 500)
					}

				})
		}.bind(this), 1000);

		this.setState({ list: [] })
		document.getElementById('to').value = ""
		document.getElementById(cla + '-contact-status').className = "contact-status"
	}
	handleKeyPress(event) {
		if (event.key == 'Enter') {
			this.sendMessage()
		}
	}
	sendMessage(event) {
		event.preventDefault();

		let data = {
			MessengerId: uuidv1(),
			RemmitId: this.userInfo.ENTITY_ID,
			ReceptorId: this.idInvited,
			Message: document.getElementById("mesageText").value,
			CurrNo: `${this.userInfo.ENTITY_ID}_${this.idInvited}`,
			Date: new Date()

		}
		if (data.ReceptorId != undefined)
			fetch(window.location.origin + '/insert/InsertMessenger/',
				{
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify(data)
				})
				.then((response) => {
					return response.json()
						.then((x) => {

							if (response.status == 200 || response.status == 202) {
								document.getElementById("mesageText").value = ""
							}
							else {
								alertify.error('ناکام، دوباره کوشش نماید')
							}
						})
				})
		else
			alertify.error('یک شخص را انتخاب نماید')

	}
	render() {

		return (
			<section>
				<div className="container ">
					<div className="row messages grayBackground radius">
						<div className="col-3 withoutPadding">
							<div className="row">
								<div className="col-12 colorGray2 padding-div topSpace-1x boxShadown">
									<div class="image-cropper text-center">
										<img src={`/images/${this.urlImage}/${this.userInfo.ENTITY_MASTER.IMAGE}`} alt="avatar" class="profile-pic " />
									</div>
									<p>{this.userInfo.USER_NAME}</p>
								</div>
								<div className="col-12 colorGray3 topSpace-1x ">
									<div className="form-group placeholderOpacity">
										<input type="text" id="to" onChange={this.search.bind(this)} class="form-control no-border" defaultValue="" placeholder="جستجو ارتباطات" />
									</div>
									<div className="list-group radius">
										{this.state.list.map(x => {
											return (
												<a key={uuidv1()} onClick={this.select.bind(this, x.NAME, x.ENTITY_ID, x.IMAGE, x.TYPE_ENTITY, null)} className="list-group-item list-group-item-action">{x.NAME} <i className="fas fa-comments badge badge-primary badge-pill"></i></a>
											)
										})}
									</div>
								</div>
								<div className="col-12">
								
								
								
								
								
								
								
								<div id="contacts">
								<ul id="ulChat">
								<br/>
									{this.state.allMessages.map(x => {
										return (
											<span key={uuidv1()}>
												{x.RECEPTOR_ID == this.userInfo.ENTITY_ID ?
													<li id={x.CURR_NO} onClick={this.select.bind(this, x.NAME, x.ENTITY_ID, x.IMAGE, x.TYPE_ENTITY, x.CURR_NO)} >
														<div className="wrap paddingLeft-x1">
															{x.REMMIT_STATUS == true ? <span id={`${x.CURR_NO}-contact-status`} className="contact-status online"></span> : null}
															{x.TYPE_ENTITY == 1 ?
																<div class="image-cropper-x1 text-center">
																	<img src={`/images/orgImages/${x.IMAGE}`} alt="avatar" class="profile-pic " />
																</div> :
																<div class="image-cropper-x1 text-center">
																	<img src={`/images/volunteerImages/${x.IMAGE}`} alt="avatar" class="profile-pic " />
																</div>
															}
															<div className="meta contact">
																{x.NAME != "" ? <p className="name withoutMargin">{x.NAME}</p> : <p className="name withoutMargin">No name</p>}
																<p className="preview">{x.MESSAGE.substring(0, 14)}</p>
															</div>
														</div>
													</li> 
													 :
													<li id={x.CURR_NO} onClick={this.select.bind(this, x.NAME, x.ENTITY_ID, x.IMAGE, x.TYPE_ENTITY, x.CURR_NO)} >
														<div className="wrap paddingLeft-x1">

															{x.TYPE_ENTITY == 1 ?
																<div class="image-cropper-x1 text-center">
																{x.IMAGE==null?
																	<img src={`/images/orgImages/defaultOrg.jpg`} alt="avatar" class="profile-pic " />:
																	<img src={`/images/orgImages/${x.IMAGE}`} alt="avatar" class="profile-pic" onError={(e)=>{e.target.src="/images/orgImages/defaultOrg.jpg"}}/>
																}
																</div> :
																<div class="image-cropper-x1 text-center">
																{x.IMAGE==null?
																	<img src={`/images/volunteerImages/defaultVolunteer.jpg`} alt="avatar" class="profile-pic " />:
																	<img src={`/images/volunteerImages/${x.IMAGE}`} alt="avatar" class="profile-pic" onError={(e)=>{e.target.src="/images/volunteerImages/defaultVolunteer.jpg"}} />
																}
																</div>
															}
															<div className="meta contact">
																{x.NAME != "" ? <p className="name withoutMargin">{x.NAME}</p> : <p className="name withoutMargin">No name</p>}
																<p className="preview">{x.MESSAGE.substring(0, 14)}<span>:شما</span> </p>

															</div>
														</div>
													</li>
												}
												<hr className="withoutMargin bottomLine"/>
											</span>
										)
									})}
								</ul>
							</div>


						

								
								
								
								
								
								</div>
							</div>
						</div>
						<div style={{ display: this.state.contactSelected ? 'none' : 'block' }} className="col-9 text-center">
							<div className="topSpace">
								<img src="/images/logoChat.png" className="topSpace mediumImage img-fluid" />
								<hr />
								<h5>Select someone to start a conversation</h5>
							</div>
						</div>
						<div className="col-9 Divmessages " style={{ display: this.state.contactSelected ? 'block' : 'none' }}>
						<div className="row text-center">
						<div className="col-12">
						<h6>Chat with {this.state.nameInvited}</h6>
						</div>
						</div>
						
					


{this.state.messages.map(x => {
	return (
		<div className="row" key={uuidv1()}>
			{(this.userInfo.ENTITY_ID == x.CURR_NO.split("_")[1]) ?


				<div className="col-6 offset-6">

					<div className="row topSpace-1x">
						<div className="col-11 speech-bubble-Gray">
							<p className="preview withoutMargin bubble-gray whiteText">{x.MESSAGE}</p>
						</div>
						<div className="col-1 maxWidReduced">


							<div class="image-cropper-x1 text-center">
								<img id="profile-img" src={`/images/${this.urlImageInvited}`} alt="avatar" class="profile-pic online" />
							</div>
						</div>

					</div>

				</div>

				:
				<div className="col-12">
					<div className="row topSpace-1x">
						<div className="col-1 maxWidReduced">


							<div class="image-cropper-x1 text-center">
								<img id="profile-img" src={`/images/${this.urlImage}/${this.userInfo.ENTITY_MASTER.IMAGE}`} alt="avatar" class="profile-pic online" />
							</div>
						</div>
						<div className="col-6 speech-bubble">
							<p className="preview withoutMargin blueBack whiteText">{x.MESSAGE}</p>
						</div>
					</div>


				</div>

			}
		</div>
	)
})}


						
						</div>
						<div className="col-9 offset-3 topSpace-1x" style={{ display: this.state.contactSelected ? 'block' : 'none' }}>
						<form onSubmit={this.sendMessage}>
								<div className="row">
									<div className="col-11 withoutPadding">
										<div class="form-group withoutMargin">

											<input type="text" class="form-control" id="mesageText" onKeyPress={this.handleKeyPress.bind(this)} placeholder="پیام ایتان را بنوسید" />
										</div>
									</div>
									<div className="col-1 .maxWidReduced  withoutPadding">
										<div class="form-group ">
											<button className="buttonSendMessage buttonSend btn btn-info"><i class="fab fa-telegram-plane"></i></button>
										</div>
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
			</section>
		)
	}

}

export default Chat