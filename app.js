var express = require('express');
var path = require('path');
var app = express();
var md5 = require('md5');
var Sequelize = require('sequelize');
var multer = require('multer');
var sequelize = require('./bin/ormConfig').sequelize;
const bodyParser = require('body-parser');
var Get = require('./routes/Get/AllDescription');
var GetLogin = require('./routes/Get/UserLogin');
var url = require('url');
var Insert = require('./routes/Insert/Insert');
var Update = require('./routes/Update/Update');
var Information = require('./routes/Get/Information');
var Messenger = require('./routes/Get/Messenger');
var ModEntityMaster = require('./models/EntityMaster').EntityMaster;
var ModEvent = require('./models/Events').Events;
var routes =require('./routes');
const Op = Sequelize.Op;

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '/public')));

// app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + '/public'));

app.use('/Get', Get);

app.use('/Get', Information);

app.use('/Get', Messenger);

app.use('/Get', GetLogin);

app.use('/Insert', Insert);

app.use('/Update', Update);

app.use('', routes);

function getFormattedUrl(req) {
  return url.format({
      protocol: req.protocol,
      host: req.get('host')
  });
}




app.post("/api/Upload", function (req, res) {
  var Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/volunteerImages");
    },
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    }
  });
  var upload = multer({ storage: Storage }).array("imgUploader", 3); //Field name and max count
  upload(req, res, function (err) {
      if (err) {
          console.log("***********************************************************",err)
      }else{
  res.redirect(`${getFormattedUrl(req)}/panel`);
        
        console.log("##########################################################")
      }
  });
});


//////////////////VACANCY IMAGE///////////////////////
app.post("/api/upload/vacancy", function (req, res) {
  console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%1%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5")
  
  var StorageVacancy = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/vacancyImages");
    },
    filename: function (req, file, callback) {
      callback(null, file.originalname);
    }
  });
  var uploadVacancy = multer({ storage: StorageVacancy }).array("imgUploader", 3); //Field name and max count
  uploadVacancy(req, res, function (err) {
  console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%2%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5")
    
      if (err) {
        //   return res.end("***********************************************************");
          console.log("***********************************************************",err)
      }else{
            res.redirect(`${getFormattedUrl(req)}/vacancy`);
        
        // return res.end("##########################################################");
        console.log("##########################################################")
      }
  });
//   res.sendFile(path.join(__dirname + '/index.html'));
});




//////////////////ORG IMAGE///////////////////////
app.post("/api/upload/org", function (req, res) {
  var StorageOng = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/orgImages");
    },
    filename: function (req, file, callback) {
      callback(null, file.originalname);
    }
  });
  var uploadOrg = multer({ storage: StorageOng }).array("imgUploader", 3); //Field name and max count

  uploadOrg(req, res, function (err) {
      if (err) {
        //   return res.end("***********************************************************");
          console.log("***********************************************************",err)
      }else{
  res.redirect(`${getFormattedUrl(req)}/panel`);
        
        // return res.end("##########################################################");
        console.log("***********************************************************")
      }
  });
//   res.sendFile(path.join(__dirname + '/index.html'));
});


//////////////////ACTIVITY IMAGE///////////////////////
app.post("/api/upload/activity", function (req, res) {
  var StorageActivity = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/activityImages");
    },
    filename: function (req, file, callback) {
      callback(null, file.originalname);
    }
  });
  var uploadVacancy = multer({ storage: StorageActivity }).array("imgUploader", 3); //Field name and max count
  uploadVacancy(req, res, function (err) {
      if (err) {
        //   return res.end("***********************************************************");
          console.log("***********************************************************",err)
      }else{
  res.redirect(`${getFormattedUrl(req)}/activity`);
        
        // return res.end("##########################################################");
        console.log("##########################################################")
      }
  });
//   res.sendFile(path.join(__dirname + '/index.html'));
});


//////////////////LEGAL DOCS///////////////////////
app.post("/api/upload/legalDocs", function (req, res) {
  var StoragelegalDocs = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "./public/images/legalDocs");
    },
    filename: function (req, file, callback) {
      callback(null, file.originalname);
    }
  });
  var uploadlegalDocs = multer({ storage: StoragelegalDocs }).array("imgUploader", 3); //Field name and max count
  uploadlegalDocs(req, res, function (err) {
      if (err) {
        //   return res.end("***********************************************************");
          console.log("***********************************************************",err)
      }else{
  res.redirect(`${getFormattedUrl(req)}/panel`);
        
        // return res.end("##########################################################");
        console.log("##########################################################")
      }
  });
//   res.sendFile(path.join(__dirname + '/index.html'));
});


module.exports = app;