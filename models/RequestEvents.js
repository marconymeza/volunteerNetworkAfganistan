const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const REQUEST_EVENTS = sequelize.define('REQUEST_EVENTS', 
    {
      // definicion del los campos
      EVENTS_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      RELATIONAL_ENTITY: 
      {
        type: Sequelize.STRING
      },
      REQUEST_STATUS: 
      {
        type: Sequelize.NUMERIC
      },
      REQUEST_DATE: 
      {
        type: Sequelize.DATE
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'REQUEST_EVENTS',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.RequestEvents = REQUEST_EVENTS


