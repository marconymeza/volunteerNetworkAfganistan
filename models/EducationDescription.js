const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const EDUCATION_DESCRIPTION = sequelize.define('EDUCATION_DESCRIPTION', 
    {
      // definicion del los campos
      EDUCATION_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      EDUCATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'EDUCATION_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.EducationDescription = EDUCATION_DESCRIPTION


