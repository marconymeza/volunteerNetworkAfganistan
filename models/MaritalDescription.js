const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const MARITAL_DESCRIPTION = sequelize.define('MARITAL_DESCRIPTION', 
    {
      // definicion del los campos
      MARITAL_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      MARITAL_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'MARITAL_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.MaritalDescription = MARITAL_DESCRIPTION


