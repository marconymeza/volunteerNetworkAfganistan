var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var ModEntity = require('../../models/EntityMaster').EntityMaster;
var ModLogin = require('../../models/Login').Login;
var url = require('url');
var ModVacancyJob = require('../../models/VacancyJob').VacancyJob;
var ModVacancyArea = require('../../models/VacancyExpertise').VacancyExpertise;
var ModVacancy = require('../../models/VacancyAnnouncement').VacancyAnnouncement;
var ModEvent = require('../../models/Events').Events;
var ModVacancyScope = require('../../models/VacancyScope').VacancyScope;
var ModEventScope = require('../../models/EventScope').EventScope;
var ModEventVolunteer = require('../../models/EventsVolunteer').EventsVolunteer;
var ModRequestEntity = require('../../models/RequestEntity').RequestEntity;
var ModRequestEvent = require('../../models/RequestEvents').RequestEvents;
var ModRequestVacancy = require('../../models/VacancyRequest').VacancyRequest;
var ModMessenger = require('../../models/Messenger').Messenger
var  NotificationCenter= require('../../models/NotificationCenter').NotificationCenter
var md5 = require('md5');
const Op = Sequelize.Op;
var app = express();
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtp://volunteernetworkafghanistan@gmail.com:@fghAnistan@smtp.gmail.com');



function getFormattedUrl(req) {
  return url.format({
    protocol: req.protocol,
    host: req.get('host')
  });
}
router.post('/InsertVacancy/', function (req, res) {

  var queue = Promise.resolve();
  queue = queue.then(function (res) {

    if (req.body.IdVacancy != "") {
      var Vacancyx = req.body.IdVacancy
    } else
      var Vacancyx = req.body.Vacancy

    var Result = ModVacancy.upsert({
      ENTITY_ID: req.body.Entity,
      VACANCY_ID: Vacancyx,
      VACANCY_TYPE: req.body.TypeVacancy,
      TITLE: req.body.Title,
      DESCRIPTION: req.body.Description,
      START_DATE: req.body.StartDate,
      END_DATE: req.body.EndDate,
      STATUS: req.body.Status,
      DURATION: req.body.Duration,
      EDUCATION_REQUIRED: req.body.EducationRequired,
      WORK_EXPERIENCE_REQUIRED: req.body.WorkExperience,
      TYPE_ORGANIZATION: req.body.TypeOrganization,
      NO_OF_JOBS: req.body.NoOfJobs,
      INCENTIVES: req.body.AddIncentForVolunteer,
      IMAGE: req.body.nameImg

    }).then(anotherTask => {
      if (req.body.Areasofexpertise != "") {
        req.body.Areasofexpertise.map(x => {
          ModVacancyArea.upsert({
            VACANCY_ID: Vacancyx,
            AREA_ID: x.id,
            AREA_DESCRIPTION: x.description
          }).then(anotherTask => {

          })
        })

      }
      if (req.body.scope != "") {
        req.body.scope.map(x => {
          ModVacancyScope.upsert({
            VACANCY_ID: Vacancyx,
            SCOPE: x
          }).then(anotherTask => {

          })
        })
      }
      if (req.body.JobDescriptor != "") {
        req.body.JobDescriptor.map(x => {
          ModVacancyJob.upsert({
            VACANCY_ID: Vacancyx,
            JOB_ID: x.id,
            JOB_DESCRIPTION: x.description
          }).then(anotherTask => {

          })
        })
      }
      return 200
    }).catch(error => {
      return error
    })
    return Result
  });

  queue.then(function (lastResponse) {
    console.log(lastResponse)
    res.status(lastResponse).send({ data: md5(req.body.Title) })

  })
})
router.post('/InsertEvent/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    if (req.body.IdEvent != "") {
      var eventx = req.body.IdEvent
    } else
      var eventx = req.body.Events
    console.log(eventx)
    var Result = ModEvent.upsert({
      ENTITY_ID: req.body.Entity,
      EVENTS_ID: eventx,
      LATITUD: req.body.Latitud,
      LONGITUDE: req.body.Longitude,
      TITULE: req.body.Titule,
      DESCRIPTION: req.body.Description,
      START_DATE: req.body.StartDate,
      END_DATE: req.body.EndDate,
      STATUS: req.body.Status,
      SUB_TITULE: req.body.SubTitule,
      RESPONSIBLE_PERSON_NAME: req.body.ReName,
      RESPONSIBLE_TELEPHONE: req.body.TeNumber,
      RESPONSIBLE_EMAIL: req.body.Email,
      DETAILS: req.body.detailsEvent,
      OBJECTIVES: req.body.EventObjetives,
      IMAGE: req.body.img

    }).then(anotherTask => {
      if (req.body.scope != "") {
        req.body.scope.map(x => {
          ModEventScope.upsert({
            EVENTS_ID: eventx,
            SCOPE: x
          }).then(anotherTask => {

          })
        })
      }
      if (req.body.Volunteers != "") {
        req.body.Volunteers.map(x => {
          ModEventVolunteer.upsert({
            EVENTS_ID: eventx,
            VOLUNTEER_ID: x.id,
            VOLUNTEER_DESCRIPTION: x.description

          }).then(anotherTask => {

          })
        })
      }
      return 200
    }).catch(error => {
      return error
    })
    return Result
  });

  queue.then(function (lastResponse) {
    console.log(lastResponse)
    res.status(lastResponse).send({ data: true })

  })
})
router.post('/SendEmail/', function (req, res) {

  req.body.To.map(x => {
    var mailOptions = {
      from: '"Volunteer Network Afghanistan 👥" <volunteernetworkafghanistan@gmail.com>',
      to: x.email,
      subject: req.body.Subjetc,
      html: req.body.Message + `Written by <a href="mailto:${req.body.Contact}">${req.body.USER_NAME} click here to respond</a>.<br> `
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return 400
        console.log(error)
      }
      else {
        return 200
      }
    });
  })
  res.status(200).send({ data: true })
})
router.post('/InsertOrganization/', function (req, res) {
  var queue = Promise.resolve();
  var mailOptions = {
    from: '"Volunteer Network Afghanistan 👥" <volunteernetworkafghanistan@gmail.com>',
    to: req.body.Email,
    subject: 'Confirmation Email',
    html: `Welcome to Afghanistan National Network of Volunteers, we have received a request to create an account in its database with your email, to complete the creation of your account, you must verify that you own this email address by clicking on Email Verification.

    NOTE:  THIS MESSAGE IS SYSTEM GENERATED.  PLEASE DO NOT REPLY TO THIS MESSAGE.
        <a href="${getFormattedUrl(req)}/confirmation/">Please Open link</a>`
  };
  ModLogin.count({
    where: {
      [Op.or]: [
        { EMAIL: req.body.Email },
        { USER_NAME: req.body.User }
      ]
    }
  }).then(count => {
    if (count == 0) {
      queue = queue.then(function (res) {
        var Result = ModEntity.upsert({
          ENTITY_ID: md5(req.body.Email),
          NAME: req.body.name,
          EMAIL: req.body.Email,
          // GENDER: req.body.Gender,
          // LEGAL_ID: req.body.LegalId,
          // TELEPHONE: req.body.Telephone,
          // NAME_RESPONSIBLE: req.body.Nresponsible,
          // TELEPHONE_RESPONSIBLE: req.body.NTelephone,
          TYPE_ENTITY: req.body.TypeEntity,
          // NACIONALITY: req.body.Country,
          STATUS: 1
          // TYPE_ORGANIZATION: req.body.TypeOrganization,
          // COMMENTARY: req.body.Commentary
        }).then(y => {

          //   var jdenticon = require("jdenticon"),
          //   fs = require("fs"),
          //   size = 200,
          //   value = y.ENTITY_ID,
          //   png = jdenticon.toPng(value, size);

          //   // console.log(__dirname)
          //  fs.writeFileSync(__dirname+'../../../public/images/perfilimages/'+y.ENTITY_ID+".png", png);;

          ModLogin.build({
            ENTITY_ID: md5(req.body.Email),
            USER_NAME: req.body.User,
            EMAIL: req.body.Email,
            PASSWORD: req.body.PassWord,
            STATUS: 0,
            KEY: req.body.Key
          }).save().then(anotherTask => {
          }).catch(error => {
          })
          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              return error
            }
            else {
              return 200
            }
          });
          return 202
        }).catch(error => {
          return error
        })
        return Result
      });
    }
    else {
      queue = queue.then(function () {
        return 406
      })
    }

    queue.then(function (lastResponse) {
      console.log(lastResponse)
      res.status(lastResponse).send({ data: true })

    })


  });


})
router.post('/FollowEntity/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModRequestEntity.upsert({
      ENTITY_ID: req.body.Entity,
      RELATIONAL_ENTITY: req.body.Rentity,
      REQUEST_STATUS: 1
    },{
      where:{ENTITY_ID: req.body.Entity, RELATIONAL_ENTITY: req.body.Rentity}
    }).then(x => {
      return 200

    }).catch(error => {
      return 400
    })
    return Result
  })
  queue.then(function (lastResponse) {

    res.status(lastResponse).send({ data: true })
  })
})
router.post('/FollowEvent/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModRequestEvent.upsert({
      EVENTS_ID: req.body.Event,
      RELATIONAL_ENTITY: req.body.Rentity,
      REQUEST_STATUS: 1
    },{
      where:{EVENTS_ID: req.body.Event, RELATIONAL_ENTITY: req.body.Rentity}
    }).then(x => {
      return 200

    }).catch(error => {
      return 404
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: true })
  })
})
//////////////////////////////////////////////////////////////
router.post('/NotificationFollow/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = NotificationCenter.upsert({
      ID_NOTIFICATION:req.body.ID,
      ENTITY_ID_SECONDARY: req.body.ENTITY_ORG,
      ENTITY_ID_PRIMARY: req.body.Rentity,
      ENTITY_ORG:req.body.ENTITY_ORG,
      STATUS: 1,
      DATE:new Date(),
      DESCRIPTION:"Follow"
    }).then(x => {
      return 200

    }).catch(error => {
      return 404
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: lastResponse })
  })
})

router.post('/NotificationEvent/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = NotificationCenter.upsert({
      ID_NOTIFICATION:req.body.ID,
      ENTITY_ID_SECONDARY: req.body.Event,
      ENTITY_ID_PRIMARY: req.body.Rentity,
      ENTITY_ORG:req.body.ENTITY_ORG,
      STATUS: 1,
      DATE:new Date(),
      DESCRIPTION:"Event"
    }).then(x => {
      return 200

    }).catch(error => {
      return 404
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: lastResponse })
  })
})
router.post('/NotificationVacancy/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = NotificationCenter.upsert({
      ID_NOTIFICATION:req.body.ID,
      ENTITY_ID_SECONDARY: req.body.Vacancy,
      ENTITY_ID_PRIMARY: req.body.Rentity,
      ENTITY_ORG:req.body.ENTITY_ORG,
      STATUS: 1,
      DATE:new Date(),
      DESCRIPTION:"Vacancy"
    }).then(x => {
      return 200

    }).catch(error => {
      return 404
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: lastResponse })
  })
})

//////////////////////////////////////////////////////////////////////
router.post('/FollowVacancy/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModRequestVacancy.upsert({
      VACANCY_ID: req.body.Vacancy,
      RELATIONAL_ENTITY: req.body.Rentity,
      REQUEST_STATUS: 1
    },{
      where:{VACANCY_ID: req.body.Vacancy, RELATIONAL_ENTITY: req.body.Rentity}
    }).then(x => {
      return 200

    }).catch(error => {
      return 404
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: true })
  })
})
router.post('/InsertMessenger/', function (req, res) {
  var queue = Promise.resolve();
  queue = queue.then(function (res) {
    var Result = ModMessenger.build({
      MESSENGER_ID: req.body.MessengerId,
      REMMIT_ID: req.body.RemmitId,
      RECEPTOR_ID: req.body.ReceptorId,
      REMMIT_STATUS: true,
      MESSAGE: req.body.Message,
      STATUS: 1,
      CURR_NO: req.body.CurrNo,
      DATE: req.body.Date,
      RECEPTOR_STATUS: true

    }).save().then(x => {
      return 200

    }).catch(error => {
      return 404
      console.log(error)
    })
    return Result

  })
  queue.then(function (lastResponse) {
    res.status(lastResponse).send({ data: true })
  })
})

module.exports = router;

// router.post('/InsertDescription/', function (req, res) {
//   sequelize.query("SELECT MAX(CAST([ABILITIES_CODE] AS NUMERIC)) AS [ABILITIES_CODE] FROM [ABILITIES_DESCRIPTION]",
//     { type: sequelize.QueryTypes.SELECT }).then(x => {
//       var MaxCode = (x[0].ABILITIES_CODE) + 1
//       ModAbilities.build({
//         ABILITIES_CODE: MaxCode, ABILITIES_DESCRIPTION: req.body.Desc

//       })
//         .save().then(anotherTask => {
//           res.status(200).send({ data: anotherTask })
//         }).catch(error => {
//           res.status(500).send({ data: anotherTask })
//         })


//     })


// })