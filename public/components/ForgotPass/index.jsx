
import React from 'react'
import uuidv1 from 'uuid/v1'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import md5 from 'md5'


class ForgotPass extends React.Component {

    constructor() {
        super()
        this.state = {

        }



    }
    componentDidMount() {

    }


Forgot(){
var Email= document.getElementById('Email').value

    fetch(window.location.origin + '/Get/ForgotPassWord/' +Email, { method: 'post' })
    .then((response) => {
        return response.json()
            .then((x) => {
                if (response.status==200 || response.status==202 ) {
                        alertify.success('Password sent to the mail')
                        setTimeout(() => {
                            window.open('/login', "_self")
                          }, 2000);
                }else if(response.status==406){
                    alertify.error('The mail was not sent')
                }
                else {
                    alertify.error('Mail does not exist in the Database')
                }
            })
    })


}



    render() {
        return (
            <section id="Confirmation" className="container-fluid">
                <div className="container">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <h1>Enter the email</h1>
                                <div className="md-form">
                                    <input type="text" id="Email" className="form-control" />
                                    <label htmlFor="Email">Email</label>
                                </div>

                            </div>

                            <div className="modal-footer">
                                <div className="text-center mt-4 mb-2">
                                    <div className="text-center mt-4 mb-2">
                                        <button onClick={this.Forgot.bind(this)} className="btn btn-info">Send<i className="fa fa-send ml-2"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

        )
    }
}

export default ForgotPass
