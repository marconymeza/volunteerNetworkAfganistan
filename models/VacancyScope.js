const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VACANCY_SCOPE = sequelize.define('VACANCY_SCOPE', 
    {
      // definicion del los campos
      VACANCY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      SCOPE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VACANCY_SCOPE',
      timestamps: false,
      freezeTableName: true,
    });
    

module.exports.VacancyScope = VACANCY_SCOPE


