const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const PROVINCE_DESCRIPTION = sequelize.define('PROVINCE_DESCRIPTION', 
    {
      // definicion del los campos
      PROVINCE_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      PROVINCE_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'PROVINCE_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.ProvinceDescription = PROVINCE_DESCRIPTION


