import React from 'react'
import { get } from 'https';
import ReactTooltip from 'react-tooltip'
import random from 'uuid/v1'
class PerfilVolunteer extends React.Component {


    constructor() {
        super()
        this.state = {
            Sdg: [],
            Occupation: [],
            Profession: [],
            Abilities:[],
            data:[]
        }
    }

    componentWillMount() {
        this.getPerfil()
    }

    getPerfil() {
        fetch(`${window.location.origin}/Get/GetCompleteInformation/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    data:x.data[0]
                })
                
                if(x.data[0].IMAGE != undefined && x.data[0].IMAGE != null){
                    $("#blah").attr('src', '/images/volunteerImages/'+ x.data[0].IMAGE);                        
                    document.getElementById("blah").className = "img-fluid imgblah"
                }



                var t = [...new Set(x.data.map(item => {
                    if (item.ENTITY_SDG != null)
                        return item.ENTITY_SDG.SDG
                }))]
                var tt = [...new Set(x.data.map(item => {
                    if (item.ENTITY_SDG != null)
                        return item.ENTITY_SDG.SDG_DESCRIPTION
                }))]
                if (t[0] != undefined)
                    t.map((y, o) => {
                        if (y != null)

                            this.state.Sdg.push({ id: y, description: tt[o] })
                    })

                var t2 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_OCCUPATION != null)
                        return item.ENTITY_OCCUPATION.OCCUPATION_ID
                }))]
                var tt2 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_OCCUPATION != null)
                        return item.ENTITY_OCCUPATION.OCCUPATION_DESCRIPTION
                }))]
                if (t2[0] != undefined)
                    t2.map((y, o) => {
                        if (y != null)

                            this.state.Occupation.push({ id: y, description: tt2[o] })
                    })

                var t3 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_PROFESSION != null)
                        return item.ENTITY_PROFESSION.PROFESSION_ID
                }))]
                var tt3 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_PROFESSION != null)
                        return item.ENTITY_PROFESSION.PROFESSION_DESCRIPTION
                }))]
                if (t3[0] != undefined)
                    t3.map((y, o) => {
                        if (y != null)

                            this.state.Profession.push({ id: y, description: tt3[o] })
                    })

                var t4 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_ABILITy != null)
                        return item.ENTITY_ABILITy.ABILITIES
                }))]
                var tt4 = [...new Set(x.data.map(item => {
                    if (item.ENTITY_ABILITy != null)
                        return item.ENTITY_ABILITy.ABILITIES_DESCRIPTION
                }))]
                if (t4[0] != undefined)
                    t4.map((y, o) => {
                        if (y != null)

                            this.state.Abilities.push({ id: y, description: tt4[o] })
                    })
                    this.setState({
                    Sdg: this.state.Sdg,
                    Occupation: this.state.Occupation,
                    Profession: this.state.Profession,
                    Abilities: this.state.Abilities
                })



            })
    }

    render() {
        return (
            <section id="profileVolunteer">
                <div className="container-fluid">
                <ReactTooltip />
                    <div className="row topSpace-1x">
                        <div className="col-12 text-left" data-tip="Go back"><a href="/home"><i className="fas fa-arrow-circle-left fa-2x redText"></i></a></div>
                    </div>
                    <div className="row">
                        <div className="col-12 text-center withoutPadding">
                            <img src="/images/Home/picture.png" className="img-fluid topSpace" onError={(e)=>{e.target.src="/images/volunteerImages/defaultVolunteer.jpg"}} id="blah" />                         
                            <h3>{this.state.data.NAME}</h3>

                            {this.state.data.LEGAL_ID != null && this.state.data.LEGAL_ID != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>National ID (Tazkira) NO:</strong> {this.state.data.LEGAL_ID}</p>:null}

                            {this.state.data.PROVINCE != null && this.state.data.PROVINCE != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Province:</strong> {this.state.data.PROVINCE}</p>:null}

                            {this.state.data.DISTRICT != null && this.state.data.DISTRICT != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>District:</strong> {this.state.data.DISTRICT}</p>:null}

                            {this.state.data.VILLAGE != null && this.state.data.VILLAGE != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Village:</strong> {this.state.data.VILLAGE}</p>:null}

                            {this.state.data.BIRTH_DATE != null && this.state.data.BIRTH_DATE != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Birthdate:</strong>{`${this.state.data.BIRTH_DATE.substring(0,10)} (${new Date().getFullYear()- this.state.data.BIRTH_DATE.substring(0,4)}) years old`}</p>:null}

                            {this.state.data.GENDER_DESCRIPTION != null && this.state.data.GENDER_DESCRIPTION != ""?
                            <p className="card-text withoutPadding withoutMargin"><strong>Gender:</strong> {this.state.data.GENDER_DESCRIPTION.GENDER_DESCRIPTION}</p>:null}

                            {this.state.data.BLOOD_GROUP_DESCRIPTION != null && this.state.data.BLOOD_GROUP_DESCRIPTION != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Blood Group:</strong> {this.state.data.BLOOD_GROUP_DESCRIPTION.BLOOD_GROUP_DESCRIPTION}</p>:null}

                            {this.state.data.EDUCATION_DESCRIPTION != null && this.state.data.EDUCATION_DESCRIPTION != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Education:</strong> {this.state.data.EDUCATION_DESCRIPTION.EDUCATION_DESCRIPTION}</p>:null}

                            {this.state.data.PERIOD_DESCRIPTION != null && this.state.data.PERIOD_DESCRIPTION != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Availability of voluntary:</strong> {this.state.data.PERIOD_DESCRIPTION.PERIOD_DESCRIPTION}</p>:null}

                            {this.state.data.EMAIL != null && this.state.data.EMAIL != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Email:</strong> {this.state.data.EMAIL}</p>:null}

                            {this.state.data.TELEPHONE != null && this.state.data.TELEPHONE != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Telephone:</strong> {this.state.data.TELEPHONE}</p>:null}

                            {this.state.data.FACEBOOK != null && this.state.data.FACEBOOK != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Facebook:</strong> {this.state.data.FACEBOOK}</p>:null}

                            {this.state.data.INSTAGRAM != null && this.state.data.INSTAGRAM != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Instagram:</strong> {this.state.data.INSTAGRAM}</p>:null}

                            {this.state.data.TWITTER != null && this.state.data.TWITTER != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Twitter:</strong> {this.state.data.TWITTER}</p>:null}

                            {this.state.data.GOOGLE != null && this.state.data.GOOGLE != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Google+:</strong> {this.state.data.GOOGLE}</p>:null}

                            {this.state.data.NAME_RESPONSIBLE != null && this.state.data.NAME_RESPONSIBLE != "" ?
                            <p className="card-text withoutPadding withoutMargin"><strong>Name of Reference:</strong> {this.state.data.NAME_RESPONSIBLE}</p>:null}

                            {this.state.data.TELEPHONE_RESPONSIBLE != null && this.state.data.TELEPHONE_RESPONSIBLE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Telephone of Contact Reference:</strong> {this.state.data.TELEPHONE_RESPONSIBLE}</p> : null}
                            <hr />
                        </div>


                        <div className="col-12 text-center ">
                            <ul className="nav nav-pills mb-3 center-pills" id="pills-tab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="pills-Freeaddress-tab" data-toggle="pill" href="#pills-Freeaddress" role="tab" aria-controls="pills-Freeaddress" aria-selected="true">Free address</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-Description-tab" data-toggle="pill" href="#pills-Description" role="tab" aria-controls="pills-Description" aria-selected="false">Description</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-SDG-tab" data-toggle="pill" href="#pills-SDG" role="tab" aria-controls="pills-SDG" aria-selected="false">SDG's (Area of interest)</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-expertise-tab" data-toggle="pill" href="#pills-expertise" role="tab" aria-controls="pills-expertise" aria-selected="false">Areas of expertise</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-Professions-tab" data-toggle="pill" href="#pills-Professions" role="tab" aria-controls="pills-Professions" aria-selected="false">Professions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-Occupations-tab" data-toggle="pill" href="#pills-Occupations" role="tab" aria-controls="pills-Occupations" aria-selected="false">Occupations</a>
                                </li>
                            </ul>
                            <div className="tab-content " id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-Freeaddress" role="tabpanel" aria-labelledby="pills-Freeaddress-tab">
                                    {this.state.data.FREE_ADDRESS != null && this.state.data.FREE_ADDRESS != "" ?
                                        <div className="col-8 offset-2 text-center topSpace-2x">
                                            <div className="card-text withoutPadding withoutMargin" >{this.state.data.FREE_ADDRESS}</div>
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-Description" role="tabpanel" aria-labelledby="pills-Description-tab">
                                    {this.state.data.COMMENTARY != null && this.state.data.COMMENTARY != "" ?
                                        <div className="col-8 offset-2 text-center topSpace-2x">
                                            <div className="card-text withoutPadding withoutMargin" >{this.state.data.COMMENTARY}</div>
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-SDG" role="tabpanel" aria-labelledby="pills-SDG-tab">
                                    {this.state.Sdg.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            {this.state.Sdg.map(x => {
                                                return (
                                                    <span>
                                                        {x.id.length < 3 ?
                                                            <img key={random()} src={`/images/SDG/${x.id}.jpg`} className="miniImg marginRigth" />
                                                            :
                                                            null
                                                        }
                                                    </span>
                                                )
                                            })}
                                        </div> : null}
                                    {this.state.Sdg.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            <p>Other areas of interest</p>
                                            {this.state.Sdg.map(x => {
                                                return (
                                                    <span>
                                                        {x.id.length > 3 ?
                                                            <span key={random()} className="badge badge-info marginRigth"><h6>{x.description}</h6></span>
                                                            :
                                                            null
                                                        }
                                                    </span>
                                                )
                                            })}
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-expertise" role="tabpanel" aria-labelledby="pills-expertise-tab">
                                    {this.state.Abilities.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            {this.state.Abilities.map(x => {
                                                return (
                                                    <span>
                                                        {x.id.length < 3 ?
                                                            <img key={random()} src={`/images/Habilities/${x.id}.png`} className="miniImg marginRigth" />
                                                            :
                                                            null
                                                        }
                                                    </span>
                                                )
                                            })}
                                        </div> : null}


                                    {this.state.Abilities.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            <p>Other sreas of expertise</p>
                                            {this.state.Abilities.map(x => {
                                                return (
                                                    <span>
                                                        {x.id.length > 3 ?
                                                            <span key={random()} className="badge badge-info marginRigth"><h6>{x.description}</h6></span>
                                                            :
                                                            null
                                                        }
                                                    </span>
                                                )
                                            })}
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-Professions" role="tabpanel" aria-labelledby="pills-Professions-tab">
                                    {this.state.Profession.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            {this.state.Profession.map(x => {
                                                return (
                                                    <span key={random()} className="badge badge-info marginRigth"><h6>{x.description}</h6></span>
                                                )
                                            })}
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-Occupations" role="tabpanel" aria-labelledby="pills-Occupations-tab">
                                {this.state.Occupation.length != 0 ?
                                        <div className="col-12 text-center topSpace-2x">
                                            {this.state.Occupation.map(x => {
                                                return (
                                                    <span key={random()} className="badge badge-info marginRigth"><h6>{x.description}</h6></span>
                                                )
                                            })}
                                        </div> : null}
                                </div>
                            </div>
                            <hr/>
                        </div>
                        <div className="col-12 bottomSpace text-center">
                            <a href="/chat" data-tip="Send message"><i className="fas fa-comments fa-2x redText"></i></a>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default PerfilVolunteer