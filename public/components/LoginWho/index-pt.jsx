import React from 'react'
import md5 from 'md5'
import random from 'uuid/v1'

class Register extends React.Component {
   
    render() {

        return (
            <section className="Register">

                <div className="container text-center">
                    <div className="row  flotante">
                        <div className="col-12 topSpace bottomSpace blueText ">
                            <h1>تاسو څوک یاست؟</h1>
                        </div>
                        <div className="col-12 col-md-4 offset-md-2">
                            <a href="/login">
                                <i className="fas fa-building fa-10x blueText"></i>
                                <h3 className="blueText">سازمان/ اداره</h3>
                            </a>
                        </div>
                        <div className="col-12 topSpace d-md-none d-lg-none d-xl-none"></div>
                        <div className="col-12 col-md-4">
                            <a href="/login">
                                <i className="fas fa-user fa-10x blueText"></i>
                                <h3 className="blueText">رضاکار</h3>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Register