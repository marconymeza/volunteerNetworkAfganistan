const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const OCCUPATION_DESCRIPTION = sequelize.define('OCCUPATION_DESCRIPTION', 
    {
      // definicion del los campos
      OCCUPATION_CODE: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      OCCUPATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'OCCUPATION_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.OccupationDescription = OCCUPATION_DESCRIPTION


