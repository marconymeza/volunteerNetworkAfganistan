const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const TYPE_ORGANIZATION_DESCRIPTION = sequelize.define('TYPE_ORGANIZATION_DESCRIPTION', 
    {
      // definicion del los campos
      TYPE_ORGANIZATION_CODE: 
      {
        type: Sequelize.NUMERIC,
        primaryKey: true,
        allowNull: false
      },
      TYPE_ORGANIZATION_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'TYPE_ORGANIZATION_DESCRIPTION',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.TypeOrganizationDescription =  TYPE_ORGANIZATION_DESCRIPTION


