const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const EVENTS_VOLUNTEER = sequelize.define('EVENTS_VOLUNTEER', 
    {
      // definicion del los campos
      EVENTS_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      VOLUNTEER_ID : 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      VOLUNTEER_DESCRIPTION  : 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'EVENTS_VOLUNTEER',
      timestamps: false,
      freezeTableName: true,
    });
    

module.exports.EventsVolunteer = EVENTS_VOLUNTEER


