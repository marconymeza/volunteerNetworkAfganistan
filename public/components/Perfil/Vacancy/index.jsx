import React from 'react'
import { get } from 'https';
import random from 'uuid/v1'
import ReactTooltip from 'react-tooltip'
import cookie from 'react-cookies'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class PerfilVacancy extends React.Component {

    constructor() {
        super()
        this.state = {
            Job: [],
            Province: [],
            Expertise: [],
            data: [],
            isFollowing: 0
        }
        this.userInfo = cookie.load('dataUser')
    }
    componentDidMount() {
        this.getVacancy()
        this.isFollowing()
    }
    isFollowing() {

        fetch(`${window.location.origin}/get/ValidateRequestVacancy/${this.userInfo.ENTITY_ID}/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    isFollowing: x.data[0].REQUEST_STATUS
                })
            })
    }
    Follow() {
        var data = {
            Vacancy: this.props.id,
            Rentity: this.userInfo.ENTITY_ID,
            ID:random(),
            ENTITY_ORG:this.state.data.ENTITY_MASTER.ENTITY_ID
        }
        fetch(window.location.origin + '/Insert/NotificationVacancy/',
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        })
        .then((response) => {
            return response.json()
                .then((x) => {
                    
                    console.log(x,"mar",response.status)

                })
        })
        fetch(window.location.origin + '/Insert/FollowVacancy/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202) {
                            alertify.success('Applying');
                            this.setState({
                                isFollowing: 1
                            })
                        }
                        else
                            alertify.success('Failed');

                    })
            })


    }
    UnFollow() {
        var data = {
            Vacancy: this.props.id,
            Rentity: this.userInfo.ENTITY_ID

        }
        fetch(window.location.origin + '/Update/UnFollowVacancy/',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202)
                            {
                            alertify.success('Not Applying');
                            this.setState({
                                isFollowing:0
                            })
                            }
                        else 
                        alertify.success('Failed');

                    })
            })
    }
    getVacancy() {
        fetch(`${window.location.origin}/get/GetVacancy/1/${this.props.id}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    data: x.data[0]
                })
                if (x.data[0].IMAGE != undefined && x.data[0].IMAGE != null) {
                    $("#blah").attr('src', '/images/vacancyImages/' + x.data[0].IMAGE);
                    document.getElementById("blah").className = "img-fluid imgblah"
                }





                document.getElementById('INCENTIVES').innerHTML = x.data[0].INCENTIVES
                document.getElementById('DESCRIPTION').innerHTML = x.data[0].DESCRIPTION
                document.getElementById('EDUCATION_REQUIRED').innerHTML = x.data[0].EDUCATION_REQUIRED
                document.getElementById('WORK_EXPERIENCE_REQUIRED').innerHTML = x.data[0].WORK_EXPERIENCE_REQUIRED



                var t1 = [...new Set(x.data.map(item => {
                    if (item.VACANCY_EXPERTISE != null)
                        return item.VACANCY_EXPERTISE.AREA_ID
                }))]
                var tt1 = [...new Set(x.data.map(item => {
                    if (item.VACANCY_EXPERTISE != null)
                        return item.VACANCY_EXPERTISE.AREA_DESCRIPTION
                }))]
                if (t1[0] != undefined)
                    t1.map((y, o) => {
                        if (y != null)

                            this.state.Expertise.push({ id: y, description: tt1[o] })
                    })

                var t2 = [...new Set(x.data.map(item => {
                    if (item.VACANCY_JOB != null)
                        return item.VACANCY_JOB.JOB_ID
                }))]
                var tt2 = [...new Set(x.data.map(item => {
                    if (item.VACANCY_JOB != null)
                        return item.VACANCY_JOB.JOB_DESCRIPTION
                }))]
                if (t2[0] != undefined)
                    t2.map((y, o) => {
                        if (y != null)

                            this.state.Job.push({ id: y, description: tt2[o] })
                    })

                var t3 = [...new Set(x.data.map(item => {
                    if (item.VACANCY_SCOPE != null)
                        return item.VACANCY_SCOPE.SCOPE
                }))]
                var tt3 = [...new Set(x.data.map(item => {
                    if (item.VACANCY_SCOPE != null)
                        return item.VACANCY_SCOPE.PROVINCE_DESCRIPTION.PROVINCE_DESCRIPTION
                }))]
                if (t3[0] != undefined)
                    t3.map((y, o) => {
                        if (y != null)

                            this.state.Province.push({ id: y, description: tt3[o] })
                    })
                this.setState({
                    Expertise: this.state.Expertise,
                    Province: this.state.Province,
                    Job: this.state.Job

                })

            })
    }
    render() {
        return (
            <section id="profileVacancy">
                <div className="container-fluid">
                    <ReactTooltip />
                    <div className="row topSpace-1x">
                        <div className="col-6 text-left" data-tip="Go back"><a href="javascript:history.back()"><i className="fas fa-arrow-circle-left fa-2x redText"></i></a></div>
                    </div>
                    <div className="row">
                        <div className="col-12 text-center withoutPadding">
                            <img src="/images/Home/picture.png" onError={(e)=>{e.target.src="/images/vacancyImages/defaultVacancy.jpg"}} className="img-fluid topSpace" id="blah" />
                            <h3>{this.state.data.TITLE}</h3>

                            {this.state.data.ENTITY_MASTER != null && this.state.data.ENTITY_MASTER != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Organization:</strong> <Link to={'/perfil/' + this.state.data.ENTITY_MASTER.ENTITY_ID}>{this.state.data.ENTITY_MASTER.NAME}</Link></p> : null}

                            {this.state.data.NO_OF_JOBS != null && this.state.data.NO_OF_JOBS != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>No. of vacancies:</strong> {this.state.data.NO_OF_JOBS}</p> : null}

                            {this.state.data.VACANCY_TYPE_DESCRIPTION != null && this.state.data.VACANCY_TYPE_DESCRIPTION != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Type of vacancy:</strong> {this.state.data.VACANCY_TYPE_DESCRIPTION['VACANCY_TYPE_DESCRIPTION']}</p> : null}

                            {this.state.data.DURATION_DESCRIPTION != null && this.state.data.DURATION_DESCRIPTION != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Contract duration:</strong> {this.state.data.DURATION_DESCRIPTION.DURATION_DESCRIPTION}</p> : null}

                            {this.state.data.START_DATE != null && this.state.data.START_DATE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>Start date publication:</strong> {this.state.data.START_DATE.substring(0, 10)}</p> : null}

                            {this.state.data.END_DATE != null && this.state.data.END_DATE != "" ?
                                <p className="card-text withoutPadding withoutMargin"><strong>End date publication:</strong> {this.state.data.END_DATE.substring(0, 10)}</p> : null}
                            <hr />
                        </div>


                        <div className="col-12 text-center">

                            <ul className="nav nav-pills mb-3 center-pills" id="pills-tab" role="tablist">
                                <li className="nav-item">
                                    <a className="nav-link active" id="pills-Description-tab" data-toggle="pill" href="#pills-Description" role="tab" aria-controls="pills-Description" aria-selected="true">Description</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-experience-tab" data-toggle="pill" href="#pills-experience" role="tab" aria-controls="pills-experience" aria-selected="false">Work experience</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-Education required-tab" data-toggle="pill" href="#pills-Education required" role="tab" aria-controls="pills-Education required" aria-selected="false">Education required</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" id="pills-incentives-tab" data-toggle="pill" href="#pills-incentives" role="tab" aria-controls="pills-incentives" aria-selected="false">Additional incentives for volunteer</a>
                                </li>
                            </ul>
                            <div className="tab-content" id="pills-tabContent">
                                <div className="tab-pane fade show active" id="pills-Description" role="tabpanel" aria-labelledby="pills-Description-tab">
                                    {this.state.data.DESCRIPTION != null && this.state.data.DESCRIPTION != "" ?
                                        <div className="col-8 offset-2 text-center topSpace-2x">
                                            <div className="card-text withoutPadding withoutMargin" id="DESCRIPTION"></div>
                                        </div> : null}
                                </div>
                                <div className="tab-pane fade" id="pills-experience" role="tabpanel" aria-labelledby="pills-experience-tab">
                                    {this.state.data.WORK_EXPERIENCE_REQUIRED != null && this.state.data.WORK_EXPERIENCE_REQUIRED != "" ?
                                        <div className="col-10 offset-1 text-center topSpace-2x">
                                            <div className="card-text withoutPadding withoutMargin" id="WORK_EXPERIENCE_REQUIRED"></div>
                                        </div> : null}

                                </div>
                                <div className="tab-pane fade" id="pills-Education required" role="tabpanel" aria-labelledby="pills-Education required-tab">
                                    {this.state.data.EDUCATION_REQUIRED != null && this.state.data.EDUCATION_REQUIRED != "" ?
                                        <div className="col-10 offset-1 text-center topSpace-2x">
                                            <div className="card-text withoutPadding withoutMargin" id="EDUCATION_REQUIRED"></div>
                                        </div> : null}

                                </div>
                                <div className="tab-pane fade" id="pills-incentives" role="tabpanel" aria-labelledby="pills-incentives-tab">
                                    {this.state.data.INCENTIVES != null && this.state.data.INCENTIVES != "" ?
                                        <div className="col-10 offset-1 text-center topSpace-2x">
                                            <div className="card-text withoutPadding withoutMargin" id="INCENTIVES"></div>
                                        </div> : null}
                                </div>
                            </div>

                            <br />
                        </div>
                        <div className="col-12 text-center ">

                            {this.state.isFollowing == 0 ?
                                <a data-tip="Apply now" onClick={this.Follow.bind(this)} className="marginRigth"><i className="fas fa-check fa-2x redText"></i></a>
                                :
                                <a data-tip="Applied" onClick={this.UnFollow.bind(this)} className="marginRigth"><i className="fas fa-check fa-2x blueText"></i></a>

                            }
                            <a href="/chat" data-tip="Message"><i className="fas fa-comments fa-2x redText"></i></a>
                            <hr />
                        </div>







                        {this.state.Province.length != 0 ?
                            <div className="col-12 text-center topSpace-2x">
                                <h5>Duty stations(s)</h5>
                                {this.state.Province.map(x => {
                                    return (
                                        <span key={random()} className="badge badge-info marginRigth">{x.description}</span>
                                    )
                                })}
                            </div> : null}

                        {this.state.Job.length != 0 ?
                            <div className="col-12 text-center topSpace-2x">
                                <h5>Job (TOR) description(s)</h5>
                                {this.state.Job.map(x => {
                                    return (
                                        <span key={random()} className="badge badge-info marginRigth">{x.description}</span>
                                    )
                                })}
                            </div> : null}

                        {this.state.Expertise.length != 0 ?
                            <div className="col-12 text-center topSpace-2x bottomSpace">
                                <h5>Areas of expertise</h5>
                                {this.state.Expertise.map(x => {
                                    return (
                                        <span key={random()} className="badge badge-info marginRigth">{x.description}</span>
                                    )
                                })}
                            </div> : null}


                    </div>
                </div>

            </section>
        )
    }
}

export default PerfilVacancy