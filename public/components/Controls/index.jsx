import React from 'react'
import random from 'uuid/v1'
import md5 from 'md5'

export class Input extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
        <label htmlFor={this.props.id}>{this.props.TextLabel}</label>
        <input type={this.props.type} id={this.props.id} className={this.props.inputClass} />
      </div>
    )
  }
}
export class Option extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
      <label htmlFor={this.props.id}>{this.props.TextLabel}</label>
      <select id={this.props.id} className={this.props.selectClass} >
          <option value="99" >Select one</option>
          {this.props.array.map(x => {
              return (
                  <option key={md5(x[this.props.value])} value={x[this.props.value]}>{x[this.props.textOption]}</option>
              )
          })}
      </select>
  </div>
    )
  }
}
export class TextArea extends React.Component {
  render() {
    return (
      <div className={this.props.className}>
        <div className="form-group purple-border">
          <label  htmlFor={this.props.id}>{this.props.TextLabel}</label>
          <textarea  id={this.props.id}  rows={this.props.row} className={this.props.textareaClass}></textarea>
        </div>
      </div>
    )
  }
}

