import React from 'react'
import cookie from 'react-cookies'

class Dashboard extends React.Component{
    constructor(){
        super()

    }

    pass(){
        if(document.getElementById("user").value=="annv" && document.getElementById("password").value=="D@shb0ard")
        {
            document.getElementById("tablero").innerHTML = '    <iframe class="dash"\
            src="https://app.powerbi.com/view?r=eyJrIjoiNTUxZTViMTItN2I0Yy00OGVjLWIwNTQtMjI4MTdmYmU1NzBiIiwidCI6IjY5ODY3YjQ4LTZmZTktNGJhMy1hZjAyLThiNzNlNjFkNDA0YSIsImMiOjR9" \
            frameborder="0" \
            allowFullScreen="true"> \
            </iframe>'
        }
        else{
            document.getElementById("message").innerHTML = "User or password incorrect, try again."
        }
            
        
    }

    render(){
        return(
            <div id="tablero" class="tablero">
                <div className="container topSpace">
                    <div className="row">
                        <div className="offset-3 col-6">
                        <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">User</label>
                    <input type="text" class="form-control" id="user"  placeholder="Enter user"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password"/>
                </div>
                <button type="button" onClick={this.pass.bind(this)} class="btn btn-primary">Entry</button>
                </form>
                <p id="message"></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard