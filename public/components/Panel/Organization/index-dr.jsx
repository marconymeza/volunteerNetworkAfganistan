import React from 'react'
import md5 from 'md5'
import uuidv1 from 'uuid/v1'
import cookie from 'react-cookies'
import { Modal, Email } from '../../Modals'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";


class Organization extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            nameFile: "",
            volunteers: [],
        }
    }
 
    componentWillMount() {
        this.RequestInformationVacancy()
    }
    uploadFile() {

        if(document.getElementById("namedoc").value == "")
        alertify.error('نام سند نیاز است');
        else if(document.getElementById("file-upload2").files.length == 0)
        alertify.error('فایل را انتخاب نماید');
        
        else{
            if(this.props.Docs[0] != undefined)
            {
                fetch(`${window.location.origin}/update/UpdateDocument/${this.props.id}/${this.props.Docs[0]}@_@${document.getElementById("namedoc").value}@-@${document.getElementById("file-upload2").files[0].name}`, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                document.getElementById("formlegalDocs").submit()
            })
            }
            else{
                fetch(`${window.location.origin}/update/UpdateDocument/${this.props.id}/@_@${document.getElementById("namedoc").value}@-@${document.getElementById("file-upload2").files[0].name}`, { method: 'post' })
                .then((response) => {
                    return response.json()
                })
                .then((x) => {
                    document.getElementById("formlegalDocs").submit()
                })
            }
        }
        // fetch(`${window.location.origin}/update/UpdateDocument/${this.props.id}/${this.state.docs}`, { method: 'post' })
        //     .then((response) => {
        //         return response.json()
        //     })
        //     .then((x) => {
                
        //     })
    }
    RequestInformationVacancy() {
        fetch(window.location.origin + '/get/RequestInformation/ModRequestEntity/ENTITY_ID/' + this.props.id, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    volunteers: x.data
                })
            })
    }
    render() {
        $("#file-upload").change(function () { readURL(this); });
        $("#file-upload2").change(function () { document.getElementById("nameFile").innerHTML = document.getElementById("file-upload2").files[0].name; readURL(this); });
        return (
            <section id="UpdateOrganization">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 col-md-12  withoutPadding">
                            <div className="card">
                                <div className="card-header text-center">
                                پروفایل مو تازه کړئ
                                    <div className="col text-center">
                                        <form id="formImgOrg" name="formImgOrg" encType="multipart/form-data" action="api/upload/org" method="post">
                                            <img src="/images/Home/picture.png" className="img-fluid" id="blah" /><br />
                                            <label htmlFor="file-upload" className="custom-file-upload">
                                                <i className="fas fa-image"></i> تصویر را انتخاب نماید
                                                </label>
                                            <input type="file" id="file-upload" className="btn btn-primary" name="imgUploader" />
                                            <br />
                                        </form>                                        
                                        <a data-toggle="modal" className="blueText labelStyle" data-target="#seeVolunteer" >تعقیب کردن ({this.state.volunteers.length})</a>
                                    </div>
                                </div>
                                <div className="card-body withoutPadding">
                                    <div className="row">
                                        <div className="col-lg-1 col-lg-1  d-none d-lg-block greenW"></div>
                                        <div className="col-12 col-lg-2 greenW text-right">
                                            <div className="topSpace-1x">
                                                <label className="lblRight" className="withoutMargin">نام مکمل</label>
                                                <input type="text" id="name" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">هویت قانونی (جواز)</label>
                                                <input type="text" id="LegalIdentification" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight ">یوزر (کاربر)</label>
                                                <input type="text" id="User" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            /////////hasta aqui va a en dari
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">رمز ورود</label>
                                                <input type="password" id="PassWord" className="form-control Md-Select whiteText text-right"  />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight ">ایمیل</label>
                                                <input type="text" id="Email" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="md-form">
                                            <label  id="CommentaryLab" className="active lblRight withoutMargin ">معلومات در باره سازمان/ اداره</label>
                                            /////////hasta aqui va a en dari
                                                <textarea  type="text" id="Commentary" row="4" className="Md-Select-textArea whiteText text-right"/>
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight ">شماره تیلفون</label>
                                                <input type="text" id="Telephone" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="form-group">
                                                <label className="labelStyle lblRight ">نوع سازمان/ اداره</label>
                                                <select id="TypeOrganization" className="form-control Md-Select whiteText withoutPadding text-right">
                                                    <option id="TypeOrganization" >یکی را انتخاب کنید</option>
                                                    {this.props.TypeOrganization.map(x => {
                                                        return (
                                                            <option key={md5(x.TYPE_ORGANIZATION_CODE)}
                                                                name={x.TYPE_ORGANIZATION_DESCRIPTION}
                                                                id={x.TYPE_ORGANIZATION_DESCRIPTION}
                                                                value={x.TYPE_ORGANIZATION_CODE}>
                                                                {x.TYPE_ORGANIZATION_DESCRIPTION}
                                                            </option>
                                                        )
                                                    })}
                                                </select>

                                            </div>
                                            <div className="md-form">
                                                <label className="labelStyle lblRight"><a data-toggle="modal" data-target="#scopes">پوشش جغرافیایی (شاخه ها)<i className="fas fa-map-marker fa-1x"></i></a></label> <br /><br />
                                                <ul>
                                                    {this.props.sxt.map((x, y) => {
                                                        return (
                                                            <li key={uuidv1()}><p name="Ars">{x}</p></li>
                                                        )
                                                    })}
                                                </ul>
                                                <button type="button" className="btn  btn-info  btn-sm" data-toggle="modal" data-target="#legalDocs">سندهای قانونی</button>
                                            </div>
                                        </div>
                                        <div className="col-12 col-lg-2 greenW text-right ">
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight ">شخص ارتباطی</label>
                                                <input type="text" id="Nresponsible" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                            <label className="withoutMargin lblRight ">شماره تیلفون شخص ارتباطی</label>
                                                <input type="number" id="NTelephone" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight ">صفحه انترنیتی</label>
                                                <input type="text" id="Website" className="form-control Md-Select whiteText text-right"  />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">ګوګل +</label>
                                                <input type="text" id="Google" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">فیسبوک</label>
                                                <input type="text" id="Facebook" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">انسټاګرام</label>
                                                <input type="text" id="Instagram" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">ټویټر</label>
                                                <input type="text" id="Twitter" className="form-control Md-Select whiteText text-right" />
                                            </div>

                                            <div className="topSpace-1x">
                                            <label className="withoutMargin lblRight">ساحی کاری سازمان/ اداره</label>
                                                <input type="text" id="WorkArea" className="form-control Md-Select whiteText text-right" />
                                            </div>
                                            <div className="topSpace-1x">
                                                <label className="withoutMargin lblRight">ادرس مکمل سازمان</label>
                                                <input type="text" id="Address" className="form-control Md-Select whiteText text-right" />
                                            </div>

                                        </div>
                                        <div className="col-md-1 greenW  d-none d-lg-block"></div>
                                        <div className="col-12 col-md-12 col-lg-6 orangeW topSpace-2x">
                                            <div className="row">
                                                <div className="col-12">
                                                    <h4 className="text-center" >دومداره پرمختیایی موخې</h4> <br />
                                                </div>
                                                {this.props.Sdg.map(x => {
                                                    return (
                                                        <div key={x.SDG_CODE} className="col-12 col-md-6 col-lg-4">
                                                            <div className="form-check">
                                                                <input
                                                                    className="form-check-input"
                                                                    type="checkbox" id={x.SDG_CODE}
                                                                    name="Sdg"
                                                                    value={x.SDG_CODE} />
                                                                <img src={`/images/SDG/${x.SDG_CODE}.png`} className="smallImg" data-toggle="tooltip" data-placement="top" title={x.SDG_DESCRIPTION} />

                                                            </div>
                                                        </div>
                                                    )
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-footer text-muted">
                                    <button onClick={this.props.UpdateOrganization} className="btn btn-info btn-lg btn-block" type="button">تغیرات را ثبت نماید<i className="fas fa-save ml-2"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal />
                <div className="modal fade bd-example-modal-lg" id="scopes" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">ولایت ایتان را انتخاب نماید</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {this.props.Province.map(x => {
                                    return (
                                        <div key={md5(x.PROVINCE_CODE)} className="form-check form-check-inline">
                                            <input className="form-check-input" id={x.PROVINCE_CODE} name="Scope" type="checkbox" value={x.PROVINCE_CODE} />
                                            <label className="form-check-label" id={"label" + x.PROVINCE_CODE} htmlFor="defaultCheck1">
                                                {x.PROVINCE_DESCRIPTION}
                                            </label>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info btn-lg btn-block" data-dismiss="modal" type="button">بسته کردن</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal fade" id="legalDocs" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalCenterTitle">سندهای قانونی</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="col-12">

                                    <form id="formlegalDocs" name="formlegalDocs" encType="multipart/form-data" action="api/upload/legalDocs" method="post">
                                        <p id="nameFile"></p>
                                        <label htmlFor="file-upload2" className="custom-file-upload">
                                            <i className="fas fa-image"></i> فایل را انتخاب نماید
                                                </label>
                                        <input type="file" id="file-upload2" className="btn btn-primary text-right" name="imgUploader" />
                                        <br />
                                    </form>
                                    <input type="text" className="form-control" id="namedoc" placeholder="نام سند"/>
                                </div>
                                <hr/>
                                <ul className="list-group">
                                
                                {this.props.Docs.length>0?
                                this.props.Docs[1].map(x=>{
                                    return(
                                        <li key={uuidv1()} className="list-group-item d-flex justify-content-between align-items-center">
                                        <span>{x[0]}</span>
                                        {/* <i className="fas fa-times redText"></i> */}
                                        <a href={`/images/legalDocs/${x[1]}`} target="_blank"><span className=" badge-pill"><i className="fas fa-eye redText marginRigth"></i></span></a>
                                    </li>
                                    )
                                }):
                                null}
                                    
                                </ul>
                            </div>
                            <div className="modal-footer">
                                <button type="button" onClick={this.uploadFile.bind(this)} className="btn btn-primary">سند جدید را تازه نماید</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/*****************************************************************SEE PEOPPLE**************************************************************/}
                <div className="modal fade" id="seeVolunteer" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5>Volunteers</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-darks">
                                    <tbody >
                                        {this.state.volunteers.map(x => {
                                            return (
                                                <tr key={uuidv1()}>
                                                {x.ENTITY_MASTER.NAME ==""?
                                                    <th scope="row" onClick={$('#seeVolunteer').modal('hide')} className="whiteText"><Link to={'/perfilVolunteer/' + x.ENTITY_MASTER.ENTITY_ID}>Nameless</Link>  </th>
:
                                                    <th scope="row" onClick={$('#seeVolunteer').modal('hide')}  className="whiteText"><Link to={'/perfilVolunteer/' + x.ENTITY_MASTER.ENTITY_ID}>{x.ENTITY_MASTER.NAME}</Link>  </th>
                                                
                                            }
                                                </tr>
                                            )
                                        })}
                                    </tbody>

                                </table>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">بسته کردن</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Organization