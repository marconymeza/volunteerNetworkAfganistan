import React from 'react'
import cookie from 'react-cookies'

class Login extends React.Component {

    constructor() {
        super()
        this.state = {
            Login:[]
        }
        this.Login = this.Login.bind(this)
    }

    Login(event) {
        event.preventDefault();
        var data = {
            Password: document.getElementById('loginPassword').value,
            Email: document.getElementById('loginEmail').value
        }
        if (data.Email == "") {
            alertify.error('Email or  User name is required');
            document.getElementById("loginEmail").className = "form-control error"

        } else if (data.Password == "") {
            alertify.error('Password is required');
            document.getElementById('loginPassword').className = "form-control error"
        }
        else {
            fetch(window.location.origin + '/Get/ValidationLogin/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status==200) {
                                    alertify.success('Successful')
                                    cookie.save('dataUser', JSON.stringify(x.data[0]), { path: '/' })
                                    setTimeout(() => {
                                        window.open('/panel', "_self")
                                      }, 2000);
                            }else if(response.status==404){
                                
                                alertify.error('Please verify your email')
                            }else if(response.status==500){
                                
                                alertify.error('User name or Email incorrect')
                            }
                        })
                })
        }
    }

    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }

    render() {
        return (
            <section id="Login">
                <div className="container-fluid topSpace">
                    <div className="row text-center" >
                        <form name="login" className="form-signin"onSubmit={this.Login}>
                            <img className="img-fluid" src="/images/Home/LogoColor.png" />
                            <h1 className="h3 mb-3 font-weight-normal">Please log in</h1>
                            <label htmlFor="inputEmail" className="sr-only">Email address</label>
                            <div className="md-form">
                                <input type="text" id="loginEmail" onChange={this.ifEmpty.bind(this, "loginEmail", "form-control")} className="form-control"/>
                                <label htmlFor="loginEmail">User name or Email address</label>
                            </div>
                            <div className="md-form">
                                <input type="password" id="loginPassword" onChange={this.ifEmpty.bind(this, "loginPassword", "form-control")} className="form-control" />
                                <label htmlFor="loginPassword">Password</label>
                            </div>
                            <button type="submit"  className="btn btn-lg btn-info btn-block" >Log in</button>
                            <a className="text-left" href={`${window.location.origin}/ForgotPassword`}>Forgot user name or password?</a>
                        </form>
                    </div>
                </div>
            </section>
        )
    }
}

export default Login