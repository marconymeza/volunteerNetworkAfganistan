var express = require('express');
var router = express.Router();
var Sequelize = require('sequelize');
var ModEntityMaster = require('../../models/EntityMaster').EntityMaster;
var ModNacionalityDescription = require('../../models/NacionalityDescription').NacionalityDescription;
var ModGenderDescription = require('../../models/GenderDescription').GenderDescription;
var ModTypeOrganizationDescription = require('../../models/TypeOrganizationDescription').TypeOrganizationDescription;
var ModLogin = require('../../models/Login').Login;
var ModEntitySdg = require('../../models/EntitySdg').EntitySDG;
var ModEntityScope = require('../../models/EntityScope').EntityScope;
var ModEntityAbilities = require('../../models/EntityAbilities').EntityAbilities;
var ModVacancyJob = require('../../models/VacancyJob').VacancyJob;
var ModVacancyArea = require('../../models/VacancyExpertise').VacancyExpertise;
var ModVacancy = require('../../models/VacancyAnnouncement').VacancyAnnouncement;
var ModVacancyScope = require('../../models/VacancyScope').VacancyScope;
var ModTypeVacancy = require('../../models/VacancyTypeDescription').VacancyTypeDescription;
var ModDuration = require('../../models/DurationDescription').DurationDescription;
var ModWork = require('../../models/WorkExperienceDescription').WorkDescription;
var ModEducation = require('../../models/EducationDescription').EducationDescription;
var ModView = require('../../models/ViewEntityMaster').ViewEntity;
var ModScopeDescription = require('../../models/ProvinceDescription').ProvinceDescription;
var ModEvent = require('../../models/Events').Events;
var ModEventScope = require('../../models/EventScope').EventScope;
var ModEventVolunteer = require('../../models/EventsVolunteer').EventsVolunteer;
var ModViewVacancy = require('../../models/ViewVacancyMaster').ViewVacancy;
var ModProfession = require('../../models/ProfessionDescription').ProfessionDescription;
var ModPeriod = require('../../models/PeriodDescription').PeriodDescription;
var ModBloodDescription = require('../../models/BloodGroupDescription').BloodGroupDescription;
var ModDistrict = require('../../models/DistrinctDescription').DistrinctDescription;
var ModEntityProfession = require('../../models/EntityProfession').EntityProfession;
var ModEntityOccupation = require('../../models/EntityOccupation').EntityOcupation;
var ModRequestEntity = require('../../models/RequestEntity').RequestEntity;
var ModRequestEvent = require('../../models/RequestEvents').RequestEvents;
var  NotificationCenter= require('../../models/NotificationCenter').NotificationCenter
var ModRequestVacancy = require('../../models/VacancyRequest').VacancyRequest;
var app = express();
const Op = Sequelize.Op;
var sequelize = require('../../bin/ormConfig').sequelize;

var Models = {
  ModEvent: require('../../models/Events').Events,
  ModView: require('../../models/ViewEntityMaster').ViewEntity,
  ViewVacancy: require('../../models/ViewVacancyMaster').ViewVacancy,
  ModEntitySdg: require('../../models/EntitySdg').EntitySDG,
  ModEntityScope: require('../../models/EntityScope').EntityScope,
  ModEntityMaster: require('../../models/EntityMaster').EntityMaster,
  ModViewVacancy: require('../../models/ViewVacancyMaster').ViewVacancy,
  ModRequestVacancy: require('../../models/VacancyRequest').VacancyRequest,
  ModRequestEvent: require('../../models/RequestEvents').RequestEvents,
  ModRequestEntity:require('../../models/RequestEntity').RequestEntity,
  ModVacancy:require('../../models/VacancyAnnouncement').VacancyAnnouncement,
  ModEvent: require('../../models/Events').Events
}
router.post('/GetCompleteInformation/:ID/', function (req, res) {
  var queue = Promise.resolve();
  ModEntityMaster.belongsTo(ModNacionalityDescription, { foreignKey: 'NACIONALITY', targetKey: 'NACIONALITY_CODE' });
  ModEntityMaster.belongsTo(ModGenderDescription, { foreignKey: 'GENDER', targetKey: 'GENDER_CODE' });
  ModEntityMaster.belongsTo(ModTypeOrganizationDescription, { foreignKey: 'TYPE_ORGANIZATION', targetKey: 'TYPE_ORGANIZATION_CODE' });
  ModEntityMaster.belongsTo(ModEntitySdg, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModEntityMaster.belongsTo(ModLogin, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModEntityMaster.belongsTo(ModEntityScope, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModEntityMaster.belongsTo(ModEntityAbilities, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModEntityMaster.belongsTo(ModEntityProfession, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModEntityMaster.belongsTo(ModEntityOccupation, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModEntityScope.belongsTo(ModScopeDescription, { foreignKey: 'SCOPE', targetKey: 'PROVINCE_CODE' });
  ModEntityMaster.belongsTo(ModBloodDescription, { foreignKey: 'BLOOD_GROUP', targetKey: 'BLOOD_GROUP_CODE' });
  ModEntityMaster.belongsTo(ModEducation, { foreignKey: 'EDUCATION', targetKey: 'EDUCATION_CODE' });
  ModEntityMaster.belongsTo(ModPeriod, { foreignKey: 'PERIOD', targetKey: 'PERIOD_CODE' });
  ModEntityMaster.findAll({
    include: [{
      model: ModNacionalityDescription,
      attributes: ['NACIONALITY_DESCRIPTION']

    }, {
      model: ModGenderDescription,
      attributes: ['GENDER_DESCRIPTION'],


    },
    {
      model: ModLogin,
      attributes: ['USER_NAME', 'PASSWORD'],


    },
    {
      model: ModEntitySdg


    },
    {
      model: ModEntityAbilities


    },
    {
      model: ModEntityProfession


    },
    {
      model: ModEntityOccupation


    },
    {
      model: ModBloodDescription


    },
    {
      model: ModEducation


    },
    {
      model: ModPeriod


    },
    {
      model: ModTypeOrganizationDescription,
      attributes: ['TYPE_ORGANIZATION_DESCRIPTION'],
    },
    {
      model: ModEntityScope,
      include: [{
        model: ModScopeDescription
      }]

    },
    ],
    where: { ENTITY_ID: req.params.ID }

  }).then(x => {
    res.status(200).send({ data: x })

  });

})
router.post('/GetVacancy/:metodo/:ID/', function (req, res) {
  if (req.params.metodo == 1) {
    ModVacancy.belongsTo(ModVacancyArea, { foreignKey: 'VACANCY_ID', targetKey: 'VACANCY_ID' });
    ModVacancy.belongsTo(ModVacancyJob, { foreignKey: 'VACANCY_ID', targetKey: 'VACANCY_ID' });
    ModVacancy.belongsTo(ModVacancyScope, { foreignKey: 'VACANCY_ID', targetKey: 'VACANCY_ID' });
    ModVacancyScope.belongsTo(ModScopeDescription, { foreignKey: 'SCOPE', targetKey: 'PROVINCE_CODE' });
    ModVacancy.belongsTo(ModDuration, { foreignKey: 'DURATION', targetKey: 'DURATION_CODE' });
    ModVacancy.belongsTo(ModTypeVacancy, { foreignKey: 'VACANCY_TYPE', targetKey: 'VACANCY_TYPE_CODE' });
    ModVacancy.belongsTo(ModEntityMaster, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
    ModVacancy.findAll({
      include: [{
        model: ModVacancyArea
      },
      {
        model: ModVacancyJob


      },
      {
        model: ModDuration


      },
      {
        model: ModTypeVacancy


      },
      {
        model: ModVacancyScope,
        include: [{
          model: ModScopeDescription
        }]
      },
      {
        model: ModEntityMaster


      }

      ],

      where: {
        [Op.and]: [
          { VACANCY_ID: req.params.ID },
          { STATUS: 1 }
        ]
      }

    }).then(x => {
      res.status(200).send({ data: x })

    });

  } else {
    ModVacancy.findAll({
      where: {
        [Op.and]: [
          { ENTITY_ID: req.params.ID },
          { STATUS: 1 }
        ]
      }

    }).then(x => {
      res.status(200).send({ data: x })

    });

  }

})
router.post('/GetEvent/:metodo/:ID/', function (req, res) {
  if (req.params.metodo == 1) {
    var queue = Promise.resolve();
    ModEvent.belongsTo(ModEventScope, { foreignKey: 'EVENTS_ID', targetKey: 'EVENTS_ID' });
    ModEvent.belongsTo(ModEventVolunteer, { foreignKey: 'EVENTS_ID', targetKey: 'EVENTS_ID' });
    ModEventScope.belongsTo(ModScopeDescription, { foreignKey: 'SCOPE', targetKey: 'PROVINCE_CODE' });
    ModEvent.belongsTo(ModEntityMaster, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
    ModEvent.findAll({
      include: [{
        model: ModEventScope,
        include: [{
          model: ModScopeDescription
        }]
      },
      {
        model: ModEventVolunteer


      },
      {
        model: ModEntityMaster


      }
      ],
      where: {
        [Op.or]: [
          { EVENTS_ID: req.params.ID },
          { ENTITY_ID: req.params.ID }
        ],
        [Op.and]: [
          { STATUS: 1 }
        ]
      }

    }).then(x => {
      res.status(200).send({ data: x })

    });
  } else {
    ModEvent.findAll({
      where: {
        [Op.and]: [
          { ENTITY_ID: req.params.ID },
          { STATUS: 1 }
        ]
      }
    }).then(x => {
      res.status(200).send({ data: x })

    });

  }

})
router.post('/EntityHome/:type/:ID/', function (req, res) {

  sequelize.query("SELECT ENTITY_ID,NAME,EMAIL,TELEPHONE,FACEBOOK,INSTAGRAM,GOOGLE,TWITTER,COMMENTARY,TYPE_ENTITY,IMAGE   FROM [dbo].[VIEW_ENTITY_MASTER]  where TYPE_ENTITY = " + req.params.type + "   group by ENTITY_ID,NAME,EMAIL,TELEPHONE,FACEBOOK,INSTAGRAM,GOOGLE,TWITTER,COMMENTARY,TYPE_ENTITY,IMAGE     order by ENTITY_ID desc offset " + req.params.ID + " rows fetch next 8 rows only;", { type: sequelize.QueryTypes.SELECT }).then(x => {
    res.status(200).send({ data: x });
  })
});
router.post('/EventHome/:ID/', function (req, res) {

  sequelize.query("SELECT EVENTS_ID,TITULE,SUB_TITULE,DESCRIPTION,IMAGE,RESPONSIBLE_PERSON_NAME,RESPONSIBLE_TELEPHONE,RESPONSIBLE_EMAIL,DETAILS,OBJECTIVES,NAME     FROM [dbo].[VIEW_EVENT_MASTER] WHERE STATUS=1 group by EVENTS_ID,TITULE,SUB_TITULE,DESCRIPTION,IMAGE,RESPONSIBLE_PERSON_NAME,RESPONSIBLE_TELEPHONE,RESPONSIBLE_EMAIL,DETAILS,OBJECTIVES,NAME  order by EVENTS_ID desc offset " + req.params.ID + " rows fetch next 8 rows only;", { type: sequelize.QueryTypes.SELECT }).then(x => {
    res.status(200).send({ data: x });
  })
});
router.post('/VacancyHome/:ID/', function (req, res) {

  sequelize.query("SELECT VACANCY_ID,TITLE,DESCRIPTION,VACANCY_TYPE_DESCRIPTION,NO_OF_JOBS,DURATION_DESCRIPTION,START_DATE,END_DATE,IMAGE,NAME  FROM [dbo].[VIEW_VACANCY_MASTER] WHERE STATUS=1  group by VACANCY_ID,TITLE,DESCRIPTION,VACANCY_TYPE_DESCRIPTION,NO_OF_JOBS,DURATION_DESCRIPTION,START_DATE,END_DATE,IMAGE,NAME  order by VACANCY_ID desc offset " + req.params.ID + " rows fetch next 8 rows only;", { type: sequelize.QueryTypes.SELECT }).then(x => {
    res.status(200).send({ data: x });
  })
});

router.post('/FilterSdg/:Sdg/:ID/', function (req, res) {

  sequelize.query("SELECT T1.ENTITY_ID,T1.NAME,T1.EMAIL,T1.TELEPHONE,T1.FACEBOOK,T1.INSTAGRAM,T1.GOOGLE,T1.TWITTER,T1.COMMENTARY,T1.TYPE_ENTITY,T1.IMAGE  FROM [dbo].[VIEW_ENTITY_MASTER] AS T1 LEFT JOIN [dbo].[ENTITY_SDG] AS T2 ON T1.ENTITY_ID=T2.ENTITY_ID WHERE T2.SDG= '" + req.params.Sdg + "' group by T1.ENTITY_ID,T1.NAME,T1.EMAIL,T1.TELEPHONE,T1.FACEBOOK,T1.INSTAGRAM,T1.GOOGLE,T1.TWITTER,T1.COMMENTARY,T1.TYPE_ENTITY,T1.IMAGE   order by ENTITY_ID desc offset " + req.params.ID + " rows fetch next 8 rows only;", { type: sequelize.QueryTypes.SELECT }).then(x => {
    res.status(200).send({ data: x });
  })
});

router.post('/FilterLike/:model/:colum/:ID/:type/', function (req, res) {

  if (req.params.type == 1 || req.params.type == 2) {
    Models[String(req.params.model)].findAll({
      where: {
        [String(req.params.colum)]: {
          [Op.like]: ['%' + req.params.ID + '%']
        }
        ,
        [Op.and]: [
          { TYPE_ENTITY: req.params.type }
        ]
      }
    }).then(x => {
      res.status(200).send({ data: x })
    });
  } else {
    Models[String(req.params.model)].findAll({
      where: {
        [String(req.params.colum)]: {
          [Op.like]: '%' + req.params.ID + '%'
        }  ,
        [Op.and]: [
          { STATUS: 1}
        ]
      }
    }).then(x => {
      res.status(200).send({ data: x })
    });
  }
});
router.post('/Filter/:model/:colum/:ID/', function (req, res) {

  var queue = Promise.resolve();
  Models[String(req.params.model)].findAll({
    where: {
      [String(req.params.colum)]: req.params.ID
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/GetDistrict/:ID/', function (req, res) {

  var queue = Promise.resolve();
  ModDistrict.findAll({
    where: {
      PROVINCE_CODE: req.params.ID
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/GetEmail/:ID/', function (req, res) {

  var queue = Promise.resolve();
  ModEntityMaster.findAll({
    attributes: ['EMAIL', 'NAME', 'ENTITY_ID','IMAGE','TYPE_ENTITY'],
    where: {
      [Op.or]: [
        {
          EMAIL: {
            [Op.like]: '%' + req.params.ID + '%'
          }
        },
        {
          NAME: {
            [Op.like]:  '%' + req.params.ID + '%'
          }
        }
      ]
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/ValidateRequestEntity/:login/:entity', function (req, res) {
  ModRequestEntity.findAll({
    where: {
      [Op.and]: [
        { ENTITY_ID: req.params.entity },
        { RELATIONAL_ENTITY: req.params.login }
      ]
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/ValidateRequestEvent/:entity/:Event', function (req, res) {
  ModRequestEvent.findAll({
    where: {
      [Op.and]: [
        { EVENTS_ID: req.params.Event },
        { RELATIONAL_ENTITY: req.params.entity }
      ]
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/ValidateRequestVacancy/:entity/:Vacancy', function (req, res) {
  ModRequestVacancy.findAll({
    where: {
      [Op.and]: [
        { VACANCY_ID: req.params.Vacancy },
        { RELATIONAL_ENTITY: req.params.entity }
      ]
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/RequestInformation/:model/:colum/:ID/', function (req, res) {
  Models[String(req.params.model)].belongsTo(ModEntityMaster, { foreignKey: 'RELATIONAL_ENTITY', targetKey: 'ENTITY_ID' });
  Models[String(req.params.model)].findAll({
    include: [{
      model: ModEntityMaster,
      attributes: ['EMAIL', 'NAME', 'ENTITY_ID','IMAGE','TYPE_ENTITY'],
    }],
    where: {
      [String(req.params.colum)]: req.params.ID,
      REQUEST_STATUS:1
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });

});
router.post('/RequestInformationEventMe/:ID/', function (req, res) {
  NotificationCenter.belongsTo(ModEvent, { foreignKey: 'ENTITY_ID_SECONDARY', targetKey: 'EVENTS_ID' });
  NotificationCenter.belongsTo(ModVacancy, { foreignKey: 'ENTITY_ID_SECONDARY', targetKey: 'VACANCY_ID' });
  NotificationCenter.belongsTo(ModEntityMaster, { foreignKey: 'ENTITY_ID_PRIMARY', targetKey: 'ENTITY_ID' });
  NotificationCenter.findAll({
    include: [{
      model: ModEvent,
      attributes: ['ENTITY_ID', 'EVENTS_ID', 'TITULE']},
      {model: ModVacancy,
        attributes: ['ENTITY_ID', 'VACANCY_ID', 'TITLE']},
        {model:ModEntityMaster,
        attributes:['NAME']}
    ],
    where:{STATUS:1,ENTITY_ORG: req.params.ID,}
   
  }).then(x => {
    res.status(200).send({ data: x })
  }).catch(error => {
    console.log("==================================")
    console.log(error)

    res.status(400).send({ data: error })
  });
});
router.post('/RequestInformationVacancyMe/:ID/', function (req, res) {
  ModRequestVacancy.belongsTo(ModVacancy, { foreignKey: 'VACANCY_ID', targetKey: 'VACANCY_ID' });
  ModRequestVacancy.findAll({
    include: [{
      model: ModVacancy,
      where: {
      ENTITY_ID: req.params.ID,
      STATUS:1
      },
      attributes: ['ENTITY_ID', 'VACANCY_ID', 'TITLE']
    }],
    where:{REQUEST_STATUS:1}
   
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/RequestInformationVacancy/:ID/', function (req, res) {
  ModRequestVacancy.belongsTo(ModVacancy, { foreignKey: 'VACANCY_ID', targetKey: 'VACANCY_ID' });
  ModRequestVacancy.findAll({
    include: [{
      model: ModVacancy
    }],
    where: {
      RELATIONAL_ENTITY: req.params.ID,
      REQUEST_STATUS:1
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/RequestInformationEvent/:ID/', function (req, res) {
  ModRequestEvent.belongsTo(ModEvent, { foreignKey: 'EVENTS_ID', targetKey: 'EVENTS_ID' });
  ModRequestEvent.findAll({
    include: [{
      model: ModEvent
    }],
    where: {
      RELATIONAL_ENTITY: req.params.ID,
      REQUEST_STATUS:1
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});
router.post('/RequestInformationEntity/:ID/', function (req, res) {
  ModRequestEntity.belongsTo(ModEntityMaster, { foreignKey: 'ENTITY_ID', targetKey: 'ENTITY_ID' });
  ModRequestEntity.findAll({
    include: [{
      model: ModEntityMaster
    }],
    where: {
      RELATIONAL_ENTITY: req.params.ID,
      REQUEST_STATUS:1
    }
  }).then(x => {
    res.status(200).send({ data: x })
  });
});

module.exports = router;