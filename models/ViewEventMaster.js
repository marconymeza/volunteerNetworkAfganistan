const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const VIEW_EVENT_MASTER = sequelize.define('VIEW_EVENT_MASTER', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      EVENTS_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      START_DATE: 
      {
        type: Sequelize.DATE
      },
      END_DATE: 
      {
        type: Sequelize.DATE
      },
      STATUS: 
      {
        type: Sequelize.NUMERIC
      },
      NOTIFICATION_STATUS: 
      {
        type: Sequelize.BOOLEAN
      },
      LATITUD: 
      {
        type: Sequelize.STRING
      },
      LONGITUDE: 
      {
        type: Sequelize.STRING
      },
      TITULE: 
      {
        type: Sequelize.STRING
      },
      SUB_TITULE: 
      {
        type: Sequelize.STRING
      },
      DESCRIPTION: 
      {
        type: Sequelize.STRING
      },
      IMAGE: 
      {
        type: Sequelize.STRING
      },
      RESPONSIBLE_PERSON_NAME: 
      {
        type: Sequelize.STRING
      },
      RESPONSIBLE_TELEPHONE: 
      {
        type: Sequelize.STRING
      },
      RESPONSIBLE_EMAIL: 
      {
        type: Sequelize.STRING
      },
      DETAILS: 
      {
        type: Sequelize.STRING
      },
      OBJECTIVES: 
      {
        type: Sequelize.STRING
      },
      NAME: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'VIEW_EVENT_MASTER',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.ViewEventMaster = VIEW_EVENT_MASTER


