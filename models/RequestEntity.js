const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const REQUEST_ENTITY = sequelize.define('REQUEST_ENTITY', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      RELATIONAL_ENTITY: 
      {
        type: Sequelize.STRING
      },
      REQUEST_STATUS: 
      {
        type: Sequelize.NUMERIC
      },
      REQUEST_DATE: 
      {
        type: Sequelize.DATE
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'REQUEST_ENTITY',
      timestamps: false,
      freezeTableName: true,
    });
    


module.exports.RequestEntity = REQUEST_ENTITY


