import React from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import cookie from 'react-cookies'



class Home extends React.Component {
    constructor() {
        super()
        this.state = {
            Events: [],
            CountExpertiseVacancy: []
        }
    }

    goHome() {
        location.href = '/home'
    }
    bubbleSort(a) {
        var swapped;
        do {
            swapped = false;
            for (var i=0; i < a.length-1; i++) {
                if (a[i].START_DATE < a[i+1].START_DATE) {
                    var temp = a[i];
                    a[i] = a[i+1];
                    a[i+1] = temp;
                    swapped = true;
                }
            }
        } while (swapped);
        return a
    }
    
    componentDidMount() {
        switch (cookie.load('language').language) {
            case 'en':
                document.getElementById("lbl1").innerHTML = 'PUTTING SKILLS INTO ACTION'
                break;
        
            default:
                break;
        }
        fetch(window.location.origin +'/Get/new/events',
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({"campos":["EVENTS_ID","START_DATE","TITULE","END_DATE"]})
        })
        .then((response) => {
            return response.json()
                .then((x) => {
                    (response.status === 200 || response.status === 202)?this.setState({Events:this.bubbleSort(x.data)}):null
                })
        })


        fetch(`${window.location.origin}/get/CountExpertiseVacancy/`, { method: 'get' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                x.data.map((r, y) => {
                    if (r.AREA_ID.length < 3)
                        this.state.CountExpertiseVacancy.push(r)
                })
                this.setState({ CountExpertiseVacancy: this.state.CountExpertiseVacancy })
            })




        // this.getDataCard('/Get/EventHome/', 'Events')
    }
    render() {
        return (
            <section  >
                <section id="particles-js" className="cover">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-12">
                            </div>
                        </div>
                    </div>
                </section>
                <div className="row">
                    <div className="col-12 text-center">
                        <p><i className="fas fa-angle-down blueText fa-5x"></i></p>
                    </div>
                </div>
                <div className="container topSpace bottomSpace">
                    <div className="row">
                        <div className="col-12 text-center bottomSpace">
                            <h4 className="blueText" id="lbl1"></h4>
                            <h1 className="colorGray" >So many ways to volunteer</h1>
                        </div>
                        <div className="col-8 offset-2 text-center">
                            <div className="row">
                                {this.state.CountExpertiseVacancy.map(x => {
                                    return (
                                        <div key={x.AREA_ID} className="col-3 cardRed text-center">
                                            <img src={`/images/Habilities/${x.AREA_ID}.png`} className="img-fluid mediumImage" />
                                            <p className="font-weight-light">{x.AREA_DESCRIPTION} <br /><strong>({x.Conteo})</strong></p>
                                        </div>
                                    )
                                })}
                            </div>
                            <button type="button" onClick={this.goHome.bind(this)} className="btn  btn-info  btn-sm">See vacancies</button>
                        </div>
                    </div>
                </div>
                <section className="blueBack">
                    <div className="container topSpace bottomSpace">
                        <div className="row">
                            <div className="col-12 text-center bottomSpace">
                                <h4 className="">GET STARTED</h4>
                                <h1 className="" >Find an event to make a difference</h1>
                            </div>
                            {this.state.Events.map(x => {
                                
                                    return (
                                        <div key={x.EVENTS_ID} className="col-3 text-center">
                                            <div className="card border-info mb-3 cardRed" >
                                                <div className="card-header blueText">{x.TITULE.substring(0, 23)}</div>
                                                <div className="card-body text-info">
                                                    <Link to={'/perfilEvent/' + x.EVENTS_ID}>View event</Link>
                                                </div>
                                            </div>
                                        </div>
                                    )
                            })}
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <div className="row grayColor bottomSpace topSpace">
                        <div className="col-12 text-center">
                            <p>© 2018 Ministry of Information & Culture all rights reserved</p>
                            <p>Database Development is Supported by UNDP</p>
                        </div>
                    </div>
                </section>
            </section>
        )
    }
}

export default Home