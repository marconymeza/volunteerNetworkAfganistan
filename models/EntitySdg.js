const Sequelize = require('sequelize');
const sequelize = require('../bin/ormConfig').sequelize;
// Empaquetado del modelo
    // definicio del modelo(tabla)
    const ENTITY_SDG = sequelize.define('ENTITY_SDG', 
    {
      // definicion del los campos
      ENTITY_ID: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      SDG: 
      {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
      },
      SDG_DESCRIPTION: 
      {
        type: Sequelize.STRING
      }
    },
    // Mas configuracion del modelo
    {
      tableName: 'ENTITY_SDG',
      timestamps: false,
      freezeTableName: true,
    });
    

module.exports.EntitySDG = ENTITY_SDG


