import React from 'react'
import random from 'uuid/v1'
import cookie from 'react-cookies'
import md5 from 'md5'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Input, Option, TextArea } from '../Controls'
class Vacancy extends React.Component {

    constructor() {
        super()
        this.state = {
            // Education: [],
            VacancyType: [],
            JobDescriptor: [],
            Areasofexpertise: [],
            Duration: [],
            Province: [],
            Abilities: [],
            scope: [],
            IdVacancy: "",
            volunteers: [],
            VacancyAall: []
        }
        this.img = null
        this.userInfo = cookie.load('dataUser')
    }
    fillCombobox(url, array) {
        fetch(window.location.origin + '/Get/api/obtener/' + url, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    [array]: x.data
                })
            })
    }
    getDataVacancy() {
        fetch(window.location.origin + '/Get/GetVacancy/all/' + this.userInfo.ENTITY_ID, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    VacancyAall: x.data
                })
            })
    }
    componentDidMount() {
        this.getDataVacancy()
        this.fillCombobox('ModVacancyType/VACANCY_TYPE_CODE', 'VacancyType')
        // this.fillCombobox('ModEducation/EDUCATION_CODE', 'Education')
        this.fillCombobox('ModDurationDescription/DURATION_CODE', 'Duration')
        // this.fillCombobox('ModWorkDescription/WORK_EXPERIENCE_CODE', 'Work')
        this.fillCombobox('ModProvinceDescription/PROVINCE_CODE', 'Province')
        this.fillCombobox('ModAbilitiesDescription/ABILITIES_CODE', 'Abilities')
    }
    reload() {
        window.location.replace("/")
    }
    renderScopes() {
        var arr = []
        $("input:checkbox[name=Scope]:checked").each(function () {
            arr.push($(this).val());
        });
        // this.state.scope = arr
        this.setState({
            scope: arr
        })
    }
    InsertVacancy() {
        if (document.getElementById('file-upload').files[0] != undefined)
            this.img = document.getElementById('file-upload').files[0].name

        var data = {
            IdVacancy: this.state.IdVacancy,
            Title: document.getElementById('Title').value,
            NoOfJobs: document.getElementById('NoOfJobs').value,
            Entity: this.userInfo.ENTITY_ID,
            Vacancy: md5(document.getElementById('Title').value),
            Description: document.getElementById('Description').value,
            AddIncentForVolunteer: document.getElementById('AddIncentForVolunteer').value,
            StartDate: document.getElementById('StartDate').value,
            EndDate: document.getElementById('EndDate').value,
            Status: 1,
            scope: [],
            nameImg: this.img,
            TypeVacancy: document.getElementById('VacancyType').value,
            Duration: document.getElementById('Duration').value,
            EducationRequired: document.getElementById('EducationRequired').value,
            WorkExperience: document.getElementById('WorkExperience').value,
            Areasofexpertise: this.state.Areasofexpertise,
            JobDescriptor: this.state.JobDescriptor
        }

        $("input:checkbox[name=Scope]:checked").each(function () {
            data.scope.push($(this).val().split('#')[0]);
        });
        if (data.Title == "") {
            alertify.error('Title is required');
        } else if (data.Description == "") {
            alertify.error('Description is required');
        } else if (data.StartDate == "") {
            alertify.error('Date is required');
        } else if (data.EndDate == "") {
            alertify.error('Date is required');
        }
        else {
            fetch(window.location.origin + '/Insert/InsertVacancy/',
                {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(data)
                })
                .then((response) => {
                    return response.json()
                        .then((x) => {
                            if (response.status == 200 || response.status == 202) {
                                alertify.success('Successful')
                                setTimeout(
                                    function () {
                                        document.getElementById("formImgVacancy").submit()
                                    }, 2000);
                            }
                            else {
                                alertify.error('Failed')
                            }
                        })
                })
        }
    }
    update(x) {
        fetch(window.location.origin + '/Get/GetVacancy/1/' + x, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                x.data.map(x => {
                    if (x.VACANCY_EXPERTISE != null)
                        x.VACANCY_EXPERTISE['idDescription'] = `${x.VACANCY_EXPERTISE.AREA_ID}#${x.VACANCY_EXPERTISE.AREA_DESCRIPTION}`
                    if (x.VACANCY_JOB != null)
                        x.VACANCY_JOB['idDescription'] = `${x.VACANCY_JOB.JOB_ID}#${x.VACANCY_JOB.JOB_DESCRIPTION}`
                    if (x.VACANCY_SCOPE != null)
                        x.VACANCY_SCOPE['idDescription'] = `${x.VACANCY_SCOPE.SCOPE}#${x.VACANCY_SCOPE.PROVINCE_DESCRIPTION.PROVINCE_DESCRIPTION}`
                })
                var t = []
                var tt = []
                var fpr = []

                var grouped = _.mapValues(_.groupBy(x.data, 'VACANCY_EXPERTISE.idDescription'),
                    clist => clist.map(car => _.omit(car, 'VACANCY_EXPERTISE.idDescription')));
                for (var propertyName in grouped) {
                    t.push({ description: propertyName.split('#')[1], id: propertyName.split('#')[0] })
                }
                var grouped = _.mapValues(_.groupBy(x.data, 'VACANCY_JOB.idDescription'),
                    clist => clist.map(car => _.omit(car, 'VACANCY_JOB.idDescription')));
                for (var propertyName in grouped) {
                    tt.push({ description: propertyName.split('#')[1], id: propertyName.split('#')[0] })
                }
                var grouped = _.mapValues(_.groupBy(x.data, 'VACANCY_SCOPE.idDescription'),
                    clist => clist.map(car => _.omit(car, 'VACANCY_SCOPE.idDescription')));
                for (var propertyName in grouped) {
                    fpr.push(propertyName)
                    if (propertyName.split("#")[0] == undefined)
                        document.getElementById(propertyName.split("#")[0]).checked = true
                }
                this.setState({
                    JobDescriptor: tt,
                    Areasofexpertise: t,
                    scope: fpr
                })
                this.img = x.data[0].IMAGE
                this.state.IdVacancy = x.data[0].VACANCY_ID
                document.getElementById('Title').value = x.data[0].TITLE
                document.getElementById('NoOfJobs').value = x.data[0].NO_OF_JOBS
                $("#blah").attr('src', '/images/vacancyImages/' + x.data[0].IMAGE);
                document.getElementById('StartDate').value = x.data[0].START_DATE.substring(0, 10)
                document.getElementById('EndDate').value = x.data[0].END_DATE.substring(0, 10)
                document.getElementById('VacancyType').value = x.data[0].VACANCY_TYPE
                document.getElementById('Duration').value = x.data[0].DURATION
                $('#EducationRequired').summernote('code', x.data[0].EDUCATION_REQUIRED)
                $('#WorkExperience').summernote('code', x.data[0].WORK_EXPERIENCE_REQUIRED)
                $('#AddIncentForVolunteer').summernote('code', x.data[0].INCENTIVES)
                $('#Description').summernote('code', x.data[0].DESCRIPTION)
            })
    }
    delete(x) {
        fetch(window.location.origin + '/Update/DeleteVacancy/' + x, { method: 'post' })
            .then((response) => {
                return response.json()
                    .then((x) => {
                        if (response.status == 200 || response.status == 202) {
                            alertify.success('Successful')
                            this.getDataVacancy()
                        }
                        else {
                            alertify.error('Failed')
                        }
                    })
            })
    }
    ifEmpty(e, c) {
        let control = document.getElementById(e)
        if (control.value)
            control.className = c
    }
    addItem(x) {
        this.state[x].push({ id: md5(document.getElementById(`txt${x}`).value), description: document.getElementById(`txt${x}`).value })
        this.setState({
            [x]: this.state[x]
        })
        document.getElementById("txt" + x).value = ""
    }
    deleteItem(array, item) {
        this.state[array].splice(item, 1)
        this.setState({
            [array]: this.state[array]
        })
    }
    addOptionToList(x) {
        this.state[x].push({ id: document.getElementById(`list${x}`).value.split('#')[0], description: document.getElementById(`list${x}`).value.split('#')[1] })
        this.setState({
            [x]: this.state[x]
        })
    }
    RequestInformationVacancy(x) {
        fetch(window.location.origin + '/get/RequestInformation/ModRequestVacancy/VACANCY_ID/' + x, { method: 'post' })
            .then((response) => {
                return response.json()
            })
            .then((x) => {
                this.setState({
                    volunteers: x.data
                })
            })
    }
    render() {
        $("#file-upload").change(function () { readURL(this); });
        $(document).ready(function () { $('#Description').summernote(); });
        $(document).ready(function () { $('#AddIncentForVolunteer').summernote(); });
        $(document).ready(function () { $('#WorkExperience').summernote(); });
        $(document).ready(function () { $('#EducationRequired').summernote(); });

        return (
            <section id="Modulevacancy">
                <div className="container-fluid topSpace-2x">
                    <div className="row">
                        <div className="col-12">
                            <div className="pos-f-t">
                                <div className="collapse" id="navbarToggleExternalContent">
                                    <div className="bg-darks p-4">
                                        <table className="table table-darks">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Title of Vacancy</th>
                                                    <th scope="col">Start Date Vacancy</th>
                                                    <th scope="col">End Date Vacancy</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody >
                                                {this.state.VacancyAall.map((x, y) => {
                                                    return (
                                                        <tr key={random()}>
                                                            <th scope="row">{y + 1}</th>
                                                            <th scope="row">{x.TITLE}</th>
                                                            <th scope="row">{x.START_DATE.substring(0, 10)}</th>
                                                            <th scope="row">{x.END_DATE.substring(0, 10)}</th>
                                                            <th >
                                                                <div className="row text-center">
                                                                    <div className="col-2 text-center">
                                                                        <a className="whiteText" href="#" onClick={this.update.bind(this, x.VACANCY_ID)} ><i className="fas fa-edit fa-1x"></i><br />edit</a>
                                                                    </div>
                                                                    <div className="col-2 text-center">
                                                                        <a className="whiteText" onClick={this.delete.bind(this, x.VACANCY_ID)} href="#"><i className="fas fa-times fa-1x"></i> <br />delete</a>
                                                                    </div>
                                                                    <div className="col-2 text-center">
                                                                        <a className="whiteText" data-toggle="modal" data-target="#seeVolunteer" onClick={this.RequestInformationVacancy.bind(this, x.VACANCY_ID)} href="#"><i className="fas fa-users fa-1x"></i> <br />Volunteers</a>
                                                                    </div>

                                                                </div>

                                                            </th>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <nav className="navbar navbar-dark bg-dark">
                                    <button id="viewEvent" className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i className="fas fa-eye"></i> View Table of Vacancies Announcement
                                    </button>
                                </nav>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div className="card border-info mb-4" >
                                    <div className="card-header blueIcon"><h5>Vacancy Announcement</h5> </div>
                                    <div className="card-body text-info">
                                        <div className="row">

                                            <div className="col text-center">
                                                <form id="formImgVacancy" name="formImgVacancy" encType="multipart/form-data" action="api/upload/vacancy" method="post">
                                                    <img src="/images/Home/picture.png" className="img-fluid" id="blah" /><br />
                                                    <label htmlFor="file-upload" className="custom-file-upload">
                                                        <i className="fas fa-image"></i> Select a Photo
                                                </label>
                                                    <input type="file" id="file-upload" className="btn btn-primary" name="imgUploader" />
                                                    <br />
                                                </form>
                                            </div>
                                        </div>
                                        <div className="row">

                                            <form className="vacancyForm">
                                                <div className="row">
                                                    <Input className="col" TextLabel="Title of Vacancy" type="text" id="Title" inputClass="form-control " />
                                                    <Input className="col" TextLabel="No. of Vacancies" type="number" id="NoOfJobs" inputClass="form-control " />
                                                    <Option className="col" id="VacancyType" TextLabel="Type of Vacancy" selectClass="form-control " array={this.state.VacancyType} value="VACANCY_TYPE_CODE" textOption="VACANCY_TYPE_DESCRIPTION" />
                                                </div>
                                                <div className="row topSpace-2x">
                                                    <Option className="col" id="Duration" TextLabel="Contract Duration" selectClass="form-control " array={this.state.Duration} value="DURATION_CODE" textOption="DURATION_DESCRIPTION" />
                                                    <Input className="col" TextLabel="Start Date Publication" type="date" id="StartDate" inputClass="form-control " />
                                                    <Input className="col" TextLabel="End Date Publication" type="date" id="EndDate" inputClass="form-control " />
                                                </div>
                                                <div className="row topSpace-2x">
                                                    <TextArea className="col" id="WorkExperience" TextLabel="Work Experience" textareaClass="form-control" />

                                                    <TextArea className="col" id="EducationRequired" TextLabel="Education Required" textareaClass="form-control" />
                                                </div>
                                                <div className="row topSpace-2x">
                                                    <TextArea className="col" id="AddIncentForVolunteer" TextLabel="Additional Incentives for Volunteer" textareaClass="form-control" />

                                                    <TextArea className="col" id="Description" TextLabel="Description" textareaClass="form-control" />
                                                </div>
                                                <div className="row ">
                                                    <div className="col-3">
                                                        <button type="button" className="btn btn-info btn-sm" data-toggle="modal" data-target="#JobDescriptor">Job (TOR) Description</button>
                                                        <ul>
                                                            {this.state.JobDescriptor.map((x, y) => {
                                                                return (
                                                                    <li key={random()} className=""><p name="Jbs" id={"Job" + y}>{x.description}</p></li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </div>
                                                    <div className="col-3">
                                                        <button type="button" className="btn btn-info btn-sm" data-toggle="modal" data-target="#Areasofexpertise">Areas of Expertise</button>
                                                        <ul >
                                                            {this.state.Areasofexpertise.map((x, y) => {
                                                                return (
                                                                    <li key={random()}><p name="Ars" id={"Area" + y}>{x.description}</p></li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </div>
                                                    <div className="col-3">
                                                        <button type="button" className="btn btn-info btn-sm" data-toggle="modal" data-target="#scopes">Select Duty Station(s)<i className="fas fa-map-marker fa-1x inLine"></i></button>
                                                        <ul className="list-inline">
                                                            {this.state.scope.map((x, y) => {
                                                                return (
                                                                    <li key={random()} className="list-inline-item"><p name="Ars" id={"Scope" + y}>■ {x.split('#')[1]}</p></li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </form>
                                            <button onClick={this.InsertVacancy.bind(this)} className="btn btn-info btn-lg btn-block" type="button">Save Vacancy
                                    <i className="fas fa-save ml-2"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                {/***********************************************************MODAL DESCRIPTION**************************************************/}
                <div className="modal fade" id="JobDescriptor" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Job Descriptor</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">


                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text">
                                                <a onClick={this.addItem.bind(this, "JobDescriptor")} ><i className="fas fa-plus blueText "></i></a>
                                            </div>
                                        </div>
                                        <input type="text" id="txtJobDescriptor" className="form-control" placeholder="Job" />
                                    </div>
                                </div>
                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.state.JobDescriptor.map((x, y) => {
                                                return (
                                                    <li key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Jbs" id={"Job" + y}>{x.description}</span>
                                                        <a onClick={this.deleteItem.bind(this, "JobDescriptor", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info " data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                {/***************************************************************MODAL AREA******************************************************/}
                <div className="modal fade" id="Areasofexpertise" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Areas of Expertise</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col">
                                        <div className="form-group">
                                            <label htmlFor="listAreasofexpertise">You Can Also Select an Ability</label>
                                            <select className="form-control" id="listAreasofexpertise" onChange={this.addOptionToList.bind(this, "Areasofexpertise")}>
                                                <option value="99" >Select one</option>
                                                {this.state.Abilities.map(x => {
                                                    return (
                                                        <option key={random()} value={`${x.ABILITIES_CODE}#${x.ABILITIES_DESCRIPTION}`}>{x.ABILITIES_DESCRIPTION}</option>
                                                    )
                                                })}
                                            </select>
                                        </div>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <div className="input-group-text">
                                                    <a onClick={this.addItem.bind(this, "Areasofexpertise")} ><i className="fas fa-plus blueText "></i></a>
                                                </div>
                                            </div>
                                            <input type="text" id="txtAreasofexpertise" className="form-control" placeholder="Area" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row topSpace-2x">
                                    <div className="col">
                                        <ul className="list-group">
                                            {this.state.Areasofexpertise.map((x, y) => {
                                                return (
                                                    <li id="ListArea" key={random()} className="list-group-item d-flex justify-content-between align-items-center">
                                                        <span name="Ars" id={"Area" + y}>{x.description}</span>
                                                        <a onClick={this.deleteItem.bind(this, "Areasofexpertise", y)} ><span className=" badge-pill"><i className="fas fa-times redText"></i></span></a>
                                                    </li>
                                                )
                                            })}
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-info" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/*****************************************************************MODAL SCOPE**************************************************************/}
                <div className="modal fade bd-example-modal-lg" id="scopes" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Select Duty Station</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {this.state.Province.map(x => {
                                    return (
                                        <div key={md5(x.PROVINCE_CODE)} onChange={this.renderScopes.bind(this)} className="form-check form-check-inline">
                                            <input className="form-check-input" id={x.PROVINCE_CODE} name="Scope" type="checkbox" value={`${x.PROVINCE_CODE}#${x.PROVINCE_DESCRIPTION}`} />
                                            <label className="form-check-label" htmlFor="defaultCheck1">
                                                {x.PROVINCE_DESCRIPTION}
                                            </label>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/*****************************************************************SEE PEOPPLE**************************************************************/}
                <div className="modal fade" id="seeVolunteer" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5>Volunteers({this.state.volunteers.length})</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table table-darks">
                                    <tbody >
                                        {this.state.volunteers.map(x => {
                                            return (
                                                <tr key={random()}>
                                                    {x.ENTITY_MASTER.NAME == "" ?
                                                        <th scope="row" className="whiteText"><Link to={'/perfilVolunteer/' + x.ENTITY_MASTER.ENTITY_ID}>Nameless</Link>  </th>
                                                        :
                                                        <th scope="row" className="whiteText"><Link to={'/perfilVolunteer/' + x.ENTITY_MASTER.ENTITY_ID}>{x.ENTITY_MASTER.NAME}</Link>  </th>
                                                    }
                                                </tr>
                                            )
                                        })}
                                    </tbody>

                                </table>
                            </div>
                            <div className="modal-footer">
                                <button className="btn btn-info" data-dismiss="modal" type="button">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        )
    }
}

export default Vacancy
